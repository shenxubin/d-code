#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "namePipe.hpp"

#define SIZE 1024
#define MAX_ARGV_SIZE 32
#define NONE_REDIR 0
#define STDIN_REDIR 1
#define STDOUT_REDIR 2

#define MAXLINE 4096
#define MAXPIPE 16
#define MAXARG 8

int redirType = NONE_REDIR;
char *filename = nullptr;
int pipe_num = 0;

char *buffer[10][10] = {0};
const char *filename2[] = {"./file1.txt", "./file2.txt", "./file3.txt", "./file3.txt", "./file4.txt", "./file5.txt"};
pid_t child_process[10];
int pfd[10][2];

struct
{
    char *argv[MAXARG];
    char *in, *out;
} cmd[MAXPIPE + 1];

const char *getEnvVa(char *env_name)
{
    if (strcmp(env_name, "HOSTNAME") == 0)
        return getenv("HOSTNAME");
    else if (strcmp(env_name, "USER") == 0)
        return getenv("USER");
    else if (strcmp(env_name, "PWD") == 0)
        return getenv("PWD");
    else if (strcmp(env_name, "HOME") == 0)
        return getenv("HOME");
}

void printCommandLine()
{
    char host_name[1024];
    char pwd[1024];
    gethostname(host_name, 1024);
    getcwd(pwd, 1024);
    printf("[%s@%s:%s]$ ", getlogin(), host_name, pwd);
}

size_t inputCommand(char command[], size_t size)
{
    fgets(command, SIZE, stdin);
    command[strlen(command) - 1] = '\0';
    size_t len = strlen(command);
    return len;
}

void splitCommand(char command[], char *argv[])
{
    int i = 0;
    argv[i++] = strtok(command, " ");
    while (argv[i++] = strtok(NULL, " "))
    {
    }
}
// export PATH=$PATH:/home/sxb/240610 path gpath=$gpath:/home/sxb/240610
int dealCdExitPathCommand(char *argv[])
{
    int res = 0;
    if (strcmp(argv[0], "cd") == 0)
    {
        res = 1;
        const char *target = argv[1];
        if (!target)
            target = getEnvVa((char *)"HOME");
        chdir(target);
    }
    else if (strcmp(argv[0], "path") == 0)
    {
        res = 1;
        char *path;
        path = getenv("gpath");
        char new_path[2048];
        char buffer[1024] = {0};
        for (int i = 0; i < strlen(argv[1]) - 12; i++)
            buffer[i] = argv[1][13 + i];
        snprintf(new_path, sizeof(new_path), "%s:%s", path, buffer);
        setenv("gpath", new_path, 1);
        setenv("PATH", getenv("gpath"), 1);
    }
    else if (strcmp(argv[0], "exit") == 0)
    {
        exit(0);
    }
    return res;
}

void checkRedir(char command[])
{
    // ls -l > log.txt
    redirType = NONE_REDIR;
    filename = nullptr;
    int pos = 0;
    while (pos < strlen(command))
    {
        if (command[pos] == '>')
        {
            redirType = STDOUT_REDIR;
            command[pos] = '\0';
            while (command[++pos] == ' ')
            {
            }
            filename = command + pos;
            break;
        }
        else if (command[pos] == '<')
        {
            redirType = STDIN_REDIR;
            command[pos] = '\0';
            while (command[++pos] == ' ')
            {
            }
            filename = command + pos;
            break;
        }
        else
            pos++;
    }
}

void checkPipe(char command[], char *argv[])
{
    pipe_num = 0;
    int pos = 0;
    while (pos < strlen(command))
    {
        if (command[pos] == '|')
        {
            pipe_num++;
        }
        pos++;
    }
    int j = 0;
    int k = 0;
    for (int i = 0; i <= pipe_num; i++)
    {
        while (argv[k] != NULL && strcmp(argv[k], "|") != 0)
            buffer[i][j++] = argv[k++];
        j = 0;
        k++;
    }
}

int parse(char *buf, int cmdnum)
{
    int n = 0;
    char *p = buf;
    cmd[cmdnum].in = cmd[cmdnum].out = NULL;

    // ls -l -d -a -F  > out
    while (*p != '\0')
    {

        if (*p == ' ')
        { // 将字符串中所有的空格,替换成'\0',方便后续拆分字符串
            *p++ = '\0';
            continue;
        }

        if (*p == '<')
        {
            *p = '\0';
            while (*(++p) == ' ')
                ; /* cat <     file 处理连续多个空格的情况*/
            cmd[cmdnum].in = p;
            if (*p++ == '\0') // 输入重定向<后面没有文件名
                return -1;
            continue;
        }

        if (*p == '>')
        {
            *p = '\0';
            while (*(++p) == ' ')
                ;
            cmd[cmdnum].out = p;
            if (*p++ == '\0')
                return -1;
            continue;
        }

        if (*p != ' ' && ((p == buf) || *(p - 1) == '\0'))
        {

            if (n < MAXARG - 1)
            {
                cmd[cmdnum].argv[n++] = p++; //"ls -l -R > file"
                continue;
            }
            else
            {
                return -1;
            }
        }
        p++;
    }

    if (n == 0)
    {
        return -1;
    }

    cmd[cmdnum].argv[n] = NULL;

    return 0;
}

int main()
{
    int x = 0;
    char buf[MAXLINE];
    pid_t pid;
    int fd, i, j, pfd[MAXPIPE][2], pipe_num, cmd_num;
    char *curcmd, *nextcmd;
    char command2[1024];
    while (true)
    {
        // 打印命令行
        printCommandLine();

        // 输入命令
        char command[SIZE];
        int n = inputCommand(command, SIZE);
        // 处理空串
        if (n == 0)
            continue;

        snprintf(command2, sizeof(command2), "%s", command);

        // 检查是否有输入输出重定向
        checkRedir(command);
        // 切割命令
        char *argv[MAX_ARGV_SIZE];
        splitCommand(command, argv);

        // 设置一个新的环境变量gpath,初始化为PATH环境变量中的内容
        if (x == 0)
        {
            setenv("gpath", getenv("PATH"), 1);
            x = 1;
        }

        // 处理cd、exit和path命令
        n = dealCdExitPathCommand(argv);
        if (n == 1)
            continue;

        // 检查是否有管道
        checkPipe(command2, argv);
        if (pipe_num != 0)
        {
            if (buf[strlen(command2) - 1] == '\n')
                buf[strlen(command2) - 1] = '\0';
            cmd_num = 0;
            nextcmd = command2;
            while ((curcmd = strsep(&nextcmd, "|")))
            {

                if (parse(curcmd, cmd_num++) < 0)
                {
                    cmd_num--;
                    break;
                }

                if (cmd_num == MAXPIPE + 1)
                    break;
            }

            if (!cmd_num)
                continue;

            pipe_num = cmd_num - 1; // 根据命令数确定要创建的管道数目

            for (i = 0; i < pipe_num; i++)
            { // 创建管道
                if (pipe(pfd[i]))
                {
                    perror("pipe");
                    exit(1);
                }
            }

            for (i = 0; i < cmd_num; i++)
            { // 管道数目决定创建子进程个数
                if ((pid = fork()) == 0)
                    break;
            }

            if (pid == 0)
            {
                if (pipe_num)
                { // 用户输入的命令中含有管道

                    if (i == 0)
                    { // 第一个创建的子进程
                        dup2(pfd[0][1], STDOUT_FILENO);
                        close(pfd[0][0]);

                        for (j = 1; j < pipe_num; j++)
                        { // 在该子进程执行期间,关闭该进程使用不到的其他管道的读端和写端
                            close(pfd[j][0]);
                            close(pfd[j][1]);
                        }
                    }
                    else if (i == pipe_num)
                    { // 最后一个创建的子进程
                        dup2(pfd[i - 1][0], STDIN_FILENO);
                        close(pfd[i - 1][1]);

                        for (j = 0; j < pipe_num - 1; j++)
                        { // 在该子进程执行期间,关闭该进程不使用的其他管道的读/写端
                            close(pfd[j][0]);
                            close(pfd[j][1]);
                        }
                    }
                    else
                    {
                        dup2(pfd[i - 1][0], STDIN_FILENO); // 重定中间进程的标准输入至管道读端
                        close(pfd[i - 1][1]);              // close管道写端

                        dup2(pfd[i][1], STDOUT_FILENO); // 重定中间进程的标准输出至管道写端
                        close(pfd[i][0]);               // close管道读端

                        for (j = 0; j < pipe_num; j++) // 关闭不使用的管道读写两端
                            if (j != i || j != i - 1)
                            {
                                close(pfd[j][0]);
                                close(pfd[j][1]);
                            }
                    }
                }
                if (cmd[i].in)
                {                                   /*用户在命令中使用了输入重定向*/
                    fd = open(cmd[i].in, O_RDONLY); // 打开用户指定的重定向文件,只读即可
                    if (fd != -1)
                        dup2(fd, STDIN_FILENO); // 将标准输入重定向给该文件
                }
                if (cmd[i].out)
                {                                                              /*用户在命令中使用了输出重定向*/
                    fd = open(cmd[i].out, O_WRONLY | O_CREAT | O_TRUNC, 0644); // 使用写权限打开用户指定的重定向文件
                    if (fd != -1)
                        dup2(fd, STDOUT_FILENO); // 将标准输出重定向给该文件
                }

                execvp(cmd[i].argv[0], cmd[i].argv); // 执行用户输入的命令
                fprintf(stderr, "executing %s error.\n", cmd[i].argv[0]);
                exit(127);
            }

            /*  parent */
            for (i = 0; i < pipe_num; i++)
            { /*父进程不参与命令执行,关闭其掌握的管道两端*/
                close(pfd[i][0]);
                close(pfd[i][1]);
            }

            for (i = 0; i < cmd_num; i++)
            { /*循环回首子进程*/
                wait(NULL);
            }
            //continue;
        //}

        // checkPipe(command2, argv);

        //     // int pipefd[10][2] = {0};
        //     // for (int i = 0; i <= pipe_num; i++)
        //     // {
        //     //     pipe(pipefd[i]);
        //     // }
        //     // for (int i = 0; i <= pipe_num; i++)
        //     // {
        //     //     pid_t id = fork();
        //     //     if (id == 0)
        //     //     {
        //     //         if (i != 0)
        //     //         {
        //     //             close(pipefd[i][1]);
        //     //             dup2(pipefd[i][0], 0);
        //     //         }
        //     //         if (i != pipe_num)
        //     //         {
        //     //             close(pipefd[i + 1][0]);
        //     //             dup2(pipefd[i + 1][1], 1);
        //     //         }
        //     //         execvp(buffer[i][0], buffer[i]);
        //     //     }
        //     //     else
        //     //     {
        //     //         close(pipefd[i][0]);
        //     //         close(pipefd[i][1]);
        //     //         waitpid(id, NULL, 0);
        //     //     }
        //     // }

        // if (pipe_num != 0)
        // {
        //     for (int i = 0; i < pipe_num; i++)
        //     {
        //         Fifo fifo(filename2[i]);
        //     }

        //     for (int i = 0; i <= pipe_num; i++)
        //     {
        //         pid_t id = fork();
        //         if (id == 0)
        //         {
        //             if (i == 0)
        //             {
        //                 int wfd = open(filename2[i], O_WRONLY);
        //                 dup2(wfd, STDOUT_FILENO);
        //                 execvp(buffer[i][0], buffer[i]);
        //             }

        //         }
        //         else if (id > 0)
        //         {
        //             child_process[i] = id;
        //         }
        //     }
        // }

        // if (pipe_num != 0)
        // {
        //     pid_t processId[pipe_num + 1];
        //     pid_t id;
        //     for (int i = 0; i <= pipe_num; i++)
        //     {
        //         int pipefd[2];
        //         pipe(pipefd);
        //         id = fork();
        //         if (id > 0)
        //         {
        //             close(pipefd[0]);
        //             close(pipefd[1]);
        //             processId[i] = id;
        //         }
        //         else if (id == 0)
        //         {
        //             pifd[i][0] = pipefd[0];
        //             pifd[i][1] = pipefd[1];
        //         }
        //     }
        //     for (int i = 0; i <= pipe_num; i++)
        //     {
        //         if (id == 0)
        //         {
        //             if (i == 0)
        //             {
        //                 dup2(pifd[i][1], STDOUT_FILENO);
        //             }
        //             else
        //             {
        //                 dup2(pifd[i - 1][0], STDIN_FILENO);
        //                 close(pifd[i - 1][1]);
        //                 dup2(pifd[i][1], STDOUT_FILENO);
        //             }
        //             execvp(buffer[i][0], buffer[i]);
        //         }
        //     }
        // }
        if(pipe != 0)
            continue;
        // 执行普通命令&&重定向输入输出
        pid_t id = fork();
        if (id == 0)
        {
            int fd = -1;
            if (redirType == STDIN_REDIR)
            {
                fd = open(filename, O_RDONLY);
                dup2(fd, STDIN_FILENO);
            }
            else if (redirType == STDOUT_REDIR)
            {
                fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0666);
                dup2(fd, STDOUT_FILENO);
            }
            execvp(argv[0], argv);
            pipe_num--;

            exit(0);
        }
        pid_t rid = waitpid(id, nullptr, 0);
    }
    return 0;
}