#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <cstdio>
 
using namespace std;
 
#define MODE 0666 //权限
#define NAME "./fifo.txt"
 
//定义命名管道结构体
class Fifo
{
private:
    const char* _name; // 文件路径加文件名
public:
    Fifo(const char*name)
        : _name(name)
    {
        int n = mkfifo(_name, MODE);
 
        if (n == 0)
            cout << "创建管道成功！" << endl;
        else
            cout << "创建管道失败！原因是：" << strerror(errno) << endl;
    };
    ~Fifo()
    {
        int n = unlink(_name);
 
        if (n == 0)
            cout << "删除管道成功！" << endl;
        else
            cout << "删除管道失败！原因是：" << strerror(errno) << endl;
    };
};