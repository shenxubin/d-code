#include <iostream>
#include <memory>
#include "udpserver.hpp"

void Usage(std::string proc)
{
    std::cout << "Usage:\n\t" << proc << " local port\n" << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        exit(USAGE_ERROR);
    }
    EnableScreen();
    uint16_t port = std::stoi(argv[1]);
    std::unique_ptr<udpserver> usvr = std::make_unique<udpserver>(port);
    usvr->InitServer();
    usvr->Start();

    return 0;
}