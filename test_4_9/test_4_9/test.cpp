#include <iostream>
//std:头文件里的所有函数以及一些对象都是在这个命名空间中定义的，为了避免重复
//using std::cout;
//using std::endl;
using std::cout;
using std::endl;

void test(int a = 10, int b = 20, int c = 30)
{
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
}


//int main()
//{
//    cout << "Hello bit" << endl;
//    system("pause");
//    return 0;
//}

//缺省参数 - 备胎

//全缺省参数
//传一个参数不能只传给b，只能传给a。从左往右给
//半缺省参数
//1.必须从右往左连续缺省，不能间隔
//2.缺省参数不能在函数声明和定义中同时出现。分离定义时，在声明中给定义中没给可以，在定义中给在声明中没给不行。
//3.缺省值必须是全局变量或常量

//函数重载
//要求：同一作用域中，参数顺序不同，个数不同，类型不同。
//意义：像用同一个函数一样

//引用：给已经存在的变量取别名，共用同一块空间
//1.引用在定义的时候必须初始化 int& b = a
//2.一个变量可以有多个引用
//3.引用一旦引用了一个实体，就不能引用其他实体

int main()
{
    int a = 10;
    int& b = a;//引用
    int x = 20;
    b = x;//这里是给b赋值
    int& b = x;
    /*cout << &b << endl;*/
    cout << &a << endl;//取地址
    return 0;
}