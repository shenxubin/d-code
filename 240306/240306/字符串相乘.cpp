class Solution {
public:
    string multiply(string num1, string num2)
    {

        int len1 = num1.length();
        int len2 = num2.length();

        //long long arrsum[300][300];

        //for (int i = 0; i < len1; i++)
        //{
        //    for (int j = 0; j < len2; j++)
        //    {
        //        arrsum[i][j] = (num1[i] - 48) * (num2[j] - 48);
        //    }
        //}

        //long long sum[500] = { 0 };

        //for (int i = 0; i < len1; i++)
        //{
        //    for (int j = 0; j < len2; j++)
        //    {
        //        int x = 499;
        //        while (arrsum[i][j] > 0)
        //        {
        //            sum[x] += arrsum[i][j] % 10;
        //            arrsum[i][j] /= 10;
        //            x--;
        //        }
        //    }
        //}
        if (num1[0] == '0' || num2[0] == '0')
            return "0";

        long long sum[500] = { 0 };
        int y = 499;
        for (int i = len1 - 1; i >= 0; i--)
        {
            int x = y;
            y--;
            for (int j = len2 - 1; j >= 0; j--)
            {
                sum[x] += (num1[i] - 48) * (num2[j] - 48);
                x--;
            }
        }


        int sumlen = 0;
        int flag = 0;
        for (int i = 0; i < 500; i++)
        {
            if (sum[i] != 0)
            {
                flag = i;
                break;
            }
        }
        sumlen = 500 - flag;
        for (int i = 499; i >= 499 - sumlen + 1; i--)
        {
            int x = sum[i];
            sum[i] %= 10;
            int y = i;
            x /= 10;
            while (x > 0)
            {
                y--;
                sum[y] += x % 10;
                x /= 10;
            }
        }
        sumlen = 0;
        for (int i = 0; i < 500; i++)
        {
            if (sum[i] != 0)
            {
                flag = i;
                break;
            }
        }
        sumlen = 500 - flag;

        string str;
        for (int i = 500 - sumlen; i < 500; i++)
        {
            str += (sum[i] + 48);
        }
        return str;
    }
};