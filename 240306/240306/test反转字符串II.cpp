//class Solution {
//public:
//
//    void reverse(string& s, int begin, int end, int k)
//    {
//        if (end - begin + 1 == 2 * k)
//        {
//            int mid = (begin + end) / 2;
//            while (begin < mid)
//            {
//                swap(s[begin], s[mid]);
//                begin++;
//                mid--;
//            }
//        }
//        else if (end - begin + 1 < k)
//        {
//            while (begin < end)
//            {
//                swap(s[begin], s[end]);
//                begin++;
//                end--;
//            }
//        }
//        else if (end - begin + 1 < 2 * k && end - begin + 1 >= k)
//        {
//            int end = begin + k - 1;
//            while (begin < end)
//            {
//                swap(s[begin], s[end]);
//                begin++;
//                end--;
//            }
//        }
//    }
//    string reverseStr(string s, int k)
//    {
//        int len = s.length();
//        int i = 0;
//        while (i < len)
//        {
//            if (i + k - 1 < len)
//                reverse(s, i, i + k - 1, k);
//            else
//            {
//                reverse(s, i, len - 1, k);
//            }
//            i += 2 * k;
//        }
//
//        return s;
//    }
//};