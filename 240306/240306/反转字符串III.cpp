//class Solution {
//public:
//
//    void reverse(string& s, int begin, int end)
//    {
//        while (begin < end)
//        {
//            swap(s[begin], s[end]);
//            begin++;
//            end--;
//        }
//    }
//    string reverseWords(string s)
//    {
//        int len = s.length();
//        int i = 0;
//        while (i < len)
//        {
//            int begin = i; int end = i;
//            while (end < len && s[end] != ' ')
//            {
//                end++;
//            }
//            reverse(s, begin, end - 1);
//            i = end + 1;
//        }
//        return s;
//    }
//};