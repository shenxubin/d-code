class Solution {
public:
    vector<string> vect;
    string str;

    void caculatorString(string str, TreeNode* root)
    {
        if (root == nullptr)
            return;
        str += to_string(root->val);
        caculatorString(str, root->left);
        caculatorString(str, root->right);
        if (root->left == nullptr && root->right == nullptr)
            vect.push_back(str);
    }

    int sumNumbers(TreeNode* root)
    {
        caculatorString(str, root);
        int sum = 0;
        for (int i = 0; i < vect.size(); i++)
        {
            sum += stoi(vect[i]);
        }
        return sum;
    }
};