class Solution {
public:
    bool evaluateTree(TreeNode* root) {
        if (root->left == nullptr && root->val == 0)
            return false;
        else if (root->left == nullptr && root->val == 0)
            return true;

        int ret1 = -1; int ret2 = -1;
        if (root->left)
            ret1 = evaluateTree(root->left);
        if (root->right)
            ret2 = evaluateTree(root->right);

        if (ret1 != -1 && ((ret1 == 0 && ret2 == 0) ||
            ((ret1 == 0 || ret2 == 0) && root->val == 3)))
        {
            root->val = 0;
        }
        else
            root->val = 1;

        return root->val;
    }
};