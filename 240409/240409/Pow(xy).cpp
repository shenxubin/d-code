class Solution {
public:
    double myPow(double x, int n)
    {
        if (n == 0)
            return 1;
        if (n == 1)
            return x;
        if (n == -1)
            return 1 / x;
        if (n % 2 == 1 || n % 2 == -1)
        {
            double ret = myPow(x, n / 2);
            if (n < 0)
                return ret * ret * (1 / x);
            return ret * ret * x;
        }
        else
        {
            double ret = myPow(x, n / 2);
            return ret * ret;
        }
    }
};