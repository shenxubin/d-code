#include <iostream>
using namespace std;

class Point2D
{
    friend class Circle;
    friend class Ellipse;
    float x, y;
    Point2D(float x = 0, float y = 0)
    {
        this->x = x;
        this->y = y;
    }
    void  Translate(float x, float y)
    {
        this->x = x;
        this->y = y;
    }
};

class Ellipse
{
    Point2D center;
    float xRadius;
    float yRadius;
public:
    Ellipse(float x, float y, float xRadius, float yRadius) :center(x, y)
    {
        this->xRadius = xRadius;
        this->yRadius = yRadius;
    }
    void Translate(float x, float y)
    {
        this->center.x = x;
        this->center.y = y;
    }
};

class Circle
{
    Point2D center;
    float radius;
public:
    Circle(float x, float y, float radius) :center(x, y), radius(radius)
    {
    
    }
    void Translate(float x, float y)
    {
        this->center.x = x;
        this->center.y = y;
    }
    void Scale(float r)
    {
        this->radius = r;
    }
};


int main()
{


    system("pause");
    return 0;
}