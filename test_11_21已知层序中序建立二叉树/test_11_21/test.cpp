#include <iostream>
#include <vector>
using namespace std;

struct node
{
    node* left;
    node* right;
    int val;
};

vector<int> level;
int pos[100];

node* CreatTree(int index, node*& root)
{
    if (root == nullptr)
    {
        root = new node;
        root->left = nullptr;
        root->right = nullptr;
        root->val = level[index];
        return root;
    }
    else
    {
        if (pos[root->val] > pos[level[index]])
            CreatTree(index, root->left);
        else
            CreatTree(index, root->right);
        return root;
    }
}

void PrevOrder(node* root)
{
    if (root == nullptr)
        return;
    cout << root->val << " ";
    PrevOrder(root->left);
    PrevOrder(root->right);
}

void PostOrder(node* root)
{
    if (root == nullptr)
        return;
    PostOrder(root->left);
    PostOrder(root->right);
    cout << root->val << " ";
}

int main()
{
    int n;
    cin >> n;
    level.resize(n+1);
    int num;
    for (int i = 1; i <= n; i++)
    {
        cin >> level[i];
    }
    for (int i = 1; i <= n; i++)
    {
        cin >> num;
        pos[num] = i;
    }

    node* root = nullptr;
    for (int index = 1; index <= n; index++)
    {
        CreatTree(index, root);
    }
    PrevOrder(root);
    cout << endl;
    PostOrder(root);
    cout << endl;
    return 0;
}