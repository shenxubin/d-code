//#include <iostream>
//using namespace std;
//
//class Person
//{
//public:
//    int m_A;
//    Person()
//    {
//        m_A = 10;
//    }
//    Person& operator++()
//    {
//        m_A++;
//        return *this;
//    }
//    Person operator++(int)
//    {
//        Person p2 = *this;
//        m_A++;
//        return p2;
//    }
//};
//
//ostream& operator<<(ostream& cout, Person p)
//{
//    cout << p.m_A;
//    return cout;
//}
//
//void test01()
//{
//    Person p1;
//    cout << ++p1 << endl;
//}
//
//void test02()
//{
//    Person p2;
//    cout << p2++ << endl;
//    cout << p2 << endl;
//}
//int main()
//{
//    test01();
//    /*test02();*/
//    system("pause");
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//class Person
//{
//    friend ostream& operator<<(ostream& cout, Person p);
//public:
//    Person()
//    {
//        m_A = 10;
//    }
//    Person& operator--()
//    {
//        m_A--;
//        return *this;
//    }
//    Person operator--(int)
//    {
//        Person p2 = *this;
//        m_A--;
//        return p2;
//    }
//private:
//    int m_A;
//};
//
//ostream& operator<<(ostream& cout, Person p)
//{
//    cout << p.m_A;
//    return cout;
//}
//
//void test01()
//{
//    Person p1;
//    cout << --p1 << endl;
//}
//
//void test02()
//{
//    Person p2;
//    cout << p2-- << endl;
//    cout << p2 << endl;
//}
//int main()
//{
//    /*test01();*/
//    test02();
//    system("pause");
//    return 0;
//}


#include <iostream>
using namespace std;

class Person
{
    friend void test01();
    Person(int age)
    {
        m_Age = new int(age);
    }
    ~Person()
    {
        if (m_Age != NULL)
        {
            delete m_Age;
            m_Age = NULL;
        }
    }
    Person& operator=(Person& p)
    {
        if (m_Age != NULL)
        {
            delete m_Age;
            m_Age = NULL; 
        }
        m_Age = new int(*p.m_Age);
        return *this;
    }

    int *m_Age;
};

void test01()
{
    Person p1(18);
    Person p2(20);
    Person p3(30);
    p3 = p2 = p1;
    cout << *p1.m_Age << endl;
    cout << *p2.m_Age << endl;
    cout << *p3.m_Age << endl;
}
int main()
{
    test01();

    system("pause");
    return 0;
}