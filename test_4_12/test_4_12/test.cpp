#include <iostream>
using namespace std;

class Day
{
public:
    void Init(/*Date* const this*/ int year, int month, int day)
    {
        _year = year;
        _month = month;
        _day = day;
    }
    void Print()
    {
        cout << _year << "-" << _month << "-" << _day << endl;
    }

private:
    int _year;
    int _month;
    int _day;
};
int main()
{
    Day d1;
    d1.Init(/*&d1,*/ 2022, 4, 12);
    d1.Print();
    return 0;
}

//#include <stdio.h>
//int main()
//{
//    int hour, minute, k;
//    scanf("%d:%d %d", &hour, &minute, &k);
//    int hour1 = k / 60;
//    int minute1 = k % 60;
//    hour += hour1;
//    minute += minute1;
//    if (minute >= 60)
//    {
//        minute -= 60;
//        hour += 1;
//    }
//    while (hour >= 24)
//    {
//        hour -= 24;
//    }
//    printf("%02d:%02d\n", hour, minute);
//    return 0;
//}

//#include <stdio.h>
//int main()
//{
//    char ch;
//    scanf("%c", &ch);
//    printf("%d\n", ch);
//    return 0;
//}


