#include <iostream>
#include <cstring>
#include <queue>
using namespace std;
void Print(int a)//输出函数
{
    cout << a << " ";
}
void DeepFirstTravel(int start, int** Tlist, int n, int visit[])
{
    Print(start);
    visit[start] = 1;//标记已经走过
    for (int i = 0; i < n; i++)//递归停止的条件就是最后一个递归for循环走完
    {
        if (Tlist[start][i] != 0 && visit[i] != 1)
            DeepFirstTravel(i, Tlist, n, visit);
    }
}
void BreadthFirstTravel(int start, int** Tlist, int n, int visit[])
{
    queue<int> q;
    Print(start);
    visit[start] = 1;
    q.push(start);
    int cur;
    while (!q.empty())
    {
        cur = q.front();
        q.pop();
        for (int i = 0; i < n; i++)
        {
            if (Tlist[cur][i] != 0 && visit[i] != 1)//先访问、标记，再入队
            {
                Print(i);
                visit[i] = 1;
                q.push(i);
            }
        }
    }
}
int main()
{
    int n;
    cin >> n;
    int** a;//邻接矩阵
    int* visit;//标记数组
    a = new int* [n];
    for (int i = 0; i < n; i++)
    {
        a[i] = new int[n];
    }
    visit = new int[n];
    for (int i = 0; i < n; i++)
        visit[i] = 0;
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cin >> a[i][j];
    }
    cout << "DFS" << endl;
    for (int i = 0; i < n; i++)
    {
        DeepFirstTravel(i, a, n, visit);
        cout << endl;
        for (int i = 0; i < n; i++)
            visit[i] = 0;

    }
    cout << "WFS" << endl;
    for (int i = 0; i < n; i++)
    {
        BreadthFirstTravel(i, a, n, visit);
        cout << endl;
        for (int i = 0; i < n; i++)
            visit[i] = 0;
    }
}


// 64 位输出请用 printf("%lld")