#include<iostream>//题目不对劲，有两条最短路
#include<string>
#include<queue>
#include<stack>
#include<cstdio>
using namespace std;
int maze[1000][1000];
int mark[1000][1000];

struct Offsets
{
    int dx, dy;
    string dir;
    Offsets(int x = -1, int y = -1, string d = "") :dx(x), dy(y), dir(d) {}
};
Offsets guide[4] = { {0,-1,"W"},{0,1,"E"},{1,0,"S"},{-1,0,"N"} };//x.y是方向对应坐标的变化
int sx, sy;
int ex, ey;

struct PosInfo
{
    int px, py;
    PosInfo(int x = 0, int y = 0) :px(x), py(y) {}
};
PosInfo pre[1000][1000] = { 0,0 };
int SeekShortesPath()
{
    PosInfo inf(sx, sy);
    //Queue<PosInfo> q;
    queue<PosInfo> q;
    //q.EnQueue(inf);//将起点位置入队
    q.push(inf);
    int nx, ny;
    mark[sx][sy] = 1;//标记起点位置已经访问过
    while (!q.empty())
    {
        //q.DeQueue(inf);//队头元素出队
        inf = q.front();
        q.pop();
        PosInfo temp = inf;
        //int ix=inf.px;
        //int jy=inf.py;
        for (int i = 0; i < 4; i++)  // 四个方向扩展（搜索）查看队头元素的四个方向
        {
            nx = inf.px + guide[i].dx;
            ny = inf.py + guide[i].dy;

            //pre[nx][ny].px=ix;
            //pre[nx][ny].py=jy;
            //pre[nx][ny]=(ix,jy);//ix,ij是nx,ny的上一个位置
            if (maze[nx][ny] == 0 || mark[nx][ny] == 1)
                continue;
            //cout<<nx<<ny<<endl;
            pre[nx][ny] = temp;
            //cout<<pre[nx][ny].px<<pre[nx][ny].py;
            if (maze[nx][ny] == 4)
            {
                ex = nx;
                ey = ny;
                return 1;
            }
            //q.EnQueue(PosInfo(nx,ny));
            q.push(PosInfo(nx, ny));
            mark[nx][ny] = 1;//将可走的位置入队并标记
        }
    }
    return 0;
}
void PrintPath(int row, int column)
{

    PosInfo temp;
    stack<PosInfo> s;
    temp.px = ex;
    temp.py = ey;
    while (temp.px != row || temp.py != column)
    {
        s.push(temp);
        temp = pre[temp.px][temp.py];
    }
    s.push(temp);
    while (!s.empty())
    {
        temp = s.top();
        cout << temp.px << " " << temp.py << " " << endl;
        s.pop();
    }
    cout << endl;
}
int main()
{
    int a, b;
    cin >> a >> b;

    for (int i = 0; i < a; i++)
    {
        for (int j = 0; j < b; j++)
            cin >> maze[i][j];
    }
    for (int i = 0; i < a; i++)
    {
        for (int j = 0; j < b; j++)
            if (maze[i][j] == 3)
            {
                sx = i;
                sy = j;
            }
    }

    SeekShortesPath();
    PrintPath(sx, sy);
}
// 64 位输出请用 printf("%lld")