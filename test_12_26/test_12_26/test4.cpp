#include<iostream>
#include<string>
#include<queue>
#include<stack>
#include<cstdio>
using namespace std;
int defaultNumVertices = 100;
int maxWeight = 0x7fffffff;
template<typename T, typename E>
class GraphMatrix
{
private:
    int maxVertices;
    int numVertices;
    int numEdges;
    T* vertexList;//定点表 记录各个顶点信息的顶点表
    E** edge;//邻接矩阵 二维数组
public:
    GraphMatrix(int sz = defaultNumVertices)
    {
        maxVertices = sz;//让最大顶点数和现在的顶点数同步
        numVertices = 0;//让顶点数为零，还没输入顶点
        numEdges = 0;
        vertexList = new T[sz];
        edge = new E * [sz];
        for (int i = 0; i < sz; i++)
        {
            edge[i] = new E[sz];
        }
        for (int i = 0; i < sz; i++)
        {
            for (int j = 0; j < sz; j++)
                edge[i][j] = 0;//全部置零
        }
    }

    ~GraphMatrix()
    {
        delete[] vertexList;
        for (int i = 0; i < maxVertices; i++)
        {
            delete edge[i];
        }
        delete[] edge;
    }
    int GetFirstNeighbor(int v)//
    {
        if (v == -1)
            return -1;
        for (int col = 0; col < maxVertices; col++)//列，选中该行第一个连接的顶点（矩阵中第一个为1的列）
        {
            if (edge[v][col] == 1)
            {
                return col;
            }
        }
        return -1;
    }
    int GetNextNeighbor(int v, int w)
    {
        if (v == -1 || w == -1)
            return -1;
        for (int col = w + 1; col < maxVertices; col++)
        {
            if (edge[v][col] == 1)
                return col;
        }
        return -1;
    }
    bool InsertEdge(int u, int v, int cost)
    {
        edge[u][v] = cost;//有向图
        numEdges++;
        return true;
    }
};
template<typename T, typename E>
void TopologicalSort(GraphMatrix<T, E>& G, int** a, int n)
{
    int i, j, w, v;
    int top = -1;//入度为为零的顶点的初始化
    int* count = new int[n];//入度数组，所有入度为零的顶点
    for (i = 0; i < n; i++)
    {
        count[i] = 0;
    }
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
        {
            if (a[i][j] > 0)
                count[j]++;
        }
    for (i = 0; i < n; i++)
    {
        if (count[i] == 0)
        {
            //刚开始top=-1
            count[i] = top;//选一个没有直接前驱的顶点，输出，并删除它发出的有向边
            top = i;//如何删除它发出的有向边
        }
    }
    for (i = 0; i < n; i++)
    {
        if (top == -1)
        {
            cout << "YES";//所有
            return;
        }
        else
        {
            v = top;//v是哪个顶点
            top = count[top];//取次级点
            //cout<<G.getValue(v)<<" "<<endl;
            //cout<<v<<endl;
            w = G.GetFirstNeighbor(v);
            //cout<<w<<endl;
    //w是列的位置，为什么要取第一个邻接点，为了减一
            while (w != -1)
            {
                if (--count[w] == 0)//这个循环就是让输出的那个边连接的点减一
                {
                    count[w] = top;//如果出现两个零怎么办？利用次级点，count[top]
                    top = w;//到这一行的最后一个连接点

                }
                w = G.GetNextNeighbor(v, w);
            }
        }
    }
    cout << "NO";
}
int main()
{
    int n;
    cin >> n;
    int** a;
    a = new int* [n];
    for (int i = 0; i < n; i++)
        a[i] = new int[n];
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            cin >> a[i][j];
    }
    GraphMatrix<int, int> b(n);
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
            if (a[i][j] > 0)
                b.InsertEdge(i, j, 1);
    }
    TopologicalSort(b, a, n);
}


// 64 位输出请用 printf("%lld")