#include "List.h"

//创建一个新结点
ListNode* BuyListNode(string _name, int _age, string _ID, DateType _CreditScore)
{
	ListNode* newnode = new ListNode;
	newnode->name = _name;
	newnode->age = _age;
	newnode->ID = _ID;
	newnode->CreditScore = _CreditScore;
	return newnode;
}

//打印链表中数据域的内容
void ListPrint(ListNode* phead)
{
	ListNode* cur = phead;
	while (cur != NULL)
	{
		cout << cur->name << " " << cur->age << " " << cur->ID << " " << cur->CreditScore << endl;
		cur = cur->next;
	}
}

//尾插
void ListPushBack(ListNode** pphead, string _name, int _age, string _ID, DateType _CreditScore)
{
	string new_file;
	cout << "请输入文件名：";
	cin >> new_file;
	fstream file;
	file.open(new_file.c_str(), ios::out);
	if (file.bad())
	{
		cout << "文件打开失败. " << endl;
		return;
	}

	ListNode* newnode = BuyListNode(_name,_age, _ID,_CreditScore);
	if (*pphead == NULL)
	{
		*pphead = newnode;
		file << newnode->name << " " << newnode->age << " " << newnode->ID << " " << newnode->CreditScore << endl;
	}
	else
	{
		ListNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
		file << tail->name << " " << tail->age << " " << tail->ID << " " << tail->CreditScore << endl;
	}
	file.close();
	cout << "录入成功" << endl;
}

//头插
void ListPushFront(ListNode** pphead, string _name, int _age, string _ID, DateType _CreditScore)
{
	ListNode* newnode = BuyListNode(_name, _age, _ID, _CreditScore);
	newnode->next = *pphead;
	*pphead = newnode;
}

//尾删
void ListPopBack(ListNode** pphead)
{
	if (*pphead == NULL)
	{
		return;
	}
	if ((*pphead)->next == NULL)
	{
		delete* pphead;
		*pphead = NULL;
	}
	else
	{
		ListNode* tail = *pphead;
		ListNode* prev = NULL;//最后会走到倒数第二个结点
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}
		delete tail;
		tail = NULL;
		prev->next = NULL;
	}
}

//头删
void ListPopFront(ListNode** pphead)
{
	if (*pphead == NULL)
	{
		return;
	}
	ListNode* next = (*pphead)->next;
	delete(*pphead);
	*pphead = next;
}

//查找指定的结点
ListNode* ListFind(ListNode* phead, string _name, int _age, string _ID, DateType _CreditScore)
{
	ListNode* cur = phead;
	while (cur)
	{
		if (cur->name == _name && cur->age == _age && cur->ID == _ID && cur->CreditScore == _CreditScore)
		{
			return cur;
		}
		else
		{
			cur = cur->next;
		}
	}
	return NULL; 
}

//修改
void ListModify(ListNode** pphead, ListNode* pos, string _name, int _age, string _ID, DateType _CreditScore)
{
	
}

//在pos位置之前插入一个结点
void ListInsert(ListNode** pphead, ListNode* pos, string _name, int _age, string _ID, DateType _CreditScore)
{
	ListNode* newnode = BuyListNode(_name, _age, _ID, _CreditScore);
	if (*pphead == pos)
	{
		//头插
		newnode->next = *pphead;
		*pphead = newnode;
	}
	else
	{
		ListNode* posPrev = *pphead;
		while (posPrev->next != pos)
		{
			posPrev = posPrev->next;
		}
		posPrev->next = newnode;
		newnode->next = pos;
	}
}

// 删除pos结点
void ListErase(ListNode** pphead, ListNode* pos)
{
	if (*pphead == pos)
	{
		ListPopFront(pphead);
	}
	else
	{
		ListNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		delete pos;
	}
}

//销毁链表
void ListDestroy(ListNode** pphead)
{
	ListNode* cur = *pphead;
	while (cur)
	{
		ListNode* next = cur->next;
		delete cur;
		cur = next;
	}
	*pphead = NULL;
}
