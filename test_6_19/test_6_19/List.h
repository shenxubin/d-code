#pragma once

#include <iostream>
#include <fstream>
using namespace std;

typedef double DateType;
class ListNode
{
public:
	ListNode();
	~ListNode();
	string name;
	int age;
	string ID;
	DateType CreditScore;
	ListNode* next;
};

//打印链表
void ListPrint(ListNode* phead);

//尾插
void ListPushBack(ListNode** pphead, string _name, int _age, string _ID, DateType _CreditScore);

//头插
void ListPushFront(ListNode** pphead, string _name, int _age, string _ID, DateType _CreditScore);

//尾删
void ListPopBack(ListNode** pphead);

//头删
void ListPopFront(ListNode** pphead);

//查找结点的值
ListNode* ListFind(ListNode* phead, string _name, int _age, string _ID, DateType _CreditScore);

//修改结点的值
void ListModify(ListNode** pphead, ListNode* pos, string _name, int _age, string _ID, DateType _CreditScore);

//在pos位置之前插入一个结点
void ListInsert(ListNode** pphead, ListNode* pos, string _name, int _age, string _ID, DateType _CreditScore);

//删除pos结点
void ListErase(ListNode** pphead, ListNode* pos);

//销毁链表
void ListDestroy(ListNode** pphead);





