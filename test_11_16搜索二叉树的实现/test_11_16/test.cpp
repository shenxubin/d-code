//test.c

#include "BinarySearchTree.h"

int main()
{
	BSTree<int> bs;

	int arr[] = { 1,3,6,4,7,8,10,14,13 };
	for (auto e : arr)
	{
		bs.Insert(e);
	}
	bs.InOrder();

	bs.EraseR(1);
	bs.InOrder();

	bs.Insert(20);
	bs.InsertR(9);
	bs.InOrder();

	bool ret = bs.FindR(20);
	cout << ret << endl;


	return 0;
}