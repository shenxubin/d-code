#pragma once

#include <iostream>
using namespace std;


template <class K>
struct BSTreeNode
{
	BSTreeNode* _left;
	BSTreeNode* _right;
	K _key;

	BSTreeNode(const K& key)
		:_left(nullptr)
		,_right(nullptr)
		,_key(key)
	{}
};

template <class K>
class BSTree
{
private:
	BSTreeNode<K>* _root;
	typedef BSTreeNode<K> node;
public:
	BSTree()
		:_root(nullptr)
	{

	}
	~BSTree()
	{
		Destroy(_root);
	}
	//增删查
	bool Insert(const K& key);
	bool Find(const K& key);
	bool Erase(const K& key);
	//中序遍历
	void InOrder();
	void _InOrder(node* root);
	//增删查的递归实现
	bool InsertR(const K& key);
	bool _InsertR(const K& key, node*& root);
	//为了对节点进行修改，这里的插入和删除的节点必须用引用传，这里是一个细节 
	bool EraseR(const K& key);
	bool _EraseR(const K& key, node*& root);
	bool FindR(const K& key);
	bool _FindR(const K& key, node* root);

	void Destroy(node* root);
};


template <class K>
bool BSTree<K>::Insert(const K& key)
{
	//树为空，则直接新增节点，赋值给root指针
	if (_root == nullptr)
	{
		_root = new node(key);
		return true;
	}
	node* parent = nullptr;
	node* cur = _root;
	while (cur)//找到key该去的位置
	{
		parent = cur;
		if (cur->_key < key)//大就往右走
		{
			cur = cur->_right;
		}
		else if (cur->_key > key)//小就往左走
		{
			cur = cur->_left;
		}
		else//有相等的值了无法再插入了
		{
			return false;
		}
	}
	if (parent->_key < key)
	{
		parent->_right = new node(key);
	}
	else
	{
		parent->_left = new node(key);
	}

	return true;
}

template <class K>
bool BSTree<K>::Find(const K& key)
{
	node* cur = _root;
	while (cur)
	{
		if (key < cur->_key)//小就往左走
		{
			cur = cur->_left;
		}
		else if (key > cur->_key)//大就往右走
		{
			cur = cur->_right;
		}
		else//找到了
		{
			return true;
		}
	}
	return false;
}

template <class K>
bool BSTree<K>::Erase(const K& key)
{
	node* parent = nullptr;
	node* cur = _root;
	while (cur)
	{
		
		if (cur->_key > key)
		{
			parent = cur;
			cur = cur->_left;
		}
		else if (cur->_key < key)
		{
			parent = cur;
			cur = cur->_right;
		}
		else//找到了就跳出循环
		{
			break;
		}
	}
	if (cur == nullptr)//cur走到空就意味着没找到
	{
		return false;
	}
	if (cur->_left == nullptr)//左为空 
	{
		if (cur == _root)
		{
			_root = cur->_right;
		}
		else if (cur == parent->_left)
		{
			parent->_left = cur->_right;
		}
		else if (cur == parent->_right)
		{
			parent->_right = cur->_right;
		}
		delete cur;
		return true;
	}
	else if (cur->_right == nullptr)//右为空
	{
		if (cur == _root)
		{
			_root = cur->_left;
		}
		else if (cur == parent->_left)
		{
			parent->_left = cur->_left;
		}
		else if (cur == parent->_right)
		{
			parent->_right = cur->_left;
		}
		delete cur;
		return true;
	}
	else//左右都不为空，去找它左树最大的节点替换它的值，再删除左树最大的节点
	{
		node* parent = nullptr;
		node* leftMax = cur;
		while (leftMax->_right)//找到左树最大的节点
		{
			parent = leftMax;
			leftMax = leftMax->_right;
		}
		swap(cur->_key, leftMax->_key);//交换值
		if (parent->_left == leftMax)
		{
			parent->_left = leftMax->_left;
		}
		else
		{
			parent->_right = leftMax->_left;
		}
		delete leftMax;
		return true;
	}
	return false;
}

template <class K>
void BSTree<K>::_InOrder(node* root)
{
	if (root == nullptr)
		return;
	_InOrder(root->_left);
	cout << root->_key << " ";
	_InOrder(root->_right);
}

template <class K>
void BSTree<K>::InOrder()
{
	_InOrder(_root);
	cout << endl;
}


template <class K>
bool BSTree<K>::EraseR(const K& key)
{
	return _EraseR(key, _root);
}


template <class K>
bool BSTree<K>::_EraseR(const K& key, node*& root)
{
	if (root == nullptr)
		return false;
	if (root->_key < key)
	{
		_EraseR(key, root->_right);
	}
	else if (root->_key > key)
	{
		_EraseR(key, root->_left);
	}
	else//找到要删除的节点了
	{
		//准备开始删除
		node* del = root;
		if (root->_left == nullptr)
		{
			root = root->_right;
		}
		else if (root->_right == nullptr)
		{
			root = root->_left;
		}
		else
		{
			node* leftMax = root->_left;
			while (leftMax->_right)
			{
				leftMax = leftMax->_right;
			}
			swap(root->_key, leftMax->_key);
			return _EraseR(key, root->_left);//交换完后去要删除节点的左子树删除最大的节点
		}
		delete del;
	}
	return true;
}

template <class K>
bool BSTree<K>::FindR(const K& key)
{
	return _FindR(key, _root);
}

template <class K>
bool BSTree<K>::_FindR(const K& key, node* root)
{
	if (root == nullptr)
	{
		return false;
	}
	if (root->_key < key)
	{
		return _FindR(key, root->_right);
	}
	else if (root->_key > key)
	{
		return _FindR(key, root->_left);
	}
	else
	{
		return true;
	}
}

template <class K>
bool BSTree<K>::InsertR(const K& key)
{
	return _InsertR(key, _root);
}
template <class K>
bool BSTree<K>::_InsertR(const K& key, node*& root)
{
	if (root == nullptr)
	{
		root = new node(key);
		return true;
	}
	if (root->_key < key)
	{
		return _InsertR(key, root->_right);
	}
	else if (root->_key > key)
	{
		return _InsertR(key, root->_left);
	}
	else
	{
		return false;
	}
}

template <class K>
void BSTree<K>::Destroy(node* root)
{
	if (root == nullptr)
		return;
	Destroy(root->_left);
	Destroy(root->_right);
	delete root;
}