#include "shared_memory.hpp"
#include "namedPipe.hpp"

int main()
{
    key_t key = get_key(PATHNAME, PROJ_ID);
    int shmid = get_shared_memory(key, DEFAULT_SIZE);
    cout << shmid << endl;

    // 挂接
    char *addr = (char *)shm_attch(shmid);

    Sync syn;
    syn.open_write();

    memset(addr, 0, DEFAULT_SIZE);
    for (char c = 'A'; c <= 'Z'; c++)
    {
        sleep(1);
        addr[c - 'A'] = c;
        syn.wake_up();
    }

    // 去挂接
    shm_detach(addr);

    return 0;
}