#include "shared_memory.hpp"
#include "namedPipe.hpp"

int main()
{
    key_t key = get_key(PATHNAME, PROJ_ID);
    int shmid = create_shared_memory(key, DEFAULT_SIZE);
    cout << shmid << endl;


    //挂接
    char* addr = (char*)shm_attch(shmid);

    Fifo fifo(NAME);
    Sync syn;
    syn.open_read();


    while (true)
    {
        int n = syn.wait();
        cout << "共享内存中的数据是：" << addr << endl;
        if(n <= 0)
            break;
    }
    

    //去挂接
    shm_detach(addr);

    //删除共享内存
    shm_del(shmid);

    return 0;
}