#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <cstdio>

using namespace std;

#define MODE 0666 //权限
#define NAME "./fifo.txt"

//定义命名管道结构体
class Fifo
{
private:
    string _name; // 文件路径加文件名
public:
    Fifo(const string &name)
        : _name(name)
    {
        int n = mkfifo(_name.c_str(), MODE);

        if (n == 0)
            cout << "创建管道成功！" << endl;
        else
            cout << "创建管道失败！原因是：" << strerror(errno) << endl;
    };
    ~Fifo()
    {
        int n = unlink(_name.c_str());

        if (n == 0)
            cout << "删除管道成功！" << endl;
        else
            cout << "删除管道失败！原因是：" << strerror(errno) << endl;
    };
};


class Sync
{
private:
    int rfd;
    int wfd;
public:
    void open_read()
    {
        rfd = open(NAME, O_RDONLY);
        if (rfd == -1)
        {
            cout << "读打开管道失败！" << endl;
            exit(1);
        }
    }

    void open_write()
    {
        wfd = open(NAME, O_WRONLY);
        if (wfd == -1)
        {
            cout << "写打开管道失败！" << endl;
            exit(1);
        }
    }

    int wait()
    {
        int ret = 0;
        int n = read(rfd, &ret, sizeof(int));
        return n;
    }

    void wake_up()
    {
        int ret = 0;
        int n = write(wfd, &ret, sizeof(int));
    }
};