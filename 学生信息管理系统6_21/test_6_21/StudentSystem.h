#pragma once

#include <iostream>
#include <conio.h>
#include <iomanip>
using namespace std;

struct Student
{
	string name = "";
	string ID = "";
	string password = "1111111111";
	int age = 0;
	double C;
	double Cplusplus;
	double math;
	double physics;
	double zuyuan;
	double English;
	double score ;
};

class Node
{
public:
	Student student;
	Node* next;
public:
	void WelcomeStudent();
	void FindStudentByStudent(Node* head, string ID);
	void ModifyStudentPassword(Node* head, string ID);
};

class Teacher :public Node
{
public:
	void WelcomeTeacher();
	void FindStudentByTeacherorSupervisor(Node* head);
	void ModifyStudentScore(Node* head);
	void SortStudent(Node* head);
	void PrintAllStudent(Node* head);
};

class Supervisor :public Node
{
public:
	void WelcomeSupervisor();
	void InsertStudent(Node* head);
	void FindStudentByTeacherorSupervisor(Node* head);
	void ModifyStudentOtherInformation(Node* head);
	void DeleteStudent(Node* head);
	void SortStudent(Node* head);
	void PrintAllStudent(Node* head);
};

class Manager :public Node
{
public:

	void WelcomeManager();
	void InsertStudent(Node* head);
	void DeleteStudent(Node* head);
	void InsertTeacher(Node* head);
	void DeleteTeacher(Node* head);
	void InsertSupervisor(Node* head);
	void DeleteSupervisor(Node* head);

};
void SaveStudent(Node* head);

void LoadStudent(Node* head);

























