#include "StudentSystem.h"

int main()
{
	Node* head = new Node;
	head->next = NULL;

	LoadStudent(head);
	cout << "请输入您的身份：学生/教师/导员/管理员" << endl;
	string name;
	cin >> name;

	if (name == "学生")
	{
		cout << "请输入学号：" << endl;
		string StuID;
		cin >> StuID;
		cout << "请输入密码：" << endl;
		string StuPassword;
		cin >> StuPassword;
		Node* move2 = head->next;
		while (move2 != NULL)
		{
			if (move2->student.ID == StuID && move2->student.password == StuPassword)
			{
				cout << "输入成功！" << endl;
				break;
			}
			move2 = move2->next;
			if (move2 == NULL)
			{
				cout << "输入的账号或密码错误，请重新输入：" << endl;
				move2 = head->next;
				cin >> StuID >> StuPassword;
			}
		}
		Node n;
		while (1)
	    {
			n.WelcomeStudent();
		    char c;
		    cin >> c;
		    switch (c)
		    {
		    case '1':
				n.FindStudentByStudent(head, StuID);
			    break;
			case '2':
				n.ModifyStudentPassword(head, StuID);
				break;
		    case '3':
			    cout << "再见，欢迎下次使用！" << endl;
				return 0;
			    break;
		    default:
			    cout << "输入错误，请重新输入：" << endl;
		    }
	    }
	}
	else if (name == "教师")
	{
		cout << "请输入账号：" << endl;
		string TeaID;
		cin >> TeaID;
		cout << "请输入密码：" << endl;
		string TeaPassword;
		cin >> TeaPassword;
		Node* move2 = head->next;
		while (move2 != NULL)
		{
			if (move2->student.ID == TeaID && move2->student.password == TeaPassword)
			{
				cout << "输入成功！" << endl;
				break;
			}
			move2 = move2->next;
			if (move2 == NULL)
			{
				cout << "输入的账号或密码错误，请重新输入：" << endl;
				move2 = head->next;
				cin >> TeaID >> TeaPassword;
			}
		}
		Teacher t;
		while (1)
		{
			t.WelcomeTeacher();
			char c;
			cin >> c;
			switch (c)
			{
			case '1':
				t.PrintAllStudent(head);
				break;
			case '2':
				t.FindStudentByTeacherorSupervisor(head);
				break;
			case '3':
				t.ModifyStudentScore(head);
				break;
			case '4':
				t.SortStudent(head);
				break;
			case '5':
				cout << "再见,欢迎下次使用！" << endl;
				return 0;
			default:
				cout << "输入错误，请重新输入：" << endl;
			}
		}
	}
	else if (name == "导员")
	{
		string SupID, SupPassword;
		cout << "请输入账号：" << endl;
		cin >> SupID;
		cout << "请输入密码：" << endl;
		cin >> SupPassword;
		Node* move2 = head->next;
		while (move2 != NULL)
		{
			if (move2->student.ID == SupID && move2->student.password == SupPassword)
			{
				cout << "输入成功！" << endl;
				break;
			}
			move2 = move2->next;
			if (move2 == NULL)
			{
				cout << "输入的账号或密码错误，请重新输入：" << endl;
				move2 = head->next;
				cin >> SupID >> SupPassword;
			}
		}
		Supervisor s;
		while (1)
		{
			s.WelcomeSupervisor();
			char c;
			cin >> c;

		    switch (c)
			{
			case '1':
				s.InsertStudent(head);
				break;
			case '2':
				s.PrintAllStudent(head);
				break;
			case '3':
				s.FindStudentByTeacherorSupervisor(head);
				break;
			case '4':
				s.ModifyStudentOtherInformation(head);
				break;
			case '5':
				s.DeleteStudent(head);
				break;
			case '6':
				s.SortStudent(head);
				break;
			case '7':
    			cout << "再见，欢迎下次使用！" << endl;
				exit(0);
				break;
			default:
				cout << "输入错误，请重新输入：" << endl;
				break;
			}
		}
	}

	else if (name == "管理员")
	{
		string ManID, ManPassword;
		cout << "请输入账号：" << endl;
		cin >> ManID;
		cout << "请输入密码：" << endl;
		cin >> ManPassword;
		Node* move2 = head->next;
		while (move2 != NULL)
		{
			if (move2->student.ID == ManID && move2->student.password == ManPassword)
			{
				cout << "输入成功！" << endl;
				break;
			}
			move2 = move2->next;
			if (move2 == NULL)
			{
				cout << "输入的账号或密码错误，请重新输入：" << endl;
				move2 = head->next;
				cin >> ManID >> ManPassword;
			}
		}
		Manager m;
		while (1)
		{
			m.WelcomeManager();
			char c;
			cin >> c;

			switch (c)
			{
			case '1':
				m.InsertStudent(head);
				break;
			case '2':
				m.DeleteStudent(head);
				break;
			case '3':
				m.InsertTeacher(head);
				break;
			case '4':
				m.DeleteTeacher(head);
				break;
			case '5':
				m.InsertSupervisor(head);
				break;
			case '6':
				m.DeleteSupervisor(head);
				break;
			case '7':
				cout << "再见，欢迎下次使用！" << endl;
				exit(0);
				break;
			default:
				cout << "输入错误，请重新输入：" << endl;
			}
		}
	}
	else
	{
		cout << "身份输入错误！" << endl;
	}
	return 0;
}


