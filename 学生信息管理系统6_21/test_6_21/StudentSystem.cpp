
#include "StudentSystem.h"

void Node::WelcomeStudent()
{
	cout << "********************************" << endl;
	cout << "****学生信息管理系统学生部分****" << endl;
	cout << "*********请选择功能列表*********" << endl;
	cout << "*********1.查找学生信息*********" << endl;
	cout << "*********2.修改密码*************" << endl;
	cout << "*********3.退出系统*************" << endl;
	cout << "********************************" << endl;
}

void Teacher::WelcomeTeacher()
{
	cout << "********************************" << endl;
	cout << "****学生信息管理系统教师部分****" << endl;
	cout << "*********请选择功能列表*********" << endl;
	cout << "*********1.打印学生信息*********" << endl;
	cout << "*********2.查找学生信息*********" << endl;
	cout << "*********3.修改学生成绩*********" << endl;
	cout << "*********4.按成绩排序***********" << endl;
	cout << "*********5.退出系统*************" << endl;
	cout << "********************************" << endl;
}

void Supervisor::WelcomeSupervisor()
{
	cout << "********************************" << endl;
	cout << "****学生信息管理系统导员部分****" << endl;
	cout << "*********请选择功能列表*********" << endl;
	cout << "*********1.录入学生信息*********" << endl;
	cout << "*********2.打印学生信息*********" << endl;
	cout << "*********3.查找学生信息*********" << endl;
	cout << "**4.修改学生除了成绩的其它信息**" << endl;
	cout << "*********5.删除学生信息*********" << endl;
	cout << "*********6.按成绩排序***********" << endl;
	cout << "*********7.退出系统*************" << endl;
	cout << "********************************" << endl;
}

void Manager::WelcomeManager()
{
	cout << "********************************" << endl;
	cout << "***学生信息管理系统管理员部分****" << endl;
	cout << "*********请选择功能列表*********" << endl;
	cout << "*********1.录入学生信息*********" << endl;
	cout << "*********2.删除学生信息*********" << endl;
	cout << "*********3.录入教师信息*********" << endl;
	cout << "*********4.删除教师信息*********" << endl;
	cout << "*********5.录入导员信息*********" << endl;
	cout << "*********6.删除导员信息*********" << endl;
	cout << "*********7.退出系统*************" << endl;
	cout << "********************************" << endl;
}

void Node::FindStudentByStudent(Node* head, string ID)
{
	Node* move = head->next;
	while (move != NULL)
	{
		if ((ID == move->student.ID))
		{
			cout << "名字    学号        年龄   C语言   C++    高数    大物    组原    英语    学分成绩" << endl;
			cout << setiosflags(ios::fixed);
			cout << setprecision(1) << move->student.name << "  " << move->student.ID
				<< "  " << move->student.age << "     " << move->student.C<<"    " 
				<<move->student.Cplusplus<<"   " <<move->student.math<<"    " 
				<<move->student.physics<<"    " <<move->student.zuyuan<<"    " 
				<<move->student.English<<"    " <<move->student.score <<endl;
			return;
		}
		move = move->next;
	}
	cout << "学号或密码输入错误，未找到该学生信息！" << endl;
}

void Node::ModifyStudentPassword(Node* head, string ID)
{
	Node* move = head->next;
	while (move != NULL)
	{
		if (move->student.ID == ID)
		{
			string password1;
			cout << "请输入新密码:" << endl;
			cin >> password1;
			string password2;
			cout << "请输入新密码:" << endl;
			cin >> password2;
			if (password1 == password2)
			{
				move->student.password = password1;
				cout << "修改成功！" << endl;
				SaveStudent(head);
				break;
			}
			else
			{
				cout << "两次输入的密码不一致，请重新输入：" << endl;
				move = head;
			}
		}
		move = move->next;
	}
}

void Teacher::PrintAllStudent(Node* head)
{
	Node* move = head->next;
	cout << "名字    学号        年龄   C语言   C++    高数    大物    组原    英语    学分成绩" << endl;
	while (move != NULL) 
	{
		if (move->student.ID.length() > 5)
		{

			cout << setiosflags(ios::fixed);
			cout << setprecision(1) << move->student.name << "  " << move->student.ID
				<< "  " << move->student.age << "     " << move->student.C << "    "
				<< move->student.Cplusplus << "   " << move->student.math << "    "
				<< move->student.physics << "    " << move->student.zuyuan << "    "
				<< move->student.English << "    " << move->student.score << endl;
		}
		move = move->next;
	}
}

void Teacher::FindStudentByTeacherorSupervisor(Node* head)
{
	cout << "请输入要查找的学生的学号：" << endl;
	string StuID;
	cin >> StuID;
	Node* move = head->next;
	while (move != NULL)
	{
		if (StuID == move->student.ID)
		{
			cout << "恭喜您输入成功，学生信息如下：" << endl;
			cout << "名字    学号        年龄   C语言   C++    高数    大物    组原    英语    学分成绩" << endl;
			cout << setiosflags(ios::fixed);
			cout << setprecision(1) << move->student.name << "  " << move->student.ID
				<< "  " << move->student.age << "     " << move->student.C << "    "
				<< move->student.Cplusplus << "   " << move->student.math << "    "
				<< move->student.physics << "    " << move->student.zuyuan << "    "
				<< move->student.English << "    " << move->student.score << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该学生信息" << endl;
}

void Teacher::ModifyStudentScore(Node* head)
{
	string name;
	cout << "请输入您的名字：" << endl;
	cin >> name;
	if (name == "杨会君")
	{
		
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.C;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}
	else if (name == "孙海天")
	{
		
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.Cplusplus;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}
	else if (name == "张倩倩")
	{
		
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.math;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}
	else if (name == "安鸿昌")
	{
		
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.physics;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}
	else if (name == "黄铝文")
	{
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.zuyuan;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}
	else if (name == "田静")
	{
		
		cout << "请输入要修改的学生的学号：" << endl;
		string StuNum;
		cin >> StuNum;
		Node* move = head->next;
		while (move != NULL)
		{
			if (move->student.ID == StuNum)
			{
				cout << "请输入要修改的学生的成绩:" << endl;
				cin >> move->student.English;
				move->student.score = (move->student.C + move->student.Cplusplus + move->student.math
					+ move->student.physics + move->student.zuyuan + move->student.English) / 6;
				SaveStudent(head);
				cout << "修改成功！" << endl;
				return;
			}
			move = move->next;
		}
		cout << "未找到该学生信息" << endl;
	}

}

void Teacher::SortStudent(Node* head)
{
	Node* save = NULL;
	Node* move = NULL;
	for (Node* turn = head->next; turn->next != NULL; turn = turn->next)
	{
		for (move = head->next; move->next != save; move = move->next)
		{
			if (move->student.score < move->next->student.score)
			{
				Student tmp = move->student;
				move->student = move->next->student;
				move->next->student = tmp;
			}
		}
		save = move;
	}
	SaveStudent(head);
	PrintAllStudent(head);
}

void Supervisor::PrintAllStudent(Node* head)
{
	Node* move = head->next;
	cout << "名字    学号        年龄   C语言   C++    高数    大物    组原    英语    学分成绩" << endl;
	while (move != NULL)
	{
		if (move->student.ID.length() > 5)
		{

			cout << setiosflags(ios::fixed);
			cout << setprecision(1) << move->student.name << "  " << move->student.ID
				<< "  " << move->student.age << "     " << move->student.C << "    "
				<< move->student.Cplusplus << "   " << move->student.math << "    "
				<< move->student.physics << "    " << move->student.zuyuan << "    "
				<< move->student.English << "    " << move->student.score << endl;
		}
		move = move->next;
	}
}

void Supervisor::InsertStudent(Node* head)
{
	Node* fresh = new Node;
	fresh->next = NULL;
	cout << "请依次输入学生的姓名，学号，年龄，C语言成绩，C++成绩，高数成绩，大物成绩，组原成绩，英语成绩" << endl;
	cin >> fresh->student.name >> fresh->student.ID >> fresh->student.age >>
		fresh->student.C>>fresh->student.Cplusplus>>fresh->student.math
		>>fresh->student.physics>>fresh->student.zuyuan>>fresh->student.English;
	Node* move = head;
	while (move->next != NULL)
	{
		move = move->next;
	}
	move->next = fresh;
	cout << "添加成功！" << endl;
	SaveStudent(head);
}

void Supervisor::FindStudentByTeacherorSupervisor(Node* head)
{
	cout << "请输入要查找的学生的学号：" << endl;
	string StuID;
	cin >> StuID;
	Node* move = head->next;
	while (move != NULL)
	{
		if (StuID == move->student.ID)
		{
			cout << "恭喜您输入成功，学生信息如下：" << endl;
			cout << "名字    学号        年龄   C语言   C++    高数    大物    组原    英语    学分成绩" << endl;
			cout << setiosflags(ios::fixed);
			cout << setprecision(1) << move->student.name << "  " << move->student.ID
				<< "  " << move->student.age << "     " << move->student.C << "    "
				<< move->student.Cplusplus << "   " << move->student.math << "    "
				<< move->student.physics << "    " << move->student.zuyuan << "    "
				<< move->student.English << "    " << move->student.score << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该学生信息" << endl;
}

void Supervisor::ModifyStudentOtherInformation(Node* head)
{
	cout << "请输入要修改的学生的学号：" << endl;
	string StuNum;
	cin >> StuNum;
	Node* move = head->next;
	while (move != NULL)
	{
		if (move->student.ID == StuNum)
		{
			cout << "请输入要修改的学生的姓名，学号，年龄:" << endl;
			cin >> move->student.name >> move->student.ID >> move->student.age;
			SaveStudent(head);
			cout << "修改成功！" << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该学生信息" << endl;
}

void Supervisor::DeleteStudent(Node* head)
{
	cout << "请输入要删除的学生的学号：" << endl;
	string StuNum;
	cin >> StuNum;
	Node* move = head;
	while (move->next != NULL)
	{
		if (move->next->student.ID == StuNum)
		{
			Node* tmp = move->next;
			move->next = move->next->next;
			free(tmp);
			tmp = NULL;
			SaveStudent(head);
			cout << "删除学生成功！" << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该学生信息" << endl;
}

void Supervisor::SortStudent(Node* head)
{
	Node* save = NULL;
	Node* move = NULL;
	for (Node* turn = head->next; turn->next != NULL; turn = turn->next)
	{
		for (move = head->next; move->next != save; move = move->next)
		{
			if (move->student.score < move->next->student.score)
			{
				Student tmp = move->student;
				move->student = move->next->student;
				move->next->student = tmp;
			}
		}
		save = move;
	}
	SaveStudent(head);
	PrintAllStudent(head);
}


void SaveStudent(Node* head)
{ 
	FILE* file = fopen("./stu.txt", "w");
	if (!file)
	{
		cout << "未找到该文件，跳过读取" << endl;
		return;
	}
	Node* move = head->next;
	while (move != NULL)
	{
		if (fwrite(&move->student, sizeof(Student), 1, file) != 1)
		{
			cout << "写入失败" << endl;
			return;
		}
		move = move->next;
	}
	fclose(file);
}

void LoadStudent(Node* head)
{
	FILE* file = fopen("./stu.txt", "r");
	if (!file)
	{
		cout << "未找到该文件，跳过读取" << endl;
		return;
	}
	Node* fresh = new Node ;
	fresh->next = NULL;
	Node* move = head;
	while (fread(&fresh->student, sizeof(Student), 1, file) == 1)
	{
		move->next = fresh;
		move = fresh;
		fresh = new Node;
		fresh->next = NULL;
	}
	//delete fresh;
	fclose(file);
	cout << "读取成功" << endl;
}

void Manager::InsertTeacher(Node* head)
{
	Node* fresh = new Node;
	fresh->next = NULL;
	cout << "请输入老师的姓名，ID，年龄，密码：" << endl;
	cin >> fresh->student.name >> fresh->student.ID >> fresh->student.age>>fresh->student.password;
	Node* move = head;
	while (move->next != NULL)
	{
		move = move->next;
	}
	move->next = fresh;
	SaveStudent(head);
	cout << "添加教师成功！" << endl;
}

void Manager::InsertSupervisor(Node* head)
{
	Node* fresh = new Node;
	fresh->next = NULL;
	cout << "请输入导员的姓名，ID，年龄，密码：" << endl;
	cin >> fresh->student.name >> fresh->student.ID >> fresh->student.age>>fresh->student.password;
	Node* move = head;
	while (move->next != NULL)
	{
		move = move->next;
	}
	move->next = fresh;
	SaveStudent(head);
	cout << "添加教师成功！" << endl;
}

void Manager::DeleteTeacher(Node* head)
{
	cout << "请输入要删除的教师的账号：" << endl;
	string TeaID;
	cin >> TeaID;
	Node* move = head;
	while (move->next != NULL)
	{
		if (move->next->student.ID == TeaID)
		{
			Node* tmp = move->next;
			move->next = move->next->next;
			free(tmp);
			tmp = NULL;
			SaveStudent(head);
			cout << "删除教师成功！" << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该教师的信息" << endl;
}


void Manager::DeleteSupervisor(Node* head)
{
	cout << "请输入要删除的导员的账号：" << endl;
	string SupID;
	cin >> SupID;
	Node* move = head;
	while (move->next != NULL)
	{
		if (move->next->student.ID == SupID)
		{
			Node* tmp = move->next;
			move->next = move->next->next;
			free(tmp);
			tmp = NULL;
			SaveStudent(head);
			cout << "删除导员成功！" << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该导员的信息" << endl;
}

void Manager::InsertStudent(Node* head)
{
	Node* fresh = new Node;
	fresh->next = NULL;
	cout << "请输入学生的姓名，学号，年龄，学分成绩" << endl;
	cin >> fresh->student.name >> fresh->student.ID >> fresh->student.age >>
		fresh->student.C >> fresh->student.Cplusplus >> fresh->student.math
		>> fresh->student.physics >> fresh->student.zuyuan >> fresh->student.English;
	Node* move = head;
	while (move->next != NULL)
	{
		move = move->next;
	}
	move->next = fresh;
	cout << "添加学生成功！" << endl;
	SaveStudent(head);
}

void Manager::DeleteStudent(Node* head)
{
	cout << "请输入要删除的学生的学号：" << endl;
	string StuNum;
	cin >> StuNum;
	Node* move = head;
	while (move->next != NULL)
	{
		if (move->next->student.ID == StuNum)
		{
			Node* tmp = move->next;
			move->next = move->next->next;
			free(tmp);
			tmp = NULL;
			SaveStudent(head);
			cout << "删除学生成功！" << endl;
			return;
		}
		move = move->next;
	}
	cout << "未找到该学生信息" << endl;
}

























