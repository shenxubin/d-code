#pragma once

#include <iostream>
#include "Base.h"
#include "Configure.h"
using namespace std;
class PushButton :public Base
{
public:
	PushButton(const string& text = "Button", int x = 0, int y = 0, int w = 100, int h = 30);
	void show();
private:
	string m_text;
	ExMessage m_msg;
};

