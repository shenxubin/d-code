#pragma once
#include "Configure.h"
#include <iostream>
using namespace std;
class Window
{
public:
	Window(int w, int h, int flag);
	void setWindowTitle(const string& title);
	int exec();

	static int width();//获取窗口宽度
	static int height();//获取窗口高度
	static void clear();//清屏
	static void beginDraw();
	static void flushDraw();
	static void endDraw();

	//按键操作和鼠标操作
	//判断有没有消息
	inline static bool hasMsg() 
	{
		return::peekmessage(&m_msg, EX_MOUSE | EX_KEY);
		//获取成功写进m_msg结构体
	}
	inline static const ExMessage& getMsg()
	{
		return m_msg;
	}
private:
	HWND m_handle;//窗口句柄
	static ExMessage m_msg;//鼠标消息和键盘消息
};