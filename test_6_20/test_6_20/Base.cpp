#include "Base.h"

Base::Base(int x, int y, int w, int h)
{
	this->m_x = x;
	this->m_y = y;
	this->m_w = w;
	this->m_h = h;
}
int Base::width()
{
	return m_w;
}
int Base::height()
{
	return m_h;
}
void Base::setFixedSize(int w, int h)
{
	this->m_w = w;
	this->m_h = h;
}

int Base::x()
{
	return m_x;
}

int Base::y()
{
	return m_y;
}

void Base::move(int x, int y)
{
	this->m_x = x;
	this->m_y = y;
}







