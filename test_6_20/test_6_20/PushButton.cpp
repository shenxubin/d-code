#include "PushButton.h"

PushButton::PushButton(const string& text, int x, int y, int w, int h)
	:Base(x, y, w, h)
{
	this->m_text = text;
}

void PushButton::show()//设置字体，颜色
{
	setfillcolor(RGB(232, 232, 236));
	::fillroundrect(m_x, m_y, m_x + m_w, m_y + m_h, 10, 10);
	settextcolor(BLACK);
	//把文字居中显示在按钮中间

	int tx = m_x + (m_w - textwidth(m_text.c_str())) / 2;
	int ty = m_y + (m_h - textheight(m_text.c_str())) / 2;
	::outtextxy(tx, ty, m_text.c_str());
}