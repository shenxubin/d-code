#pragma once

#include <iostream>
#include <conio.h>
#include <vector>
#include "Window.h"
#include "PushButton.h"
using namespace std;
class Management
{
public:
	Management();
	enum Operator
	{
		Display,
		Add,
		Erase,
		Modify,
		Search,
		Menu = 66
	};
	//启动管理类
	void run();

	int menu();
	//1.显示所有学生
	void display();
	//2.添加
	void add();
	//3.删除
	void erase();
	//4.修改
	void modify();
	//5.查找
	void search();

	void drawBackground();

private:
	IMAGE m_bk;
	ExMessage m_msg;
	vector<PushButton*>menu_btns;
};