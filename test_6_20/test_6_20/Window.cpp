#include "Window.h"
ExMessage Window::m_msg;
Window::Window(int w, int h, int flag)
{
	m_handle = ::initgraph(w, h, flag);
	::setbkmode(TRANSPARENT);
}

int Window::exec()
{

	return getchar();
}

int Window::width()
{
	return ::getwidth();
}//获取窗口宽度
int Window::height()
{
	return ::getheight();
}//获取窗口高度
void Window::clear()
{
	::cleardevice();
}//清屏
void Window::beginDraw()
{
	::BeginBatchDraw();
}
void Window::flushDraw()
{
	::FlushBatchDraw();
}
void Window::endDraw()
{
	::EndBatchDraw();
}

void Window::setWindowTitle(const string& title)
{
	::SetWindowText(m_handle, title.c_str());
}