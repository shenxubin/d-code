#include "Manage.h"
#include "Window.h"

Management::Management()
{
	::loadimage(&m_bk, "./images/bg.jpg", Window::width(), Window::height());
	//主界面按钮初始化
	menu_btns.push_back(new PushButton("查看学生"));
	menu_btns.push_back(new PushButton("添加学生"));
	menu_btns.push_back(new PushButton("删除学生"));
	menu_btns.push_back(new PushButton("修改学生"));
	menu_btns.push_back(new PushButton("查找学生"));
	menu_btns.push_back(new PushButton("退出系统"));
	for (int i = 0; i < menu_btns.size(); i++)
	{
		menu_btns[i]->setFixedSize(250, 50);
		int bx = (Window::width() - menu_btns[i]->width()) / 2;
		int vspace = (Window::height() - menu_btns.size() * menu_btns[i]->height()) / 2;
		int by =vspace+ i * menu_btns[i]->height();

		menu_btns[i]->move(bx, by);
	}
}
void Management::run() 
{
	int op = Menu;
	Window::beginDraw();
	while (true)
	{
		Window::clear();
		drawBackground();
		if (Window::hasMsg())
		{
			m_msg = Window::getMsg();
			switch (m_msg.message)
			{
			case WM_KEYDOWN://按键按下
				//按ESC退出操作，返回主界面
				if (m_msg.vkcode == VK_ESCAPE)
				{
					op = Menu;
				}
				break;
			default:
				break;
			}
		}
		if (_kbhit())
		{
			if (_getch() == 27)
			{
				op = Menu;
			}
		}
		switch (op)
		{
		case Management::Menu:
			op = menu(); 
			break;
		case Management::Display: 
			display();
			break;
		case Management::Add: 
			add(); 
			break;
		case Management::Erase: 
			erase(); 
			break;
		case Management::Modify: 
			modify();
			break;
		case Management::Search: 
			search(); 
			break;
		default:
			break;
		}
		Window::flushDraw();
	}
	Window::endDraw();
}

int Management::menu()//按钮个数设置
{
	for (int i = 0; i < menu_btns.size(); i++)
	{
		menu_btns[i]->show();
		if (menu_btns[i])
		{
			return i;
		}
	}
	return Menu;
}

void Management::display()
{
	outtextxy(0, 0, "display");
	cout << "display" << endl;
}

void Management::add()
{
	outtextxy(0, 0, "add");
	cout << "add" << endl;
}

void Management::erase()
{
	outtextxy(0, 0, "erase");
	cout << "erase" << endl;
}

void Management::modify()
{
	outtextxy(0, 0, "modify");
	cout << "modify" << endl;
}

void Management::search()
{
	outtextxy(0, 0, "search");
	cout << "search" << endl;
}

void Management::drawBackground()
{
	::putimage(0, 0, &m_bk);
}
