#include "BinaryTree.h"
#include "Queue.h"


BTNode* buyNode(int x)
{
	BTNode* tmp = (BTNode*)malloc(sizeof(BTNode));
	if (tmp == NULL)
	{
		perror("malloc fail");
		exit(-1);
	}
	tmp->_data = x;
	tmp->_left = NULL;
	tmp->_right = NULL;
	return tmp;
}




BTNode* CompleteBinaryTreeCreate(BTDataType* a, int n)
{
	BTNode* cur[100] = { NULL };


	for (int i = 0; i < 100; i++)
	{
		BTNode* cur1 = (BTNode*)malloc(sizeof(BTNode));
		cur1->_data = 0;
		cur1->_left = NULL;
		cur1->_right = NULL;
		cur[i] = cur1;
	}
	for (int i = 0; i < n; i++)
	{
		cur[i]->_data = a[i];
	}
	int child = n - 1;
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (child % 2 == 0)
		{
			cur[parent]->_right = cur[child];
		}
		else
		{
			cur[parent]->_left = cur[child];
		}
		child--;
		parent = (child - 1) / 2;
	}
	return cur[0];
}


void BinaryTreeDestory(BTNode* root)
{
	if (root == NULL)
		return;


	BinaryTreeDestory(root->_left);
	BinaryTreeDestory(root->_right);
	free(root);
}


int BinaryTreeSize(BTNode* root)
{
	return root == NULL ? 0 :
		BinaryTreeSize(root->_left) + BinaryTreeSize(root->_right) + 1;
}


int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
	{
		return 0;
	}
	if (root->_left != NULL || root->_right != NULL)
	{
		return BinaryTreeLeafSize(root->_left)
			+ BinaryTreeLeafSize(root->_right);
	}
	else
	{
		return 1;
	}


}


int BinaryTreeLevelKSize(BTNode* root, int k)
{
	assert(k > 0);
	if (root == NULL)
		return 0;
	if (k == 1)
		return 1;
	return BinaryTreeLevelKSize(root->_left, k - 1)
		+ BinaryTreeLevelKSize(root->_right, k - 1);
}




BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	if (root->_data == x)
		return root;
	if (root == NULL)
		return NULL;


	BTNode* cur = BinaryTreeFind(root->_left, x);
	if (cur && cur->_data == x)
	{
		return cur;
	}
	BTNode* cur2 = BinaryTreeFind(root->_right, x);
	if (cur2 && cur2->_data == x)
	{
		return cur;
	}
	return NULL;
}




void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%d ", root->_data);
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
}


void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeInOrder(root->_left);
	printf("%d ", root->_data);
	BinaryTreeInOrder(root->_right);
}


void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePostOrder(root->_left);
	BinaryTreePostOrder(root->_right);
	printf("%d ", root->_data);
}




void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);
	while (QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		printf("%d ", front->_data);
		if (front->_left)
			QueuePush(&q, front->_left);
		if (front->_right)
			QueuePush(&q, front->_right);
		QueuePop(&q);
	}
	printf("\n");


	QueueDestroy(&q);
}




int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	QueueInit(&q);
	if (root)
		QueuePush(&q, root);
	while (QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		if (front == NULL)
			break;
		QueuePush(&q, front->_left);
		QueuePush(&q, front->_right);
		QueuePop(&q);
	}
	// 已经遇到空节点，如果队列中后面的节点还有非空，就不是完全二叉树
	while (QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);
		QueuePop(&q);


		if (front != NULL)
		{
			QueueDestroy(&q);
			return 0;
		}
	}
	QueueDestroy(&q);
	return 1;
}