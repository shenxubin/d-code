
//构建二叉树，分别输出前序，中序，后序遍历的结果

#include <iostream>
#include <memory>
using namespace std;

struct BTNode
{
    int val;
    BTNode* left;
    BTNode* right;
};


BTNode* buyBTNode(int x)
{
    BTNode* node = new BTNode;
    node->left = nullptr;
    node->right = nullptr;
    node->val = x;
    return node;
}

void CreatBinaryTree(BTNode** tmp, int n)
{
    int child = n - 1;
    int parent = (child - 1) / 2;
    while (child > 0)
    {
        if (child % 2 == 0)
        {
            tmp[parent]->right = tmp[child];
        }
        else
        {
            tmp[parent]->left = tmp[child];
        }
        child--;
        parent = (child - 1) / 2;
    }
}

void BinaryTreePrevOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    if (root->val != 0)
        cout << root->val << " ";
    BinaryTreePrevOrder(root->left);
    BinaryTreePrevOrder(root->right);
}


void BinaryTreeInOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreeInOrder(root->left);
    if (root->val != 0)
        cout << root->val << " ";
    BinaryTreeInOrder(root->right);
}


void BinaryTreePostOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreePostOrder(root->left);
    BinaryTreePostOrder(root->right);
    if (root->val != 0)
        cout << root->val << " ";
}

void BinaryTreeDestroy(BTNode* root)
{
    if (root == nullptr)
    {
        return;
    }
    BinaryTreeDestroy(root->left);
    BinaryTreeDestroy(root->right);
    delete root;
}



int main()
{
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    BTNode* tmp[n];
    for (int i = 0; i < n; i++)
    {
        tmp[i] = buyBTNode(arr[i]);
    }

    CreatBinaryTree(tmp, n);
    /*int child = n - 1;
    int parent = (child - 1) / 2;
    while (child > 0)
    {
        if (child % 2 == 0)
        {
            tmp[parent]->right = tmp[child];
        }
        else
        {
            tmp[parent]->left = tmp[child];
        }
        child--;
        parent = (child - 1) / 2;
    }*/


    BinaryTreePrevOrder(tmp[0]);
    cout << endl;
    BinaryTreeInOrder(tmp[0]);
    cout << endl;
    BinaryTreePostOrder(tmp[0]);
    cout << endl;
    BinaryTreeDestroy(tmp[0]);
    return 0;
}