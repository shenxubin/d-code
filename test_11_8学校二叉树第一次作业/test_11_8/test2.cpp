//从先序中序重构二叉树按层序后序输出
#include <iostream>
#include <queue>
using namespace std;

struct BTNode
{
    int val;
    BTNode* left;
    BTNode* right;
};

void CreatBTree(BTNode*& root, int* Pre, int* In, int L1, int R1, int L2, int R2)
{
    root = new BTNode;
    root->val = Pre[L1];

    //找中序根节点的位置
    int position;
    for (position = 0; position <= R2; position++)
    {
        if (Pre[L1] == In[position])
        {
            break;
        }
    }

    //判断中序序列根节点的左边是否存在左子序列
    if (position != L2)
    {
        CreatBTree(root->left, Pre, In, L1 + 1, L1 + (position - L2), L2, position - 1);
    }
    else
    {
        root->left = nullptr;
    }
    //判断中序序列根节点的右边是否存在右子序列
    if (position != R2)
    {
        CreatBTree(root->right, Pre, In, R1 - (R2 - position) + 1, R1, position + 1, R2);
    }
    else
    {
        root->right = nullptr;
    }
}


void BinaryTreePostOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreePostOrder(root->left);
    BinaryTreePostOrder(root->right);
    cout << root->val << " ";
}


void BinaryTreelevelOrder(BTNode* root)
{
    queue<BTNode*> q;
    if (root)
        q.push(root);
    while (!q.empty())
    {
        BTNode* tmp = q.front();
        cout << tmp->val << " ";
        if (tmp->left)
            q.push(tmp->left);
        if (tmp->right)
            q.push(tmp->right);
        q.pop();
    }
}


void BinaryTreeDestroy(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreeDestroy(root->left);
    BinaryTreeDestroy(root->right);
    delete root;
}

int main()
{
    int n;
    cin >> n;
    int arrPrev[n];
    int arrIn[n];
    for (int i = 0; i < n; i++)
    {
        cin >> arrPrev[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> arrIn[i];
    }
    BTNode* root;
    CreatBTree(root, arrPrev, arrIn, 0, n - 1, 0, n - 1);

    BinaryTreelevelOrder(root);
    cout << endl;
    BinaryTreePostOrder(root);
    cout << endl;
    BinaryTreeDestroy(root);

    return 0;
}