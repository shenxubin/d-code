//树的括号表示法：给定一棵树的括号表示法，输出层序遍历的结果

#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

struct node
{
    char ch;
    int num;
};

vector<node*> st1;

node* buyNode(char ch)
{
    node* tmp = new node;
    tmp->ch = ch;
    tmp->num = 0;
    return tmp;
}

int main()
{
    char arr[1000];
    cin >> arr;
    int len = strlen(arr);
    int charnum = 0, charlayers = 0;
    for (int i = 0; i < len; i++)
    {
        if (arr[i] == '(')
        {
            charnum++;
        }
        else if (arr[i] == ')')
        {
            charnum--;
        }
        if (arr[i] != '(' && arr[i] != ')' && arr[i] != ',')
        {
            if (charlayers < charnum)
            {
                charlayers = charnum;
            }
            node* tmp = buyNode(arr[i]);
            tmp->num = charnum;
            st1.push_back(tmp);
        }
    }
    for (int i = 1; i <= charlayers; i++)
    {
        for (int j = 0; j < st1.size(); j++)
        {
            if (st1[j]->num == i)
            {
                cout << st1[j]->ch;
            }
        }
    }
    cout << endl;
    return 0;
}