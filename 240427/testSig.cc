#include <iostream>
#include <signal.h>
#include <unistd.h>

void print(const sigset_t& pending)
{
    for(int i = 31; i>=1; i--)
    {
        if(sigismember(&pending, i))
            std::cout << "1";
        else
            std::cout << "0";
    }
    std::cout << std::endl;
}

int main()
{
    std::cout << "pid: " << getpid() << std::endl; 
    sigset_t set, oldset;
    sigemptyset(&set);
    sigemptyset(&oldset);

    sigaddset(&set, 2);//将2号信号加入到要屏蔽的信号集中

    //设置set进block位图中
    int n = sigprocmask(SIG_SETMASK, &set, &oldset);
    if(n == -1)
    {
        std::cout << "设置屏蔽字错误！" << std::endl;
        return 1;
    }

    int cnt = 0;

    //查看pending位图，给进程发送2号信号，pending位图中应该出现2号信号，但是进程不会退出
    //等到20秒时程序退出
    while(true)
    {
        cnt++;
        if(cnt == 20)
            sigprocmask(SIG_UNBLOCK, &set, &oldset);
        sigset_t pending;
        sigemptyset(&pending);
        int m = sigpending(&pending);
        print(pending);
        sleep(1);
    }
    return 0;
}