//#include <iostream>
//using namespace std;
//
//class A 
//{
//public:
//	virtual void vfunc1() { cout << "A::vfunc1()" << endl; };
//	virtual void vfunc2() { cout << "A::vfunc2()" << endl; };
//	void func1() { cout << "A::func1()" << endl; };
//	void func2() { cout << "A::func2()" << endl; };
//private:
//	int data1_;
//	int data2_;
//};
//
//class B :public A {
//public:
//	virtual void vfunc1() override { cout << "B::vfunc1()" << endl; };
//	void func2() { cout << "B::func2()" << endl; };
//private:
//	int data3_;
//};
//
//class C :public B {
//public:
//	virtual void vfunc1() override { cout << "C::vfunc1()" << endl; };
//	void func2() { cout << "C::func2()" << endl; };
//private:
//	int data1_, data4_;
//};
//
////演示了手动调用虚函数的过程
//int main() {
//	B a;
//	typedef void(*Fun)(void);
//	Fun pFun = nullptr;
//	cout << "虚函数表地址：" << (int*)(&a) << endl;
//	cout << "虚函数表第1个函数地址：" << (int*)*(int*)(&a) << endl;
//	cout << "虚函数表第2个函数地址：" << (int*)*(int*)(&a) + 1 << endl;
//	pFun = (Fun) * ((int*)*(int*)(&a));
//	pFun();
//	pFun = (Fun) * ((int*)*(int*)(&a) + 1);
//	pFun();
//	return 0;
//}



//#include <iostream>
//using namespace std;
//
//template<class _T1, class _T2, class _T3>
//void Add(_T1 a, _T2 b, _T3& c)
//{
//    c = a + b;
//}
//
//template<class _T1, class _T2>
//_T1 Add(_T1 a, _T2 b)
//{
//    return a + b;
//}
//int main()
//{
//    int a, b;
//    cin >> a >> b;
//    int c = Add(a, b);
//    double e, f;
//    cin >> e >> f;
//    double g = Add(e, f);
//    cout << c << " " << g << endl;
//    return 0;
//}

//定义数组类
//#include <iostream>
//using namespace std;
//template <class _T = int, int length = 10>
//class Array
//{
//	_T arr[length];
//public:
//	Array(_T* a);
//	/*friend ostream& operator<<(ostream& cout, const Array<_T, length>& a)
//	{
//		for (int i = 0; i < length; i++)
//		{
//			cout << a.arr[i] << " ";
//		}
//		cout << endl;
//		return cout;
//	}*/
//	
//	template<class _Ty, int Size>
//	friend ostream& operator<<(ostream& out, const Array<_Ty, Size>& obj);
//};
//
//template <class _T, int length>
//ostream& operator<<(ostream& cout,const Array<_T, length>& a)
//{
//	for (int i = 0; i < length; i++)
//	{
//		cout << a.arr[i] << " ";
//	}
//	cout << endl;
//	return cout;
//}
//
//template<class _T, int length>
//Array<_T, length>::Array(_T* a)
//{
//	for (int i = 0; i < length; i++)
//	{
//		this->arr[i] = a[i];
//	}
//}
//
//int main()
//{
//	int* a = new int[10];
//	for (int i = 0; i < 10; i++)
//	{
//		a[i] = i;
//	}
//	double b[] = { 1.1,2.2,3.3,4.4,5.5 };
//	Array<int , 10> arr1(a);
//	Array<double, 5>arr2(b);
//	cout << arr1 << arr2;
//	return 0;
//}

#include <iostream>
using namespace std;
template<class _T>
class Node
{
public:
	_T data;
	Node<_T>* next;
	Node(_T data = 0);
};
template<class _T>
Node<_T>::Node(_T data) {
	this->data = data;
	this->next = NULL;
}

template<class _T>
class LinkedList 
{
	Node<_T>* head;
public:
	LinkedList();
	LinkedList(const LinkedList<_T>& obj);
	~LinkedList();
	void TailInsert(_T data);
	bool HeadDelete(_T& data);
	const LinkedList<_T>& operator=(const LinkedList<_T>& obj);
	const LinkedList<_T> operator-();
	_T& operator[](int x);

	template<class _Tf>
	friend const LinkedList<_Tf> operator*(int left,const LinkedList<_Tf>& right);

	template<class _Tf>
	friend const LinkedList<_Tf> operator+(const LinkedList<_Tf>& left,const LinkedList<_Tf>& right);

	template<class _Tf>
	friend ostream& operator<<(ostream& out,const LinkedList<_Tf>& obj);
};


template<class _T>
LinkedList<_T>::LinkedList() 
{
	this->head = new Node<_T>;
}
template<class _T>
LinkedList<_T>::~LinkedList() 
{
	_T data;
	while (HeadDelete(data)) {}
	delete this->head;
}












