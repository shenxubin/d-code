//字符串中的第一个唯一字符

class Solution {
public:
    int firstUniqChar(string s)
    {

        int len = s.length();
        int arr[26] = { 0 };
        for (int i = 0; i < s.length(); i++)
        {
            arr[s[i] - 97]++;
        }
        char x = 0;
        int onenum = 0;
        for (int i = 0; i < 26; i++)
        {
            if (arr[i] == 1)
                onenum++;
        }
        if (onenum == 0)
            return -1;
        char onenumArr[onenum];
        int j = 0;
        for (int i = 0; i < 26; i++)
        {
            if (arr[i] == 1)
                onenumArr[j++] = i + 97;
        }
        int appear[onenum];
        int y = 0;
        for (int i = 0; i < len; i++)
        {
            for (int k = 0; k < j; k++)
            {
                if (s[i] == onenumArr[k])
                    appear[y++] = i;
                if (y == 1)
                    return i;
            }
        }

        int min = appear[0];
        // for(int i = 0; i<j; i++)
        // {
        //     if(min > appear[i])
        //     min = appear[i];
        // }
        return min;

    }
};