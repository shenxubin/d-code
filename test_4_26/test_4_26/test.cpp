#include <iostream>
using namespace std;

class Complex
{
    friend Complex Multiply(const Complex& a, double b);
    friend Complex Multiply(const Complex& a, const Complex& b);
    int real;
    int imag;
public:

    Complex(int real = 0, int imag = 0)
    {
        this->real = real;
        this->imag = imag;
    }
    void Print()
    {
        cout << real << "+" << imag << "i" << endl;
    }
    ~Complex()
    {

    }
};

Complex Multiply(const Complex& a, const Complex& b)
{
    Complex c;
    c.real = a.real * b.real - a.imag * b.imag;
    c.imag = a.imag * b.real + a.real * b.imag;
    return c;
}
Complex Multiply(const Complex& a, double b)
{
    Complex c;
    c.real = a.real * b;
    c.imag = a.imag * b;
    return c;
}

int main()
{


    return 0;
}