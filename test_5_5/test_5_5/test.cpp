#include <iostream>
#include <vector>
using namespace std;

template<class _T>
class Vectors
{
    vector<_T> error_thrown;
public:
    
    vector<vector<_T>> arr;
    Vectors() :arr() {}
    Vectors(vector<vector<_T>> arr)
    {
        this->arr = arr;
    }
    vector<_T>& operator[](const int index)
    {
        if (index >(int) this->arr.size())
        {
            
            cout << " Out of boundary!" << endl;
            return this->error_thrown;
        }
        return this->arr[index];
    }

    template<class _Ty>
    friend ostream& operator<<(ostream& out, Vectors<_Ty>& v);
};

template<class _T>
ostream& operator<<(ostream& out, Vectors<_T>& v)
{
    for (int i = 0; i <(int) v.arr.size(); i++)
    {
        for (int j = 0; j < (int)v.arr[i].size(); j++)
        {
            out << v[i][j] << " ";
        }
        out << endl;
    }
    return out;
}
template <class _T>
class Vector : public vector<_T>
{
public:
    Vector() :vector<_T>() {} 

    template<class _Ty>
    friend ostream& operator<<(ostream& out, Vector<_Ty>& v);
};

template <class _T>
ostream& operator<<(ostream& out, Vector<_T>& v)
{
    for (int i = 0; i < (int)v.size(); i++)
    {
        out << v[i] << " ";
    }
    out << endl;
    return out;
}
int main()
{
    vector<vector<int>> a;
    a.push_back(vector<int>(5, 5));
    a.push_back(vector<int>(6, 6));
    a.push_back(vector<int>(7, 7));
    Vectors<int> v(a);
    cout << v;
    Vector<int> v2;
    for (int i = 0; i < v[1].size(); i++)
    {
        v2.push_back(v[1][i]);
    }
    cout << v2;
    return 0;
}