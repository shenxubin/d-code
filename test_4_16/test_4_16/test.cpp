
#include "Date.h"

void TestDate1()
{
    Date d1(2023, 4, 18);
    d1 += 4;
    d1.Print();
    d1 += 40;
    d1.Print();
    d1 += 400;
    d1.Print();
    d1 += 4000;
    d1.Print();
}
int main()
{
    TestDate1();

    return 0;
}