#include "Date.h"

bool Date::operator==(const Date& d)
{
	if ((this->_year == d._year) && (this->_month == d._month) && (this->_day == d._day))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date::operator!=(const Date& d)
{
	return !(*this == d);
}

bool Date::operator>(const Date& d)
{
	if ((this->_year > d._year) || (this->_year == d._year && this->_month > d._month)
		|| (this->_year == d._year && this->_month == d._month && this->_day > d._day))
	{
		return true;
	}
	return false;
}
bool Date::operator<(const Date& d)
{
	return !(*this >= d);
}

bool Date::operator<=(const Date& d)
{
	return !(*this > d);
}

bool Date::operator>=(const Date& d)
{
	return (*this > d) || (*this == d);
}

Date Date::operator+(int day)
{
	Date ret = *this;
	ret += day;
	return ret;
}
Date& Date::operator+=(int day)
{
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

void Date::Print()
{
	cout << _year << "/" << _month << "/" << _day << endl;
}

Date& Date::operator++()
{
	*this += 1;
	return *this;
}
Date Date::operator++(int)
{
	Date ret = *this;
	*this += 1;
	return ret;
}