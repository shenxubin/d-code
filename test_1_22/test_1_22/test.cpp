#include <cstdlib>
#include <iostream>
using namespace std;


void ShellSort(int* a, int n)
{
    int gap = n;
    while (gap > 1)
    {
        gap = gap / 3 + 1;
        for (int i = 0; i < n - gap; i++)
        {
            int endi = i;
            int tmp = a[endi + gap];
            while (endi >= 0)
            {
                if (tmp < a[endi])
                    a[endi + gap] = a[endi];
                else
                    break;
                endi -= gap;
            }
            a[endi + gap] = tmp;
        }
    }
}

int cmp(const void* p1, const void* p2)
{
    return *(int*)p1 - *(int*)p2;
}

int main()
{
    int n;
    cin >> n;
    int arr[100];
    for (int i = 0; i < 3 * n; i++)
    {
        cin >> arr[i];
    }
    //qsort(arr, 3*n, sizeof(int), cmp);
    ShellSort(arr, 3 * n);
    for (int i = 0; i < 3 * n; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
    int sum = 0, m = n;
    int num = 3 * m - 2;
    while (m)
    {
        sum += arr[num];
        m--;
        num -= 2;
    }
    cout << sum << endl;
    return 0;
}
// 64 λ������� printf("%lld")