#include <stdio.h>  
#include <stdlib.h>  
#include <dirent.h>  
#include <string.h>  
#include <sys/types.h>  
#include <sys/stat.h>  
#include <unistd.h>  
  
typedef struct {  
    int regular_files;  
    int directories;  
    int other_files;  // 包括链接文件、设备文件等  
} FileStats;  
  
FileStats count_file_types(const char *path) {  
    DIR *dir;  
    struct dirent *entry;  
    struct stat entry_stat;  
    FileStats stats = {0, 0, 0};  
  
    if (!(dir = opendir(path))) {  
        perror("opendir");  
        exit(EXIT_FAILURE);  
    }  
  
    while ((entry = readdir(dir)) != NULL) {  
        char full_path[1024];  
        snprintf(full_path, sizeof(full_path), "%s/%s", path, entry->d_name);  
  
        if (lstat(full_path, &entry_stat) == -1) {  
            perror("lstat");  
            continue;  
        }  
  
        if (S_ISREG(entry_stat.st_mode)) {  
            stats.regular_files++;  
        } else if (S_ISDIR(entry_stat.st_mode)) {  
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0) {  
                continue;  // 忽略当前目录和上级目录  
            }  
            stats.directories++;  
        } else {  
            stats.other_files++;  
        }  
    }  
  
    closedir(dir);  
    return stats;  
}  
  
int main() {  
    char path[1024];  
    printf("Enter the path of the directory: ");  
    scanf("%1023s", path);  // 防止缓冲区溢出  
  
    FileStats stats = count_file_types(path);  
  
    printf("Regular files: %d\n", stats.regular_files);  
    printf("Directories: %d\n", stats.directories);  
    printf("Other files: %d\n", stats.other_files);  
  
    return 0;  
}