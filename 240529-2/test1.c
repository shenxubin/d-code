#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>

int reg_count = 0;
int dir_count = 0;
int blk_count = 0;
int chr_count = 0;
int sock_count = 0;
int fifo_count = 0;
int link_count = 0;

void file_count(const char *pathname)
{
    // 打开这个目录
    DIR *dir;
    if ((dir = opendir(pathname)) == 0)
    {
        printf("目录打开失败！\n");
        exit(-1);
    }

    struct dirent *enter;
    struct stat enter_stat;
    // 读取目录的文件内容
    while ((enter = readdir(dir)) != NULL)
    {
        char pathname2[1024];
        snprintf(pathname2, sizeof(pathname2), "%s/%s", pathname, enter->d_name);
        // 获取文件属性
        if (stat(pathname2, &enter_stat) == -1)
        {
            printf("获取文件属性失败！\n");
            continue;
        }
        //判断文件类型
        if(S_ISREG(enter_stat.st_mode))
            reg_count++;
        else if(S_ISDIR(enter_stat.st_mode))
            dir_count++;
        else if(S_ISBLK(enter_stat.st_mode))
            blk_count++;
        else if(S_ISCHR(enter_stat.st_mode))
            chr_count++;
        else if(S_ISSOCK(enter_stat.st_mode))
            sock_count++;
        else if(S_ISFIFO(enter_stat.st_mode))
            fifo_count++;
        else if(S_ISLNK(enter_stat.st_mode))
            link_count++;
    }

    closedir(dir); 
}
int main()
{
    char pathname[1024];
    scanf("%s", pathname);

    file_count(pathname);

    printf("%s 目录下普通文件个数为：%d\n",pathname, reg_count) ;
    printf("目录文件个数为：%d\n", dir_count) ;
    printf("块设备文件个数为：%d\n", blk_count) ;
    printf("字符设备文件个数为：%d\n", chr_count) ;
    printf("套接字文件个数为：%d\n", sock_count) ;
    printf("管道文件个数为：%d\n", fifo_count) ;
    printf("链接文件个数为：%d\n", link_count) ;

    return 0;
}