//#include <iostream>
//using namespace std;
//
//class Rectangle
//{
//	friend ostream& operator<<(ostream& cout, Rectangle& r);
//private:
//	double x[4];
//	double y[4];
//public:
//	Rectangle(double* _x, double* _y)
//	{
//		for (int i = 0; i < 4; i++)
//		{
//			this->x[i] = _x[i];
//			this->y[i] = _y[i];
//		}
//	}
//	double l1 = sqrt((x[0] - x[1]) * (x[0] - x[1]) + (y[0] - y[1]) * (y[0] - y[1]));
//	double l2 = sqrt((x[1] - x[2]) * (x[1] - x[2]) + (y[1] - y[2]) * (y[1] - y[2]));
//	double l3 = sqrt((x[0] - x[2]) * (x[0] - x[2]) + (y[0] - y[2]) * (y[0] - y[2]));
//	double GetSideLength(int i)
//	{
//		if (i == 0 || i == 2)
//		{
//			return l1;
//		}
//		else if (i == 1 || i == 3)
//		{
//			return l2;
//		}
//	}
//	double GetSquare()
//	{
//		double cos = (l1 * l1 + l2 * l2 - l3 * l3) / (2 * l1 * l2);
//		double sin = sqrt(1 - cos * cos);
//		return l1 * l2 * sin;
//	}
//	
//};
//
//ostream& operator<<(ostream& cout, Rectangle& r)
//{
//	cout << r.GetSquare() << endl;
//	return cout;
//}
//
//int main()
//{
//	double x[4], y[4];
//	for (int i = 0; i < 4; i++)
//	{
//		cin >> x[i] >> y[i];
//	}
//	Rectangle r(x,y);
//	cout << r;
//	return 0;
//}

#include <iostream>
using namespace std;

template<class _T, int length>
class Set
{
private:
	_T arr[length];
public:
	Set(_T* a);
};

template<class _T, int length>
Set<_T, length>::Set(_T* a)
{
	for (int i = 0; i < length; i++)
	{
		this->arr[i] = a[i];
	}
}
int main()
{
	double arr[] = { 1.1,2.2,3.3,4.4,5.5 };
	Set<double, 5> s(arr);

	return 0;
}



//#include <iostream>
//using namespace std;
//
//template<class _T, int length>
//class Array
//{
//private:
//	_T arr[length];
//public:
//	template<class _Ty, int Size>
//	friend ostream& operator<<(ostream& cout, const Array<_Ty, Size>& obj);
//	
//};
//
//template<class _T, int length>
//ostream& operator<<(ostream& cout, const Array<_T, length> &obj)
//{
//	for (int i = 0; i < length; i++)
//	{
//		cout << obj.arr[i];
//	}
//	cout << endl;
//	return cout;
//}
//int main()
//{
//
//	return 0;
//}


//#include <iostream>
//#include <vector>
//#include <list>
//#include <map>
//using namespace std;
/*int main()
{
	vector<int> v;
	for (int i = 0; i < 6; i++)
	{
		v.push_back(i);
	}
	for (int i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	cout << endl;
	for (int i = 0; i < 3; i++)
	{
		v.pop_back();
	}
	for (int i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
	return 0;
}*/

//int main()
//{
//	list<int> mylist;
//	list<int>::iterator it;
//	for (int i = 1; i < 6; i++)
//	{
//		mylist.push_back(i);
//	}
//	it = mylist.begin();
//	++it;
//	mylist.insert(it, 10);
//	mylist.insert(it, 2, 20);
//	--it;
//	vector<int> myvector(2, 30);
//	mylist.insert(it, myvector.begin(), myvector.end());
//	for (it = mylist.begin(); it != mylist.end(); it++)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	return 0;
//}


//int main()
//{
//	map<char, int> m;
//	map<char, int>::iterator it;
//	int i;
//	char ch;
//	for (int i = 0; i < 26; i++)
//	{
//		m.insert(pair<char, int>('A' + i, 65 + i));
//	}
//	cout << "Enter key is : ";
//	cin >> ch;
//	it = m.find(ch);
//	if (it != m.end())
//	{
//		cout << "The ASCII VALUE is " << it->second<< endl;
//	}
//	else
//	{
//		cout << "The key is not is map" << endl;
//	}
//	return 0;
//}

//template<class _T>
//class Array
//{
//private:
//	_T* a;
//	int length;
//public:
//	Array(_T* arr, int Size);
//	
//	Array(const Array& arr)
//	{
//		length = arr.length;
//		a = new _T[length];
//		for (int i = 0; i < length; i++)
//		{
//			a[i] = arr.a[i];
//		}
//	}
//	_T GetNumber(int i)
//	{
//		return a[i];
//	}
//	int GetLength()
//	{
//		return length;
//	}
//	~Array()
//	{
//		delete[]a;
//	}
//};
//
//template<class _T>
//Array<_T>::Array(_T* arr, int Size)
//{
//	{
//		this->length = Size;
//		a = new _T[length];
//		for (int i = 0; i < length; i++)
//		{
//			a[i] = arr[i];
//		}
//	}
//}


//class Shape
//{
//public:
//	double x;
//	double y;
//public:
//	Shape(double _x, double _y)
//	{
//		this->x = _x;
//		this->y = _y;
//	}
//	virtual void Type() = 0;
//};
//
//class Triangle :public Shape
//{
//public:
//	Triangle(double _x, double _y) :Shape(_x, _y) {}
//	virtual void Type()
//	{
//		cout << "I am triangle(x, y)" << endl;
//	}
//};
//
//class Rectangle:public Shape
//{
//public:
//	Rectangle(double _x, double _y) :Shape(_x, _y) {}
//	virtual void Type()
//	{
//		cout << "I am Rectangle(x, y)" << endl;
//	}
//};
//
//
//int main()
//{	
//	int n;
//	cin >> n;
//	Shape* s[100], *p;
//	double x, y;
//	char ch;
//	for (int i = 0; i < n; i++)
//	{
//		cin >> ch >> x >> y;
//		if (ch == 'T')
//		{
//			s[i] = new Triangle(x, y);
//		}
//		else if (ch == 'R')
//		{
//			s[i] = new Rectangle(x, y);
//		}
//	}
//	for (int i = 0; i < n - 1; i++)
//	{
//		for (int j = 0; j < n-1-i; j++)
//		{
//			if (s[j]->x > s[j+1]->x)
//			{
//				p = s[j];
//				s[j] = s[j+1];
//				s[j+1] = p;
//			}
//		}
//	}
//	for (int i = 0; i < n; i++)
//	{
//		s[i]->Type();
//	}
//	for (int i = 0; i < n; i++)
//	{
//		delete s[i];
//	}
//	return 0;
//}


























