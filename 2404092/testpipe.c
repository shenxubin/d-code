#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>



void writer(int wfd)
{
    char buffer[128];
    int count = 0;
    while (1)
    {
        snprintf(buffer, sizeof(buffer), "I am your child!, pid:%d, count:%d\n", getpid(), count++);
        write(wfd, buffer, strlen(buffer));
        sleep(1);
    }
    
   
}

void reader(int rfd)
{
    char buffer[128];
   while (1)
   {
        read(rfd, buffer, sizeof(buffer));
        printf("father get message:%s", buffer);
   }
   
}

int main()
{
    int pipefd[2];
    int n = pipe(pipefd);

    int fd = fork();
    if(fd == 0)
    {
        //子进程充当写端，从而关闭读端
        close(pipefd[0]);
        writer(pipefd[1]);
        exit(0);
    }

    //父进程充当读端，从而关闭写端
    close(pipefd[1]);
    reader(pipefd[0]);
    wait(NULL);
    return 0;
}