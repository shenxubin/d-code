#include <iostream>
#include <vector>
using namespace std;

void testVector1()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testVector2()
{
	vector<int> v1(10, 1);
	vector<int>::iterator it = v1.begin();
	while (it != v1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v2(v1.begin(), v1.end());
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void testVector3()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	vector<int>::reverse_iterator it = v.rbegin();
	while (it != v.rend())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}


void testVector4()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	vector<int>::iterator pos = find(v.begin(), v.end(), 3);

	if (pos != v.end())
	{
		v.insert(pos, 20);
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	pos = find(v.begin(), v.end(), 20);
	if (pos != v.end())
	{
		v.erase(pos);
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{

	//testVector1();
	//testVector2();
	testVector4();
	//string s1("hello world");
	//string::iterator it = s1.begin();
	//while (it != s1.end())
	//{
	//	cout << *it;
	//	it++;
	//}
	//cout << endl;
	return 0;
}