#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

void Round_robin_date_play(int k, vector<vector<int>>& vect) {
    int n = 2;
    vect[0][0] = 1; vect[0][1] = 2;
    vect[1][0] = 2; vect[1][1] = 1;
    for (int t = 1; t < k; t++) 
    {
        int temp = n;
        n *= 2;
        for (int i = temp; i < n; i++) // 填写左下角元素
            for (int j = 0; j < temp; j++)
                vect[i][j] = vect[i - temp][j] + temp; // 左下角元素和左上角元素的对应关系
        for (int i = 0; i < temp; i++) // 填写右上角元素
            for (int j = temp; j < n; j++)
                vect[i][j] = vect[i + temp][j - temp]; // 右上角元素和左下角元素的对应关系
        for (int i = temp; i < n; i++) // 填写右下角元素
            for (int j = temp; j < n; j++)
                vect[i][j] = vect[i - temp][j - temp]; // 右下角元素和左上角元素的对应关系
    }
}


int main() {
    int k;
    cin >> k;
    int n = pow(2,k);
    vector<vector<int>> vect(n, vector<int>(n));
    Round_robin_date_play(k, vect);
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            cout << vect[i][j] << " ";
        }
        cout << endl;
    }
    return 0;
}