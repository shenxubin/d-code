#pragma once

#include <iostream>
#include <vector>
#include <map>
using namespace std;

template <class V, class W, W MAX_W = INT_MAX, bool Direction = false>
class Graph
{
private:
	vector<V> _vertexs;			// 顶点集合
	map<V, int> _indexMap;		// 顶点映射下标
	vector<vector<W>> _matrix;  // 邻接矩阵

public:
	typedef Graph<V, W, MAX_W, Diretion> Self;

	Graph() = default;
	//手动添加边
	Graph(const V* a, int n)
	{
		_vertexs.reserve(n);
		for (size_t i = 0; i < n; i++)
		{
			_vertexs.push_back(a[i]);
			_indexMap[a[i]] = i;
		}
		_matrix.resize(n);
		for (size_t i = 0; i < n; i++)
		{
			_matrix[i].resize(n, MAX_W);
		}
	}

	size_t GetVertexIndex(const V& v)
	{
		auto it = _indexMap.find(v);
		if (it != _indexMap.end())
		{
			return it->second;
		}
		else
		{
			cout << "未找到该点" << endl;
			return -1;
		}
	}

	void AddEdge(const V& src, const V& dst, const W& w)
	{
		size_t srci = GetVertexIndex(src);
		size_t dsti = GetVertexIndex(dst);

		_AddEdge(srci, dsti, w);
	}

	void _AddEdge(size_t srci, size_t dsti, const W& w)
	{
		_matrix[srci][dsti] = w;

		if (Direction == false)
		{
			_matrix[dsti][srci] = w;
		}
	}

	void Print()
	{
		// 顶点
		for (size_t i = 0; i < _vertexs.size(); ++i)
		{
			cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
		}
		cout << endl;

		// 矩阵
		// 横下标
		cout << "  ";
		for (size_t i = 0; i < _vertexs.size(); ++i)
		{
			//cout << i << " ";
			printf("%4d", i);
		}
		cout << endl;

		for (size_t i = 0; i < _matrix.size(); ++i)
		{
			cout << i << " "; // 竖下标
			for (size_t j = 0; j < _matrix[i].size(); ++j)
			{
				//cout << _matrix[i][j] << " ";
				if (_matrix[i][j] == MAX_W)
				{
					//cout << "* ";
					printf("%4c", '*');
				}
				else
				{
					//cout << _matrix[i][j] << " ";
					printf("%4d", _matrix[i][j]);
				}
			}
			cout << endl;
		}
		cout << endl;

		for (size_t i = 0; i < _matrix.size(); ++i)
		{
			for (size_t j = 0; j < _matrix[i].size(); ++j)
			{
				if (i < j && _matrix[i][j] != MAX_W)
				{
					cout << _vertexs[i] << "->" << _vertexs[j] << ":" << _matrix[i][j] << endl;
				}
			}
		}

	}


};
