#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct meeting
{
    int begin;
    int finish;
};

bool cmp(meeting meet1, meeting meet2)
{
    return meet1.finish < meet2.finish;
}

int greedySelector(int n,vector<meeting>& vect)
{
    int j = 0;
    int cot = 1;
    int q = 1;
    cout << "安排后可以进行的会议:" << endl;
    cout <<"第" << q << "场： 开始时间：" << vect[0].begin << " 结束时间：" << vect[0].finish << endl;
    for (int i = 1; i < n; i++)
    {
        if (vect[i].begin >= vect[j].finish)
        {
            q += 1;
            cout << "第" << q << "场： 开始时间：" << vect[i].begin << " 结束时间：" << vect[i].finish << endl;
            j = i;
            cot++;
        }

    }
    return cot;
}
int main()
{
    int n;
    cout << "会议总数:";
    cin >> n;
    vector<meeting> vect;

    cout << "输入会议开始和结束时间:" << endl;
    for (int i = 1; i <= n; i++)
    {
        meeting meet;
        cin >> meet.begin >> meet.finish;
        vect.push_back(meet);
    }

    //按结束时间非减序排列
    sort(vect.begin(), vect.end(), cmp);

    int cot;
    cot = greedySelector(n,vect);
    cout << "安排会议总数为:" << cot << endl;
    return 0;
}