#include "Student.h"
#include<iostream>
using namespace std;

string filename("untitled");
void show(student* p);
void Student::findcouse()
{
	cout << "请输入需要导出数据的科目名称" << endl;
	string name;
	cin >> name;
	if (name == "C")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->C.usual_performance <<
				"期末成绩:" << pHead->C.end_performance << " "
				<< "总成绩" << pHead->C.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;
	}
	if (name == "高数")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->HigerMath.usual_performance <<
				"期末成绩:" << pHead->HigerMath.end_performance << " "
				<< "总成绩" << pHead->HigerMath.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;
	}
	if (name == "英语")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->English.usual_performance <<
				"期末成绩:" << pHead->English.end_performance << " "
				<< "总成绩" << pHead->English.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;
	}
	if (name == "大物")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->CollegePhysics.usual_performance <<
				"期末成绩:" << pHead->CollegePhysics.end_performance << " "
				<< "总成绩" << pHead->CollegePhysics.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;
	}
	if (name == "组原")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->zuyuan.usual_performance <<
				"期末成绩:" << pHead->zuyuan.end_performance << " "
				<< "总成绩" << pHead->zuyuan.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;
	}
	if (name == "C++")
	{
		student* p;
		string new_file;
		cout << "请输入欲保存的文件名: ";
		cin >> new_file;
		fstream file;
		file.open(new_file.c_str(), ios::out);
		if (file.bad())
		{
			cout << "文件打开失败. " << endl;
			return;
		}
		int len = headCount();
		file << len << endl;
		student* pHead = head->stu_next;
		for (p = head; p->stu_next != NULL; p = p->stu_next)
		{

			file << pHead->stu_name << " ";
			file << "平均成绩：" << pHead->Cplusplus.usual_performance <<
				"期末成绩:" << pHead->Cplusplus.end_performance << " "
				<< "总成绩" << pHead->Cplusplus.fin_performance << endl;
		}
		file.close();
		cout << "录入完成" << endl;

	}
}

double couse::intfin_performance(couse& a)
{
	return (a.usual_performance * 0.3 + a.end_performance * 0.7);
}


Student::Student()
{
	head = new student;
	head->stu_id = 0;
	head->stu_name = "No name";
	head->stu_next = NULL;
}
Student::~Student()
{
	student* p = head, * q;
	while (p)
	{
		q = p;
		p = q->stu_next;
		delete q;
	}
}

//注册函数
void Student::logon()
{
	student tmp;
	cout << "学号 (0 结束输入 ): ";
	cin >> tmp.stu_id;

	while (tmp.stu_id)
	{
		if (findById(tmp.stu_id) == NULL)
		{
			tmp.C.cousename = "C语言";
			tmp.HigerMath.cousename = "高数";
			tmp.English.cousename = "英语";
			tmp.CollegePhysics.cousename = "大物";
			tmp.zuyuan.cousename = "组原";
			tmp.Cplusplus.cousename = "C++";

			cout << "姓    名： " << endl;
			cin >> tmp.stu_name;

			cout << "C语言   ： " << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.C.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.C.end_performance;
			tmp.C.fin_performance = tmp.C.intfin_performance(tmp.C);
			cout << "总成绩为：" << tmp.C.fin_performance << endl;
			cout << endl;

			cout << "高数    ： " << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.HigerMath.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.HigerMath.end_performance;
			tmp.HigerMath.fin_performance = tmp.HigerMath.intfin_performance(tmp.HigerMath);
			cout << "总成绩为：" << tmp.HigerMath.fin_performance << endl;
			cout << endl;

			cout << "英    语 ： " << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.English.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.English.end_performance;
			tmp.English.fin_performance = tmp.English.intfin_performance(tmp.English);
			cout << "总成绩为：" << tmp.English.fin_performance << endl;
			cout << endl;

			cout << "大    物： " << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.CollegePhysics.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.CollegePhysics.end_performance;
			tmp.CollegePhysics.fin_performance = tmp.CollegePhysics.intfin_performance(tmp.CollegePhysics);
			cout << "总成绩为：" << tmp.CollegePhysics.fin_performance << endl;
			cout << endl;

			cout << "组    原：" << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.zuyuan.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.zuyuan.end_performance;
			tmp.zuyuan.fin_performance = tmp.zuyuan.intfin_performance(tmp.zuyuan);
			cout << "总成绩为：" << tmp.zuyuan.fin_performance << endl;
			cout << endl;

			cout << "C++     ：" << endl;
			cout << "平时成绩：" << endl;
			cin >> tmp.Cplusplus.usual_performance;
			cout << "期末成绩：" << endl;
			cin >> tmp.Cplusplus.end_performance;
			tmp.Cplusplus.fin_performance = tmp.Cplusplus.intfin_performance(tmp.Cplusplus);
			cout << "总成绩为：" << tmp.Cplusplus.fin_performance << endl;
			cout << endl;

			insert(tmp);
		}
		else
		{
			cout << "重复的学号: " << tmp.stu_id << endl;
		}
		cout << "添加成功" << endl;
		cout << "学号 (0 结束输入): ";
		cin >> tmp.stu_id;
	}
}

//查找函数
void Student::Query() const {
	int select;
	unsigned id;
	string name;
	string sex;
	string email;
	student* p;
	cout << "1、按学号查询\n2、按姓名查询\n3、返回\n";
	cin >> select;
	switch (select)
	{
	case 1: cout << "请输入学号: ";
		    cin >> id;
			if ((p = findById(id)))
			{
				show(p->stu_next);
			}
		    break;
	case 2: cout << "请输入姓名: "; 
		    cin >> name;
			if ((p = findByName(name)))
			{
				show(p->stu_next);
			}
		    break;
	case 3: return;
	default: cout << "选择错误. \n";
	}
}

student* Student::findByName(const string& name) const 
{
	student* p;
	for (p = head; p->stu_next; p = p->stu_next)
	{
		if (p->stu_next->stu_name == name) 
			return p;
	}
	cout << "没有找到输入的姓名" << endl;
	return NULL;
}
student* Student::findById(unsigned id)  const
{
	student* p;
	for (p = head; p->stu_next; p = p->stu_next)
		if (p->stu_next->stu_id == id) return p;
	return NULL;
}

//删除函数
void Student::deleteBy()
{
	int select;
	unsigned id;
	string name;
	cout << "1、按学号删除\n2、按姓名删除\n0、返回\n";
	cin >> select;
	switch (select)
	{
	case 1: cout << "请输入学号: "; cin >> id;
		deleteById(id);
		break;
	case 2: cout << "请输入姓名: "; cin >> name;
		deleteByName(name);
		break;
	case 0: return;
	default: cout << "选择错误. \n";
	}
}
bool Student::deleteById(unsigned& id)
{
	student* q, * p;
	p = findById(id);
	if (p == NULL)
	{
		cout << "没有找到学号是 \"" << id << "\" 的学生，删除失败! \n";
		return false;
	}
	q = p->stu_next;
	p->stu_next = q->stu_next;
	delete q;
	cout << "成功删除 " << id << " 的信息. \n";
	return true;
}
bool Student::deleteByName(string& name) {
	student* q, * p;
	p = findByName(name);
	if (p == NULL) {
		cout << "没有找到姓名是 \"" << name << "\" 的学生, 删除失败! \n";
		return false;
	}
	q = p->stu_next;
	p->stu_next = q->stu_next;
	delete q;
	cout << "成功删除 " << name << " 的信息. \n";
	return true;
}

//修改函数
void Student::modify()
{
	int select;
	cout << "1、按学号修改\n2、按姓名修改\n0、返回\n";
	cin >> select;
	switch (select)
	{
	case 1: if (modifyById()) cout << "修改成功. \n"; break;
	case 2: if (modifyByName()) cout << "修改成功. \n"; break;
	case 0: return;
	default: cout << "选择错误. \n";
	}
}

bool Student::modifyById()
{
	student* p;
	unsigned id;
	cout << "输入要修改的学号: ";
	cin >> id;
	p = findById(id);
	if (p == NULL)
	{
		cout << "没有找到学号是 \"" << id << "\" 的学生, 修改失败! \n";
		return false;
	}
	createByStdin(1, *(p->stu_next));
	return true;
}
bool Student::modifyByName()
{
	student* p;
	string name;
	cout << "输入要修改人的姓名: ";
	cin >> name;
	p = findByName(name);
	if (p == NULL) {
		cout << "没有找到姓名是 \"" << name << "\" 的学生, 修改失败! \n";
		return false;
	}
	createByStdin(2, *(p->stu_next));
	return true;
}
student* Student::createByStdin(int model, student& tmp)
{
	if (model != 1) { cout << "学    号: "; cin >> tmp.stu_id; }
	if (model != 2) { cout << "姓    名: "; cin >> tmp.stu_name; }

	cout << "C语言   ： " << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.C.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.C.end_performance;
	tmp.C.fin_performance = tmp.C.intfin_performance(tmp.C);
	cout << "总成绩为：" << tmp.C.fin_performance << endl;
	cout << endl;
	cout << "高    数： " << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.HigerMath.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.HigerMath.end_performance;
	tmp.HigerMath.fin_performance = tmp.HigerMath.intfin_performance(tmp.HigerMath);
	cout << "总成绩为：" << tmp.HigerMath.fin_performance << endl;
	cout << endl;

	cout << "英    语： " << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.English.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.English.end_performance;
	tmp.English.fin_performance = tmp.English.intfin_performance(tmp.HigerMath);
	cout << "总成绩为：" << tmp.English.fin_performance << endl;
	cout << endl;

	cout << "大    物： " << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.CollegePhysics.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.CollegePhysics.end_performance;
	tmp.CollegePhysics.fin_performance = tmp.CollegePhysics.intfin_performance(tmp.CollegePhysics);
	cout << "总成绩为：" << tmp.CollegePhysics.fin_performance << endl;
	cout << endl;
	cout << "组    原：" << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.zuyuan.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.zuyuan.end_performance;
	tmp.zuyuan.fin_performance = tmp.zuyuan.intfin_performance(tmp.zuyuan);
	cout << "总成绩为：" << tmp.zuyuan.fin_performance << endl;
	cout << endl;
	cout << "C++     ：" << endl;
	cout << "平时成绩：" << endl;
	cin >> tmp.Cplusplus.usual_performance;
	cout << "期末成绩：" << endl;
	cin >> tmp.Cplusplus.end_performance;
	tmp.Cplusplus.fin_performance = tmp.Cplusplus.intfin_performance(tmp.Cplusplus);
	cout << "总成绩为：" << tmp.Cplusplus.fin_performance << endl;
	cout << endl;
	return &tmp;
}


//文件操作函数
void Student::saveToFile()
{
	string new_file;
	cout << "请输入欲保存的文件名: ";
	cin >> new_file;
	fstream file;
	file.open(new_file.c_str(), ios::out);
	if (file.bad())
	{
		cout << "文件打开失败. " << endl;
		return;
	}
	int len = headCount();
	file << len << endl;
	student* pHead = head->stu_next;
	while (pHead != NULL)
	{
		file << pHead->stu_name << endl;
		file << pHead->stu_id << endl;

		file << pHead->C.cousename << endl;
		file << pHead->C.usual_performance << endl;
		file << pHead->C.end_performance << endl;
		file << pHead->C.fin_performance << endl;

		file << pHead->HigerMath.cousename << endl;
		file << pHead->HigerMath.usual_performance << endl;
		file << pHead->HigerMath.end_performance << endl;
		file << pHead->HigerMath.fin_performance << endl;

		file << pHead->English.cousename << endl;
		file << pHead->English.usual_performance << endl;
		file << pHead->English.end_performance << endl;
		file << pHead->English.fin_performance << endl;

		file << pHead->CollegePhysics.cousename << endl;
		file << pHead->CollegePhysics.usual_performance << endl;
		file << pHead->CollegePhysics.end_performance << endl;
		file << pHead->CollegePhysics.fin_performance << endl;

		file << pHead->zuyuan.cousename << endl;
		file << pHead->zuyuan.usual_performance << endl;
		file << pHead->zuyuan.end_performance << endl;
		file << pHead->zuyuan.fin_performance << endl;

		file << pHead->Cplusplus.cousename << endl;
		file << pHead->Cplusplus.usual_performance << endl;
		file << pHead->Cplusplus.end_performance << endl;
		file << pHead->Cplusplus.fin_performance << endl;


		pHead = pHead->stu_next;
	}
	file.close();
	cout << "保存成功. " << endl;
	filename = new_file;
	return;
}
void Student::readFile()
{
	string new_file;
	cout << "请输入欲读取的文件名: ";
	cin >> new_file;
	fstream file;
	file.open(new_file.c_str(), ios::in);
	if (file.bad())
	{
		cout << "文件打开失败. " << endl;
		return;
	}
	int len = 0;
	file >> len;
	if (!len)
	{
		cout << "文件数据异常. " << endl;
		return;
	}
	student pHead;
	file.get();
	while (len--)
	{
		file >> pHead.stu_id;
		file >> pHead.stu_name;

		file >> pHead.C.cousename;
		file >> pHead.C.usual_performance;
		file >> pHead.C.end_performance;
		file >> pHead.C.fin_performance;

		file >> pHead.HigerMath.cousename;
		file >> pHead.HigerMath.usual_performance;
		file >> pHead.HigerMath.end_performance;
		file >> pHead.HigerMath.fin_performance;

		file >> pHead.English.cousename;
		file >> pHead.English.usual_performance;
		file >> pHead.English.end_performance;
		file >> pHead.English.fin_performance;

		file >> pHead.CollegePhysics.cousename;
		file >> pHead.CollegePhysics.usual_performance;
		file >> pHead.CollegePhysics.end_performance;
		file >> pHead.CollegePhysics.fin_performance;

		file >> pHead.zuyuan.cousename;
		file >> pHead.zuyuan.usual_performance;
		file >> pHead.zuyuan.end_performance;
		file >> pHead.zuyuan.fin_performance;

		file >> pHead.Cplusplus.cousename;
		file >> pHead.Cplusplus.usual_performance;
		file >> pHead.Cplusplus.end_performance;
		file >> pHead.Cplusplus.fin_performance;


		insert(pHead);
	}
	file.close();
	cout << "文件读入成功. " << endl;
	filename = new_file;
	return;
}
bool Student::insert(const student& astu)
{
	student* newnode, * p = head;
	if (p->stu_next == NULL)    //当没有下一个数据，则新建一个空间来储存修改数据
	{
		p->stu_next = new student(astu);   //用传入的已修改结构初始化
		p->stu_next->stu_next = NULL;      //NULL处理再下一个堆
		return true;
	}
	while (p->stu_next)           //当存在下一个堆
	{
		if (p->stu_next->stu_id == astu.stu_id)    //判重
		{
			cout << "重复的学号, 插入失败! \n";
			return false;
		}
		if (p->stu_next->stu_id > astu.stu_id)    //如果下一个堆的id大于传入id
		{
			newnode = new student(astu);           //newnode指向初始修改结构
			newnode->stu_next = p->stu_next;
			p->stu_next = newnode;               // 移位
			return true;
		}
		p = p->stu_next;    //既不大于也不重复就后移添加
	}
	p->stu_next = new student(astu);
	p->stu_next->stu_next = NULL;
	return true;
}

//显示函数
void Student::Show() const {
	student* p;
	cout << "----------------------------------------------------------\n";
	for (p = head->stu_next; p; p = p->stu_next)
	{
		show(p);
	}
	cout << "----------------------------------------------------------\n";
}
void show(student* p)
{

	cout << "  学号：" << p->stu_id << endl;
	cout << "  姓名： " << p->stu_name << endl;
	cout << "  C语言： 平时成绩：" << p->C.usual_performance
		<< "  期末成绩：" << p->C.end_performance
		<< "  总评成绩：" << p->C.fin_performance << endl;

	cout << "  高数： 平时成绩：" << p->HigerMath.usual_performance
		<< "  期末成绩：" << p->HigerMath.end_performance
		<< "  总评成绩：" << p->HigerMath.fin_performance << endl;

	cout << "  英语： 平时成绩：" << p->English.usual_performance
		<< "  期末成绩：" << p->English.end_performance
		<< "  总评成绩：" << p->English.fin_performance << endl;

	cout << "  大物： 平时成绩：" << p->CollegePhysics.usual_performance
		<< "  期末成绩：" << p->CollegePhysics.end_performance
		<< "  总评成绩：" << p->CollegePhysics.fin_performance << endl;

	cout << "  组原： 平时成绩：" << p->zuyuan.usual_performance
		<< "  期末成绩：" << p->zuyuan.end_performance
		<< "  总评成绩：" << p->zuyuan.fin_performance << endl;

	cout << "  C++： 平时成绩：" << p->Cplusplus.usual_performance
		<< "  期末成绩：" << p->Cplusplus.end_performance
		<< "  总评成绩：" << p->Cplusplus.fin_performance << endl;

}

unsigned Student::headCount() const
{
	unsigned cnt = 0;
	student* p;
	for (p = head->stu_next; p; p = p->stu_next, ++cnt);
	return cnt;
}


