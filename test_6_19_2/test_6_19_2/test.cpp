#include<iostream>
#include "Student.h"
#include "User.h"
using namespace std;

void Menutea()
{
	cout << endl;
	cout << "*********************************************" << endl;
	cout << "**            *学生管理教师系统*           **" << endl;
	cout << "**             1、读取学生成绩             **" << endl;
	cout << "**             2、保存学生成绩             ** " << endl;
	cout << "**             3、添加学生信息             **" << endl;
	cout << "**             4、修改学生信息             **" << endl;
	cout << "**             5、浏览学生成绩             **" << endl;
	cout << "**             6、查询具体学生成绩         **" << endl;
	cout << "**             7、删除学生成绩             **" << endl;
	cout << "**             8、导出单科数据             **" << endl;
	cout << "**             0、退出                     **" << endl;
	cout << "*********************************************" << endl;
	cout << endl;
}

void usemenu()
{
	cout << endl;
	cout << "*********************************************" << endl;
	cout << "**          *学生管理管理员系统*           **" << endl;
	cout << "**             1、读取学生成绩             **" << endl;
	cout << "**             2、保存学生成绩             ** " << endl;
	cout << "**             3、添加老师账号             **" << endl;
	cout << "**             4、查找教师账号             **" << endl;
	cout << "**             5、删除老师账号             **" << endl;
	cout << "**             6、查询具体学生成绩         **" << endl;
	cout << "**             7、查询具体老师账号         **" << endl;
	cout << "**             8、查询所有学生成绩         **" << endl;
	cout << "**             0、退出                     **" << endl;
	cout << "*********************************************" << endl;
	cout << endl;
}

void menu() {
	cout << endl;
	cout << "+-----------------------------------------------+\n";
	cout << "|                   学生信息管理系统            |\n";
	cout << "+---------------------------------------------+\n";
	cout << "|   1、添加老师账号      |  6、添加新学生信息   |\n";
	cout << "|   2、查看老师账号      |  7、修改学生信息     |\n";
	cout << "|   3、删除老师账号      |  8、显示学生信息     |\n";
	cout << "|   4、读取学生信息      |  9、查询学生信息     |\n";
	cout << "|   5、保存学生信息      | 10、删除学生信息     |\n";
	cout << "+-----------------------------------------------+\n";
	cout << "|                     0、退出                   |\n";
	cout << "+-----------------------------------------------+\n";
	cout << endl;
}


void showtea()
{

}

int main()
{

	cout << "学生成绩管理系统" << endl;
	cout << "请输入您的身份(管理员/教师）" << endl;
	string name;
	cin >> name;
	User u1;
	u1.readFile();
	if (u1.logon())
	{

		Student m_stu;
		int choice;
		char str[20];
		if (name == "教师")
		{
			cout << "欢迎来到学生成绩管理教师系统" << endl;
			Menutea();
			do {
				cout << "请输入指令:";
				cin >> choice;
				while (!cin)                //判断choice是否输入的是整数，如果是正确类型,cin=0
				{
					cin.clear();                //清空cin流
					cin >> str;                  //赋值错误输入到str
					cout << "请输入正确的指令：" << endl;
					cin >> choice;
				}
				switch (choice) {
				case 1: m_stu.readFile(); 	Menutea(); break;
				case 2: m_stu.saveToFile(); Menutea(); break;
				case 3: m_stu.logon(); Menutea(); break;
				case 4: m_stu.modify(); Menutea(); break;
				case 5: m_stu.Show(); Menutea(); break;
				case 6: m_stu.Query(); Menutea(); break;
				case 7: m_stu.deleteBy(); Menutea(); break;
				case 8:m_stu.findcouse(); Menutea(); break;
				case 0:; break;
				default: cout << "请输入正确的选项" << endl;
					menu(); break;
				}
			} while (choice);
			return 0;

		}
		else if (name == "管理员")
		{
			cout << "欢迎来到学生成绩管理管理员系统" << endl;
			usemenu();
			do {
				cout << "请输入指令:";
				cin >> choice;
				while (!cin)                //判断choice是否输入的是整数，如果是正确类型,cin=0
				{
					cin.clear();                //清空cin流
					cin >> str;                  //赋值错误输入到str
					cout << "请输入正确的指令：" << endl;
					cin >> choice;
				}
				switch (choice)
				{
				case 1: m_stu.readFile(); 	usemenu(); break;
				case 2: m_stu.saveToFile(); usemenu(); break;
				case 3: u1.login(); usemenu(); break;
				case 4: u1.show(); usemenu(); break;
				case 5: u1.deleteByName(); usemenu(); break;
				case 6: m_stu.Query(); usemenu(); break;
				case 7:u1.showtailshowname(); usemenu; break;
				case 8: m_stu.Show(); usemenu; break;
				case 0: break;
				default: cout << "请输入正确的选项" << endl;
					menu(); break;
				}
			} while (choice);
			return 0;

		}
	}
}


