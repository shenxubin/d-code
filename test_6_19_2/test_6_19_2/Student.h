#pragma once

#include <string>
#include <fstream>
using namespace std;

struct couse
{
	string cousename;
	int usual_performance;   //平时成绩
	int end_performance;     //期末成绩
	double fin_performance;    //总成绩
	double intfin_performance(couse& a);     //计算总成绩   平时成绩30%+期末成绩70%
};


typedef struct student 
{
	string stu_name;    //姓名
	unsigned stu_id;    //学号
	string couse;             //课程名
	struct couse C;
	struct couse HigerMath;
	struct couse English;
	struct couse CollegePhysics;
	struct couse zuyuan;
	struct couse Cplusplus;
	student* stu_next = NULL;    //射线结构链表
} student;

class Student
{
public:
	Student();     //构造函数
	~Student();    //析构函数

	//录入学生信息
	student* createByStdin(int model, student& tmp); // 从标准输入中创建学生，model = 1: 不读取学号，2:不读取姓名，其他，读取所有信息。
	void logon(); // 连续录入学生信息


	//查找
	void Query() const; // 查询学生信息
	student* findById(unsigned id) const; // 根据学号查找
	student* findByName(const string& name) const; // 根据姓名查找


	//删除
	void deleteBy(); // 删除
	bool deleteById(unsigned& id); // 根据学号删除
	bool deleteByName(string& name); // 根据姓名删除

	//修改
	void modify(); // 修改
	bool modifyById(); // 根据学号修改
	bool modifyByName(); // 根据姓名修改


	//显示
	void Show() const; // 列出学生信息

	//文件操作
	void saveToFile(); // 保存到文件
	void readFile(); // 读取文件
	bool insert(const student& astu);

	unsigned headCount() const;

	void findcouse();     //调出单科成绩

private:
	student* head;   //进入链表的指针
};
