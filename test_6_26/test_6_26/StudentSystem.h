#pragma once

#include <iostream>
#include<fstream>
#include <conio.h>
using namespace std;

struct Student
{
	string name;
	string ID;
	int age;
	double score;
};

class Node
{
public:
	Student student;
	Node* next;
};

void WelcomeStudent();

void WelcomeTeacher();

void WelcomeSupervisor();

void InsertStudent(Node* head);

void PrintAllStudent(Node* head);

void FindStudent(Node* head);

void SaveStudent(Node* head);

void LoadStudent(Node* head);

void DeleteStudent(Node* head);

void SortStudent(Node* head);

void ModifyStudentScore(Node* head);

void ModifyStudentOtherInformation(Node* head);
