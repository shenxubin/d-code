#include <iostream>
#include <stack>
using namespace std;

int main()
{
    string A;
    cin >> A;
    int n;
    cin >> n;

    stack<char> st;
    for (int i = 0; i < n; i++)
    {
        if (A[i] != '(' && A[i] != ')')
        {
            return false;
        }

        if (A[i] == ')' && st.empty())
        {
            return false;
        }
        if (A[i] == '(')
        {
            st.push(A[i]);
        }
        else if (A[i] == ')')
        {
            st.pop();
        }
    }
    if (!st.empty())
    {
        return false;
    }
    return true;
}