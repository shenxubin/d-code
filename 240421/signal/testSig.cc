#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>


void handler(int signumber)
{
    std::cout << " get a signal, number is : " << signumber << std::endl;
}

int main()
{
    signal(2, handler);
    int cnt = 0;
    while(true)
    {
        std::cout << "cnt: " << cnt++ << std::endl;
        sleep(1);
        if(cnt % 5 == 0) 
        {
            std::cout << "send 2 to caller, pid:" <<getpid() << std::endl;
            raise(2); // SIGSTOP : 19
            //abort();
        }
    }
}

// void handler(int sig)
// {
//     std::cout << "收到二号信号，但不退出！" << std::endl;
// }

// int main()
// {
//     signal(SIGINT, SIG_IGN);

//     while (true)
//     {
//         std::cout << "I am a process! my pid is : " << getpid() << std::endl;
//         sleep(1);
//     }

//     return 0;
// }

