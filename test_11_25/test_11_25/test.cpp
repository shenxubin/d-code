#include <iostream>
#include <map>
#include <set>
using namespace std;


void testMap1()
{
	string arr[] = { "西瓜","西瓜" ,"西瓜" ,"西瓜" ,"苹果","苹果" ,"苹果" ,"梨子", "桃子" };
	map<string, int> countMap;
	for (auto& e : arr)
	{
		auto ret = countMap.find(e); //ret是一个地址
		if (ret == countMap.end())
		{
			countMap.insert(make_pair(e, 1));
		}
		else
		{
			ret->second++;
		}
	}
	for (auto& e : countMap)
	{
		cout << e.first << " " << e.second << endl;
	}
}

//set可以实现排序加去重
//multiset可以实现排序不去重
void testSet1()
{
	set<int> s1;
	s1.insert(1);
	s1.insert(2);
	s1.insert(3);
	s1.insert(4);
	s1.insert(6);
	s1.insert(13);
	s1.insert(67);
	s1.insert(1);

	int x;
	while (cin >> x)
	{
		set<int>::iterator it;
		it = s1.find(x);
		if (it != s1.end())
		{
			cout << "在" << endl;
		}
		else
		{
			cout << "不在" << endl;
		}
	}
}

void testSet2()
{
	set<int> s1;
	s1.insert(1);
	s1.insert(2);
	s1.insert(3);
	s1.insert(4);
	s1.insert(6);
	s1.insert(13);
	s1.insert(67);
	s1.insert(1);

	set<int>::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}

void testMultiSet1()
{
	multiset<int> s1;
	s1.insert(1);
	s1.insert(2);
	s1.insert(3);
	s1.insert(4);
	s1.insert(6);
	s1.insert(13);
	s1.insert(67);
	s1.insert(1);

	multiset<int>::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	//如果有多个key，会去找中序的第一个key
}
int main()
{
	//testSet1();
	//testSet2();
	testMultiSet1();
	return 0;
}