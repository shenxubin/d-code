#include "Date.h"

int Date::GetMonthDay(int year, int month)
{
	int arrDay[] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2)
	{
		if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
		{
			return arrDay[2];
		}
		else
		{
			return arrDay[2] + 1;
		}
	}
	return arrDay[month];
}


Date::Date(int year, int month, int day)
{
	this->_year = year;
	this->_month = month;
	this->_day = day;
}

Date::Date(const Date& d)
{
	this->_year = d._year;
	this->_month = d._month;
	this->_day = d._day;
}

Date& Date:: operator=(const Date& d)
{
	this->_year = d._year;
	this->_month = d._month;
	this->_day = d._day;

	return *this;
}

Date::~Date()
{

}

Date& Date:: operator+=(int day)
{
	this->_day += day;
	while (_day > GetMonthDay(_year,_month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
		
	}

	return *this;

}

Date Date:: operator+(int day)
{
	Date tmp = *this += day;
	return tmp;
}

Date& Date::operator-=(int day)
{
	this->_day -= day;
	while (_day < 1)
	{
		_month--;
		if (_month == 0)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}

//2023 12 1
// 2
// -1 + 30
// 2023 1 1
// 3
//-2 + 31

Date Date:: operator-(int day)
{
	Date tmp = *this -= day;
	return tmp;
}

Date& Date:: operator++()
{
	_day++;
	if (_day == GetMonthDay(_year, _month)+1)
	{
		_day = 1;
		_month++;
		if (_month == 12)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}

Date Date:: operator++(int)
{
	Date tmp = *this;
	++(*this);
	return tmp;
}

Date& Date:: operator--()
{
	_day--;
	if (_day == 0)
	{
		if (_month == 1)
		{
			_day = 31;
			_month = 12;
			_year--;
		}
		else
		{
			_month--;
			_day = GetMonthDay(_year, _month);
		}
	}

	return *this;
}

Date Date:: operator--(int)
{
	Date tmp = *this;
	--(*this);
	return tmp;
}


bool Date:: operator>(const Date& d)
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year && _month > d._month)
	{
		return true;
	}
	else if (_year == d._year && _month == d._month && _day > d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date:: operator==(const Date& d)
{
	if (_year == d._year && _month == d._month && _day == d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date:: operator >= (const Date& d)
{
	if (*this > d || *this == d)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date:: operator < (const Date& d)
{
	return !(*this >= d);
}

bool Date:: operator <= (const Date& d)
{
	if (*this < d || *this == d)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Date:: operator != (const Date& d)
{
	return !(*this == d);
}

int Date:: operator-(const Date& d)
{
	int daySum = 0;
	Date bigdate = *this;
	Date smalldate = d;
	if (bigdate < smalldate)
	{
		bigdate = d;
		smalldate =  *this;
	}
	while (bigdate != smalldate)
	{
		daySum++;
		bigdate--;
	}
	return daySum;
}

ostream& operator<<(ostream& cout, const Date& d)
{
	cout << d._year << " " << d._month << " " << d._day;
	return cout;
}

istream& operator>>(istream& in, Date& d)
{
	cin >> d._year >> d._month >> d._day;
	return in;
}