#include "Date.h"

#include <iostream>
using namespace std;

int getMonthDay(int year, int month)
{
    int arrDay[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
    if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
    {
        if (month == 2)
        {
            return 29;
        }
    }
    return arrDay[month];
}



void test01()
{
	Date d1(2023, 12, 12);
	Date d2 = d1;
	cout << d2 << endl;
}

void test02()
{
	Date d1(2023, 12, 12);
	d1 += 30;
	cout << d1 << endl;
}

void test03()
{
	/*Date d1(2023, 12, 12);
	d1 += 1130;
	cout << d1 << endl;*/

  
}

int main()
{
	//test01();
	//test03();
    int year, month, day;
    cin >> year >> month >> day;
    int monthSum = month - 1, sum = 0;
    for (int i = 1; i <= monthSum; i++)
    {
        sum += getMonthDay(year, i);
    }
    monthSum += day;
    cout << sum << endl;

	return 0;
}