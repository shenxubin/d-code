#include <iostream>
#include <stack>
using namespace std;

struct postion
{
	int x;
	int y;
};

stack<postion> st1;
postion pos2;

bool IsPass(int** maze, int n, int m, postion cur)
{
	if (cur.x >= 0 && cur.x < n && cur.y >= 0 && cur.y < m && maze[cur.x][cur.y] == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}


bool GetPath(int** maze, int N, int M, postion cur)
{
	//走过的坐标先入栈
	st1.push(cur);

	//到达终点
	//if (cur.x == 4 && cur.y == 6)
	//{
	//	return true;
	//}

	//上下左右判断
	postion next;
	maze[cur.x][cur.y] = 2;

	//到达终点
	if ((((cur.x) - 1) == pos2.x && cur.y == pos2.y) || (((cur.x) + 1) == pos2.x && cur.y == pos2.y)
		|| (((cur.x)) == pos2.x && ((cur.y) - 1) == pos2.y) || (((cur.x)) == pos2.x && ((cur.y) + 1) == pos2.y))
	{
		postion next = { 4,6 };
		st1.push(next);
		return true;
	}

	//到达终点
	//if ((((cur.x) - 1) == N-1 && cur.y == M-1) || (((cur.x) + 1) == N-1 && cur.y == M-1)
	//|| (((cur.x)) == N-1 && ((cur.y) - 1) == M-1) || (((cur.x)) == N-1 && ((cur.y) + 1) == M-1))
	//{
	//	postion next = { N-1,M-1 };
	//	st1.push(next);
	//	return true;
	//}

	//上
	next = cur;
	next.x -= 1;
	if (IsPass(maze, N, M, next))
	{
		if (GetPath(maze, N, M, next))
		{
			return true;
		}
	}


	//下
	next = cur;
	next.x += 1;
	if (IsPass(maze, N, M, next))
	{
		if (GetPath(maze, N, M, next))
		{
			return true;
		}
	}

	//左
	next = cur;
	next.y -= 1;
	if (IsPass(maze, N, M, next))
	{
		if (GetPath(maze, N, M, next))
		{
			return true;
		}
	}


	//右
	next = cur;
	next.y += 1;
	if (IsPass(maze, N, M, next))
	{
		if (GetPath(maze, N, M, next))
		{
			return true;
		}
	}

	//四个方向都不能走，就出栈返回
	st1.pop();
	return false;
}

void PrintPath()
{
	stack<postion> st2;//创建需要导数据的另一个栈

	while (!st1.empty())
	{
		st2.push(st1.top());
		st1.pop();
	}

	while (!st2.empty())
	{
		postion cur = st2.top();
		cout << cur.x << " " << cur.y << endl;
		st2.pop();
	}

}



int main()
{
	int n, m;
	cin >> n >> m;
	postion cur;

	int** maze = new int* [n];
	for (int i = 0; i < n; i++)
	{
		maze[i] = new int[m];
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
		{
			cin >> maze[i][j];
			if (maze[i][j] == 3)
			{
				cur.x = i;
				cur.y = j;
			}
			else if (maze[i][j] == 4)
			{
				pos2.x = i;
				pos2.y = j;
			}
		}
	}

	if (GetPath(maze, n, m, cur))
	{
		PrintPath();
	}

	for (int i = 0; i < n; i++)
	{
		delete maze[i];
	}
	delete maze;
	return 0;
}