//chika很喜欢吃蜜柑。每个蜜柑有一定的酸度和甜度，chika喜欢吃甜的，但不喜欢吃酸的。
//一共有n个蜜柑， chika吃k个蜜柑， 将获得所吃的甜度之和与酸度之和。
// chika想获得尽可能大的甜度总和。如果有多种方案，她希望总酸度尽可能小。
//她想知道，最终的总酸度和总甜度是多少 ?

#include <iostream>
#include <stdexcept>
#include <vector>
#include <queue>
using namespace std;

struct orange
{
    long long _acidity;
    long long _sweetness;
};

//建大根堆，注意 < 和 > ，建大根堆用<号，有点后来者居上的意思
struct cmp_orange
{
    bool operator()(const orange& o1, const orange& o2)
    {
        if (o1._sweetness < o2._sweetness)
            return true;
        else if (o1._sweetness == o2._sweetness && o1._acidity > o2._acidity)
            return true;
        else
            return false;
    }
};

int main()
{
    int n, k;
    cin >> n >> k;
    priority_queue<orange, vector<orange>, cmp_orange> q;
    vector<long long> vect_sweetness(n);
    vector<long long> vect_acidity(n);
    for (int i = 0; i < n; i++)
    {
        cin >> vect_acidity[i];
    }
    for (int i = 0; i < n; i++)
    {
        cin >> vect_sweetness[i];
    }

    for (int i = 0; i < n; i++)
    {
        orange oo;
        oo._acidity = vect_acidity[i];
        oo._sweetness = vect_sweetness[i];
        q.push(oo);
    }

    long long sweet1 = 0, aci1 = 0;
    while (k--)
    {
        sweet1 += q.top()._sweetness;
        aci1 += q.top()._acidity;
        q.pop();
    }
    cout << aci1 << " " << sweet1 << endl;
    return 0;
}
// 64 位输出请用 printf("%lld")