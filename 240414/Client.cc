#include "namedPipe.hpp"

int main()
{
    int wfd = open(NAME, O_WRONLY);
    
    if (wfd < 0)
    {
        cout << "文件打开失败，原因是：" << strerror(errno) << endl;
        return 1;
    }
    cout << "文件打开成功！" << endl;

    string buffer;
    while(true)
    {
        cout << "请输入你的信息：" << endl;
        getline(cin, buffer);
        if(buffer == "quit")
        break;

        int n = write(wfd, buffer.c_str(), sizeof(buffer)-1);
        
        if(n < 0)
        {
            cout << "输入错误，错误原因是：" << strerror(errno) << endl;
        }

    }

    close(wfd);
    return 0;
}