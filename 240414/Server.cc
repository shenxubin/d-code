#include "namedPipe.hpp"

int main()
{

    Fifo fifo(NAME);
    //打开文件，打开成功返回文件描述符，打开失败返回-1
    int rfd = open(NAME, O_RDONLY);
    if (rfd < 0)
    {
        cout << "文件打开失败，原因是：" << strerror(errno) << endl;
        return 1;
    }
    cout << "文件打开成功！" << endl;

    while (true)
    {
        char buffer[1024];
        //读端不退，read函数会一直阻塞等待，读端退出read读到0
        ssize_t n = read(rfd, buffer, sizeof(buffer) - 1);

        if (n > 0)
        {
            buffer[n] = '\0';
            cout << "客户说：" << buffer << endl;
        }
        else if (n == 0)
        {
            cout << "客户退出了，我也退出了";
            break;
        }
        else
        {
            cout << "读取出错！原因是：" << strerror(errno) << endl;
            break;
        }
    }

    close(rfd);
    return 0;
}