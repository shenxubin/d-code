//#include <iostream>
//#include <queue>
//using namespace std;
//
//class JCB
//{
//public:
//    string name;//作业名
//    int ftime;//作业完成时间
//    int atime;//作业到达时间
//    int rtime;//作业运行时间
//    int wtime;//作业等待时间
//    int ztime;//作业周转时间
//    int lefttime;//剩余时间
//    double qtime;//作业带权周转时间
//    char state;//作业状态
//    float super;//优先级
//
//    JCB operator =(JCB t)
//    {
//        this->atime = t.atime;
//        this->ftime = t.ftime;
//        this->name = t.name;
//        this->qtime = t.qtime;
//        this->rtime = t.rtime;
//        this->state = t.state;
//        this->super = t.super;
//        this->wtime = t.wtime;
//        this->ztime = t.ztime;
//        return *this;
//    }
//};
//
//int n;//作业数量
//int fn = 0;//完成作业数量
//int source;//每个进程最多占用cpu时间
//
//queue <JCB> q;
//
//void input()
//{
//    JCB jcb;
//    cout << "请输入每个进程最多占用cpu时间：";
//    cin >> source;
//    cout << "请输入作业数量：";
//    cin >> n;
//    for (int i = 0; i < n; i++)
//    {
//        cout << "请输入第" << i + 1 << "个进程的信息：" << endl;
//        cout << "请输入进程名：";
//        cin >> jcb.name;
//        jcb.atime = i;
//        cout << "请输入进程运行时间：";
//        cin >> jcb.rtime;
//        if (jcb.rtime <= 0)
//        {
//            cout << "运行时间必须为正数，请重新输入！" << endl;
//            i--;
//            continue;
//        }
//        jcb.wtime = 0;
//        jcb.ftime = 0;
//        jcb.ztime = 0;
//        jcb.qtime = 0;
//        jcb.lefttime = jcb.rtime;
//        jcb.state = 'w';
//        jcb.super = 0;
//        q.push(jcb);
//    }
//}
//
//void FCFS(JCB result[], double& sum1, double& sum2)
//{
//    int before = 0;
//    while (!q.empty())
//    {
//        JCB t = q.front();
//        q.pop();
//        t.state = 'r';
//        cout << "当前正在运行的作业是" << t.name << endl;
//        if (t.lefttime > source)
//        {
//            t.lefttime -= source;
//            before += source;
//            t.state = 'w';
//            q.push(t);
//        }
//        else
//        {
//            t.ftime = before + t.lefttime;
//            before += t.lefttime;
//            t.ztime = t.ftime - t.atime;
//            t.wtime = t.ftime - t.rtime;
//            t.qtime = (double)t.ztime / t.rtime;
//            sum1 += t.ztime;
//            sum2 += t.qtime;
//            t.state = 'f';
//            result[fn] = t;
//            fn++;
//        }
//    }
//    if (fn == n)
//    {
//        cout << "作业已全部完成" << endl;
//    }
//}
//void output(JCB* data)
//{
//    cout << endl << "所有作业完成情况" << endl;
//    cout << "作业名\t" << "    " << "到达时间\t" << "    " 
//        << "运行时间\t" << "    " << "完成时间\t" << "    " 
//        << "周转时间\t" << "    " << "带权周转时间\t" << endl;
//    for (int i = 0; i < n; i++)
//    {
//        cout << data[i].name << "            " << data[i].atime << "                 " 
//            << data[i].rtime << "              " << data[i].ftime << "              " 
//            << data[i].ztime << "              " << data[i].qtime << endl;
//    }
//}
//int main()
//{
//    input();
//    JCB* result = new JCB[n];
//    double sum1 = 0, sum2 = 0;
//    cout << "                                     " << "多道程序系统下的先来先服务算法" << endl;
//    cout << "-----------------------------------------------------------------------------------------------------------" << endl;
//    FCFS(result, sum1, sum2);
//    output(result);
//    cout << "多道程序系统下的先来先服务算法的平均周转时间是" << sum1 / n << endl;
//    cout << "多道程序系统下的先来先服务算法的平均带权周转时间是" << sum2 / n << endl;
//    cout << "-----------------------------------------------------------------------------------------------------------" << endl;
//    delete[] result;
//    return 0;
//}