//#include <iostream>
//#include <queue>
//#include <vector>
//#include <algorithm>
//using namespace std;
//
//struct JCB
//{
//	string _name;
//	int _sub_time;
//	int _run_time;
//	char _state = 'W';
//	double _priority = 0;
//
//	JCB(string name, int sub_time, int run_time)
//		:_name(name), _sub_time(sub_time), _run_time(run_time)
//	{}
//};
//
//struct cmp
//{
//	bool operator()(const JCB& jcb1, const JCB& jcb2)
//	{
//		return jcb1._priority < jcb2._priority;
//	}
//};
//
//priority_queue<JCB,vector<JCB>,cmp> waitQueue;
//queue<JCB> finishQueue;
//
//
//void addWork(JCB jcb)
//{
//	waitQueue.push(jcb);
//}
//
//int main()
//{
//	int n;
//	cout << "请输入作业个数：";
//	cin >> n;
//
//	for (int i = 0; i < n; i++)
//	{
//		string name;
//		int sub_time;
//		int run_time;
//		cout << "请输入作业的作业名，提交时间，所需的运行时间：";
//		cin >> name >> sub_time >> run_time;
//		addWork(JCB(name, sub_time, run_time));
//	}
//	//要求打印每个作业开始运行时刻、完成时刻、
//	//周转时间、带权周转时间，以及这组作业的平均周转时间及带权平均周转时间
//	int prev_finish_times = 0;
//
//	vector<double> Turnaround_time;
//	vector<double> Weighted_turnaround_time;
//
//	for (int i = 0; i < n; i++)
//	{
//		//waitQueue.top()._state = 'R';
//		JCB jcb = waitQueue.top();
//		jcb._state = 'R';
//		waitQueue.pop();
//
//		if (jcb._sub_time < prev_finish_times)
//		{
//			cout << "作业" << i + 1 << "运行时刻：" << prev_finish_times
//			<< "，完成时刻：" << prev_finish_times + jcb._run_time
//			<< "，周转时间：" << prev_finish_times + jcb._run_time - jcb._sub_time
//			<< "，带权周转时间：" << (prev_finish_times + jcb._run_time - jcb._sub_time) / (double)jcb._run_time << endl;
//
//			Turnaround_time.push_back(prev_finish_times + jcb._run_time - jcb._sub_time);
//			Weighted_turnaround_time.push_back((prev_finish_times + jcb._run_time - jcb._sub_time) / (double)jcb._run_time);
//
//			vector<JCB> vect1;
//			while(!waitQueue.empty())
//			{
//				JCB jcb2 = waitQueue.top();
//				waitQueue.pop();
//				jcb2._priority = (prev_finish_times - jcb._sub_time + jcb._run_time) / jcb._run_time;
//				vect1.push_back(jcb2);
//			}
//			while (!vect1.empty())
//			{
//				waitQueue.push(vect1.back());
//				vect1.pop_back();
//			}
//			prev_finish_times = prev_finish_times + jcb._run_time;
//		}
//		else
//		{
//			cout << "作业" << i + 1 << "运行时刻：" << jcb._sub_time
//			<< "，完成时刻：" << jcb._sub_time + jcb._run_time
//			<< "，周转时间：" << jcb._run_time
//			<< "，带权周转时间：" << 1 << endl;
//
//			Turnaround_time.push_back(jcb._run_time);
//			Weighted_turnaround_time.push_back(1);
//			vector<JCB> vect1;
//			while (!waitQueue.empty())
//			{
//				JCB jcb2 = waitQueue.top();
//				waitQueue.pop();
//				jcb2._priority = (prev_finish_times - jcb._sub_time + jcb._run_time) / jcb._run_time;
//				vect1.push_back(jcb2);
//			}
//			while (!vect1.empty())
//			{
//				waitQueue.push(vect1.back());
//				vect1.pop_back();
//			}
//
//			prev_finish_times = jcb._sub_time + jcb._run_time;
//		}
//		jcb._state = 'F';
//		finishQueue.push(jcb);
//	}
//
//	double ave_Turnaround_time = 0;
//	for (int i = 0; i < n; i++)
//	{
//		ave_Turnaround_time += Turnaround_time[i];
//	}
//	ave_Turnaround_time /= n;
//
//	double ave_Weighted_turnaround_time = 0;
//	for (int i = 0; i < n; i++)
//	{
//		ave_Weighted_turnaround_time += Weighted_turnaround_time[i];
//	}
//	ave_Weighted_turnaround_time /= n;
//
//	cout << "这组作业的平均周转时间：" << ave_Turnaround_time
//		<< "，这组作业的带权平均周转时间：" << ave_Weighted_turnaround_time << endl;
//	return 0;
//}
//
