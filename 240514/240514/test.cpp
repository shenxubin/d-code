//#include <iostream>  
//#include <vector>  
//#include <cmath>  
//#include <limits>  
//#include <cstdlib>  
//
//using namespace std;
//
//// 城市之间的距离通过二维坐标计算  
//struct City {
//    double x, y;
//};
//
//// 计算两个城市之间的距离  
//double distance(City a, City b) {
//    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
//}
//
//// 最近邻法解决TSP问题  
//vector<int> nearestNeighborTSP(vector<City>& cities, int start) {
//    int n = cities.size();
//    vector<bool> visited(n, false);
//    vector<int> tour(n);
//
//    tour[0] = start;
//    visited[start] = true;
//
//    for (int i = 1; i < n; ++i) {
//        double minDist = numeric_limits<double>::max();
//        int nextCity = -1;
//
//        for (int j = 0; j < n; ++j) {
//            if (!visited[j]) {
//                double dist = distance(cities[tour[i - 1]], cities[j]);
//                if (dist < minDist) {
//                    minDist = dist;
//                    nextCity = j;
//                }
//            }
//        }
//
//        tour[i] = nextCity;
//        visited[nextCity] = true;
//    }
//
//    // 添加回到起始城市的路径（通常假设距离为0，或者计算实际距离）  
//    // 假设回到起始城市的距离为0  
//
//    return tour;
//}
//
//int main() {
//    vector<City> cities = {
//        {0, 0},
//        {0, 2},
//        {2, 0},
//        {2, 2},
//        {1, 1}
//    };
//
//    int startCity = 0; // 假设从第一个城市开始  
//    vector<int> tour = nearestNeighborTSP(cities, startCity);
//
//    cout << "Tour: ";
//    for (int i = 0; i < tour.size(); ++i) {
//        cout << "(" << cities[tour[i]].x << ", " << cities[tour[i]].y << ") ";
//    }
//    cout << endl;
//
//    return 0;
//}