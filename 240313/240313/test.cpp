#include "vector.h"


void test1()
{
	sxb::vector<int> vect(10, 1);
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test2()
{
	sxb::vector<int> vect(10, 1);
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;

	vect.reserve(100);
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << vect.capacity() << endl;
}

void test3()
{
	sxb::vector<int> vect(10, 1);
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;

	sxb::vector<int> vect2(vect.begin(), vect.begin() + 4);
	for (auto e : vect2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test4()
{
	sxb::vector<int> vect(10, 1);
	sxb::vector<int> vect2(vect);

	for (auto e : vect2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test5()
{
	sxb::vector<int> vect(10, 1);
	sxb::vector<int> vect2 = vect;

	for (auto e : vect2)
	{
		cout << e << " ";
	}
	cout << endl;
}

//void test6()
//{
//	sxb::vector<int> vect(10, 1);
//	vect.resize(20, 2);
//	sxb::vector<int>::iterator it = vect.begin();
//	while (it != vect.fcapacity())
//	{
//		cout << *it << " ";
//		it++;
//	}
//	cout << endl;
//}

void test7()
{
	sxb::vector<int> vect(10, 1);
	int arr[10] = { 1,2,3,4 ,5,6,7,8,9};
	for (int i = 0; i < 9; i++)
	{
		vect.push_back(arr[i]);
	}
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;

	vect.pop_back();
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test8()
{
	sxb::vector<int> vect(10, 1);
	vect.insert(vect.end(), 10);
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test9()
{
	sxb::vector<int> vect(10, 1);
	int arr[10] = { 1,2,3,4 ,5,6,7,8,9 };
	for (int i = 0; i < 9; i++)
	{
		vect.push_back(arr[i]);
	}
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;

	vect.erase(vect.begin());
	for (auto e : vect)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test9();
	return 0;
}