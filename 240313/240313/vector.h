#pragma once
#include <iostream>
using namespace std;

namespace sxb
{
    template<class T>

    class vector
    {
    public:
        typedef T* iterator;
       
    private:

        iterator _start; // 指向数据块的开始

        iterator _finish; // 指向有效数据的尾

        iterator _endOfStorage; // 指向存储容量的尾

    public:

        // Vector的迭代器是一个原生指针
       
        typedef const T* const_iterator;

        iterator begin()
        {
            return _start;
        }

        iterator end()
        {
            return _finish;
        }

        const_iterator cbegin() const
        {
            return _start;
        }

        const_iterator cend() const
        {
            return _finish;
        }

            // construct and destroy

        vector() {}

        vector(int n, const T& value = T())
        {
            _start = new T[n+1];
            _finish = _start + n;
            _endOfStorage = _start + n;

            for (int i = 0; i < n; i++)
            {
                *(_start + i) = value;
            }
        }

            template<class InputIterator>

            vector(InputIterator first, InputIterator last)
            {
                int len = 0;
                while (first != last)// 1 2 3 4 5
                {
                    len++;
                    first++;
                }
                len++;
                _start = new T[len+1];
                memcpy(_start, first, sizeof(T) * len);
                _finish = _start + len;
                _endOfStorage = _finish;

            }

            vector(const vector<T>& v)
            {
                _start = new T[v.size() + 1];
                memcpy(_start, v.cbegin(), sizeof(T) * v.size());
                _finish = _start + v.size();
                _endOfStorage = _finish;
            }

            vector<T>& operator= (vector<T> v)
            {
                vector(v);

                return *this;
            }

            ~vector()
            {
                delete[] _start;
                _finish = nullptr;
                _endOfStorage = nullptr;
            }

            // capacity

            size_t size() const
            {
                return _finish - _start;
            }

            size_t capacity() const
            {
                return _endOfStorage - _start;
            }

            void reserve(size_t n)
            {
                if (n > capacity())
                {
                    T* tmp = new T[n + 1];
                    int oldnum = _finish - _start;
                    memcpy(tmp, _start, sizeof(T) * (_finish - _start));
                    delete[] _start;
                    _start = tmp;
                    _finish = _start + oldnum;
                    _endOfStorage = _start + n;
                }
            }

            void resize(size_t n, const T& value = T())
            {
                if (n > capacity())
                {
                    int oldnum = _finish - _start;
                    reserve(n);
                    while ((_start + oldnum) != _endOfStorage)
                    {
                        *(_start + oldnum) = value;
                        oldnum++;
                    }
                }
                else
                {
                    _finish = _start + n;
                }
            }



            ///////////////access///////////////////////////////

            T& operator[](size_t pos)
            {
                return *(_start + pos);
            }

            const T& operator[](size_t pos)const
            {
                return *(_start + pos);
            }



            ///////////////modify/////////////////////////////

            void push_back(const T& x)
            {
                if (size() == capacity())
                {
                    reserve(capacity() == 0 ? 4 : capacity() * 2);
                }
                _start[size()] = x;
                _finish++;
            }

            void pop_back()
            {
                _finish--;
            }

            void swap(vector<T>& v)
            {
                std::swap(_start, v._start);
                std::swap(_finish, v._finish);
                std::swap(_endOfStorage, v._endOfStorage);
            }
            //迭代器失效
            iterator insert(iterator pos, const T& x)
            {
                int len = 0;
                while (pos != _start)
                {
                    len++;
                    pos--;
                }
                if (size() == capacity())
                {
                    reserve(capacity() == 0 ? 4 : capacity() * 2);
                }
                iterator endfinish = _finish;
                while (endfinish > _start + len)
                {
                    *(endfinish) = *(endfinish - 1);
                    endfinish--;
                }
                *endfinish = x;
                _finish++;

                return _start + len;
            }

            iterator erase(iterator pos)
            {
                iterator pos2 = pos;
                while (pos != _finish - 1)
                {
                    *pos = *(pos + 1);
                    pos++;
                }
                _finish--;
                return pos2;
            }
    };

}

