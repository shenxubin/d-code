#include <iostream>
using namespace std;

#define PI 3.141592
class Shape
{
public:
	virtual double Area() = 0;
	virtual void Show() = 0;
	virtual ~Shape() {};
	friend bool operator==(const Shape& p1, const Shape& p2);
	friend bool operator>(const Shape& p1, const Shape& p2);
	friend bool operator<(const Shape& p1, const Shape& p2);
};

bool operator==( Shape& p1,  Shape& p2)
{
	if (p1.Area() == p2.Area())
	{
		return true;
	}
	return false;
}

bool operator>( Shape& p1,  Shape& p2)
{
	if (p1.Area() > p2.Area())
	{
		return true;
	}
	return false;
}

bool operator<(Shape& p1, Shape& p2)
{
	if (p1.Area() < p2.Area())
	{
		return true;
	}
	return false;
}

class Rectangle :public Shape
{
protected:
	double rectWidth;
	double rectHeight;
public:
	Rectangle(double rectWidth = 0, double rectHeight = 0)
	{
		this->rectWidth = rectWidth;
		this->rectHeight = rectHeight;
	}
	virtual double Area()
	{
		return this->rectHeight * this->rectWidth;
	}
	virtual void Show()
	{
		cout << "W: " << this->rectWidth
			<< "; H: " << this->rectHeight
			<< "; Area: " << this->Area() << endl;
	}
};

class Ellipse :public Shape
{
protected:
	double rectWidth;
	double rectHeight;
public:
	Ellipse(double rectWidth = 0, double rectHeight = 0)
	{
		this->rectWidth = rectWidth;
		this->rectHeight = rectHeight;
		
	}
	virtual double Area()
	{
		return PI * this->rectHeight * this->rectWidth / 4;
	}
	virtual void Show()
	{
		cout << "W: " << this->rectWidth
			<< "; H: " << this->rectHeight
			<< "; Area: " << this->Area() << endl;
	}
};
int main()
{
	int w = 0, h = 0;
	int i = 0, j = 0;
	int n = 0;
	char c = 0;
	cin >> n;
	Shape** arr = new Shape * [n];
	for (int i = 0; i < n; i++)
	{
		cin >> c >> w >> h;
		if (c == 'R')
		{
			arr[i] = new Rectangle(w, h);
		}
		else if (c == 'E')
		{
			arr[i] = new Ellipse(w, h);
		}
	}
	for (int i = 0; i < n; i++)
	{
		arr[i]->Show();
	}
	for (i = 0; i < n-1; i++)
	{
		for (j = i+1; j < n; j++)
		{
			if (arr[i]->Area() == arr[j]->Area())
			{
				cout << "Area of Shape[" << i
					<< "] is equal to Shape[" << j << "]" << endl;
			}
		}
	}
	for (int i = 0; i < n - 1; i++)
	{
		for (int j = i+1; j < n; j++)
		{
			if (arr[i]->Area() >= arr[j]->Area())
			{
				Shape* tmp = arr[i];
				arr[i] = arr[j];
				arr[j] = tmp;
			}
		}
	}
	for (int i = n-1; i >=0; i--)
	{
		arr[i]->Show();
	}
	return 0;
}




//#include <iostream>
//using namespace std;
//
//class Shape
//{
//public:
//	virtual double Area() = 0;
//
//	virtual void Show() = 0;
//
//	bool operator==(Shape& a)
//	{
//		if (this->Area() == a.Area())
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	bool operator>(Shape& a)
//	{
//		if (this->Area() > a.Area())
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//
//	bool operator<(Shape& a)
//	{
//		if (this->Area() < a.Area())
//		{
//			return true;
//		}
//		else
//		{
//			return false;
//		}
//	}
//};
//
//class Rectangle : public Shape
//{
//protected:
//	double rectWidth;
//	double rectHeight;
//
//public:
//	Rectangle(double rectWidth, double rectHeight)
//	{
//		this->rectWidth = rectWidth;
//		this->rectHeight = rectHeight;
//	}
//
//	void Show()
//	{
//		cout << "W: " << rectWidth << "; H:" << rectHeight << "; Area: " << this->Area() << endl;
//	}
//
//	double Area()
//	{
//		return rectHeight * rectWidth;
//	}
//};
//
//class Ellipse : public Shape
//{
//protected:
//	double rectWidth;
//	double rectHeight;
//
//public:
//	Ellipse(double rectWidth, double rectHeight)
//	{
//		this->rectWidth = rectWidth;
//		this->rectHeight = rectHeight;
//	}
//
//	void Show()
//	{
//		cout << "W: " << rectWidth << "; H:" << rectHeight << "; Area: " << this->Area() << endl;
//	}
//
//	double Area()
//	{
//		return 0.785398 * rectWidth * rectHeight;
//	}
//};
//
//int main()
//{
//	int i, j;
//	int n;
//	cin >> n;
//
//	Shape** arr = new Shape * [n];
//
//	for (i = 0; i < n; i++)
//	{
//		char x;
//		double w, h;
//		cin >> x >> w >> h;
//		if (x == 'R')
//		{
//			arr[i] = new Rectangle(w, h);
//		}
//		else if (x == 'E')
//		{
//			arr[i] = new Ellipse(w, h);
//		}
//	}
//
//	for (i = 0; i < n; i++)
//	{
//		arr[i]->Show();
//	}
//
//	for (i = 0; i < n; i++)
//	{
//		for (j = i + 1; j < n; j++)
//		{
//			if (arr[i]->Area() == arr[j]->Area())
//			{
//				cout << "Area of Shape[" << i << "] is equal to Shape[" << j << "]" << endl;
//			}
//		}
//	}
//
//	for (i = 0; i < n; i++)
//	{
//		for (j = i; j < n; j++)
//		{
//			if (arr[i]->Area() < arr[j]->Area())
//			{
//				Shape* temp;
//				temp = arr[i];
//				arr[i] = arr[j];
//				arr[j] = temp;
//			}
//		}
//	}
//
//	for (i = 0; i < n; i++)
//	{
//		arr[i]->Show();
//	}
//
//	return 0;
//}






















