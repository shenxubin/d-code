#include <iostream>
#include <vector>
#include "Thread.hpp"

using namespace ThreadModule;

// 数据不一致
// int g_tickets = 10000; // 共享资源，没有保护的

// void route(int &tickets)
// {
//     while (true)
//     {
//         if(tickets>0)
//         {
//             usleep(1000);
//             printf("get tickets: %d\n", tickets);
//             tickets--;
//         }
//         else
//         {
//             break;
//         }
//     }
// }

// const int num = 4;
// int main()
// {
//     // std::cout << "main： &tickets: " << &g_tickets << std::endl;

//     std::vector<Thread<int>> threads;
//     // 1. 创建一批线程
//     for (int i = 0; i < num; i++)
//     {
//         std::string name = "thread-" + std::to_string(i + 1);
//         threads.emplace_back(route, g_tickets, name);
//     }

//     // 2. 启动 一批线程
//     for (auto &thread : threads)
//     {
//         thread.Start();
//     }

//     // 3. 等待一批线程
//     for (auto &thread : threads)
//     {
//         thread.Join();
//         std::cout << "wait thread done, thread is: " << thread.name() << std::endl;
//     }

//     return 0;
// }

using namespace ThreadModule;

void print(int cnt)
{
    while (cnt)
    {
        std::cout << "hello I am myself thread, cnt: " << cnt-- << std::endl;
        sleep(1);
    }
}

const int num = 10;

int main()
{
    std::vector<Thread<int> > threads;
    // 1. 创建一批线程
    for (int i = 0; i < num; i++)
    {
        int n = 10;
        std::string name = "thread-" + std::to_string(i + 1);
        threads.emplace_back(print, n, name);
    }

    // 2. 启动 一批线程
    for (auto &thread : threads)
    {
        thread.Start();
    }

    // 3. 等待一批线程
    for (auto &thread : threads)
    {
        thread.Join();
        std::cout << "wait thread done, thread is: " << thread.name() << std::endl;
    }

    // Thread<int> t1(print, 10);
    // t1.Start();
    // std::cout << "name: " << t1.name() << std::endl;
    // t1.Join();
    return 0;
}