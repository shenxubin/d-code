#ifndef __TESTTHREAD_HPP__
#define __TESTTHREAD_HPP__

#include <iostream>
#include <pthread.h>
#include <string>
#include <functional>
#include <unistd.h>

namespace testModule
{
    template <class T>
    using func_t = std::function<void(T)>;

    template <class T>
    class thread
    {
    private:
        pthread_t _tid;
        std::string _threadname;
        T _data; 
        func_t<T> _func;
        bool _stop;
    
    public:
        thread(func_t<T> func,T data, std::string name)
        :_func(func),_data(data), _threadname(name), _stop(true)
        {

        }

        void Excute()
        {
            _func(_data);
        }
        //除去this指针
        static void* threadRun(void* args)
        {
            thread<T>* t = static_cast<thread<T>*> (args);
            t->Excute();
            return nullptr;
        }

        bool Start()
        {
            int n = pthread_create(&_tid, nullptr, threadRun, this);
            if(!n)
            {
                _stop = false;
                return true;
            }
            else
                return false;
        }

        void Detach()
        {
            if(!_stop)
                pthread_detach(_tid);
        }

        void Join()
        {
            if(!_stop)
                pthread_join(_tid, nullptr);
        }

        std::string getName()
        {
            return this->_threadname;
        }
        ~thread()
        {}
    };
};

#endif