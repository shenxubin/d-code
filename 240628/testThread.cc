
#include "testThread.hpp"
#include <vector>
using namespace testModule;

void testRun(int i)
{
    while(i--)
    {
        std::cout << "pid:" << getpid() << " i=" << i << std::endl;
        sleep(1); 
    }
}
int main()
{
    

    std::vector<thread<int>> threads;
    for(int j = 0; j<5; j++)
    {
        int n = 10;
        thread<int> t(testRun, n, std::to_string(j+1));
        threads.push_back(t);
    }

    
    for(auto& thread : threads)
    {
        thread.Start();
    }

    for(auto& thread : threads)
    {
        thread.Join();
        std::cout << thread.getName()<< std::endl;
    }

    return 0;
}