#pragma once

#include <iostream>
#include <assert.h>
using namespace std;

template <class K, class V>
struct  AVLTreeNode
{
	AVLTreeNode<K, V>* _parent;
	AVLTreeNode<K, V>* _left;
	AVLTreeNode<K, V>* _right;
	pair<K, V> _kv;
	int _bf;//balance fator 平衡因子

	AVLTreeNode(const pair<K, V>& kv)
		:_parent(nullptr)
		,_left(nullptr)
		,_right(nullptr)
		,_kv(kv)
		,_bf(0)
	{}

};

template <class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> node;
public:
	bool Insert(const pair<K, V>& kv);
	void RotateL(node* parent);
	void RotateR(node* parent);
	void RotateRL(node* parent);
	void RotateLR(node* parent);
	void InOrder();
	void _InOrder(node* root);
	int Height();
	int _Height(node* root);
	bool IsBalance();
	bool _IsBalance(node* root);
	bool Delete(const pair<K, V>& kv);
private:
	node* _root = nullptr;
};

template <class K, class V>
bool AVLTree<K, V>::Insert(const pair<K, V>& kv)
{
	node* parent = nullptr;
	node* cur = _root;

	if (cur == nullptr)
	{
		_root = new node(kv);
		return true;
	}
	while (cur)
	{
		if (cur->_kv.first < kv.first)
		{
			parent = cur;
			cur = cur->_right;
		}
		else if (cur->_kv.first > kv.first)
		{
			parent = cur;
			cur = cur->_left;
		}
		else
		{
			return false;
		}
	}
	cur = new node(kv);
	if (parent->_kv.first < kv.first)
	{
		parent->_right = cur;
		cur->_parent = parent;
	}
	else
	{
		parent->_left = cur;
		cur->_parent = parent;
	}

	while (parent)
	{
		if (cur == parent->_right)
		{
			parent->_bf++;
		}
		else
		{
			parent->_bf--;
		}

		if (parent->_bf == 0)
		{
			break;
		}
		else if (parent->_bf == -1 || parent->_bf == 1)
		{
			cur = parent;
			parent = parent->_parent;
		}
		else if (parent->_bf == -2 || parent->_bf == 2)
		{
			//旋转
			if (parent->_bf == 2 && cur->_bf == 1)
			{
				RotateL(parent);
			}
			else if (parent->_bf == -2 && cur->_bf == -1)
			{
				RotateR(parent);
			}
			else if (parent->_bf == 2 && cur->_bf == -1)
			{
				RotateRL(parent);
			}
			else if (parent->_bf == -2 && cur->_bf == 1)
			{
				RotateLR(parent);
			}
			else
			{
				assert(false);
			}
			break;
			return true;
		}
		else
		{
			assert(false);
		}
	}
}

template <class K, class V>
void AVLTree<K, V>::RotateL(node* parent)
{
	node* subR = parent->_right;
	node* subRL = subR->_left;
	node* parentParent = parent->_parent;

	parent->_right = subRL;
	subR->_left = parent;

	parent->_parent = subR;
	if (subRL)
	{
		subRL->_parent = parent;
	}

	if (_root == parent)
	{
		subR->_parent = nullptr;
		_root = subR;
	}
	else
	{
		if (parent == parentParent->_left)
		{
			parentParent->_left = subR;
		}
		else
		{
			parentParent->_right = subR;
		}
		subR->_parent = parentParent;
	}
	subR->_bf = parent->_bf = 0;
}

template <class K, class V>
void AVLTree<K, V>::RotateR(node* parent)
{
	node* subL = parent->_left;
	node* subLR = subL->_right;
	node* parntParent = parent->_parent;

	parent->_left = subLR;
	subL->_right = parent;
	
	parent->_parent = subL;
	if (subLR)
	{
		subLR->_parent = parent;
	}

	if (_root == parent)
	{
		subL->_parent = nullptr;
		_root = subL;
	}
	else
	{
		if (parntParent->_left == parent)
		{
			parntParent->_left = subL;
		}
		else
		{
			parntParent->_right = subL;
		}
		subL->_parent = parntParent;
	}
	subL->_bf = parent->_bf = 0;
}


template<class K, class V>
void AVLTree<K, V>::RotateRL(node* parent)
{
	node* subR = parent->_right;
	node* subRL = subR->_left;
	int bf = subRL->_bf;

	RotateR(subR);
	RotateL(parent);

	if (bf == 0)
	{
		parent->_bf = subR->_bf = subRL->_bf = 0;
	}
	else if (bf == -1)
	{
		parent->_bf = subRL->_bf = 0;
		subR->_bf = 1;
	}
	else if (bf == 1)
	{
		subR->_bf = subRL->_bf = 0;
		parent->_bf = -1;
	}
	else
	{
		assert(false);
	}
}

template <class K, class V>
void AVLTree<K, V>::RotateLR(node* parent)
{
	node* subL = parent->_left;
	node* subLR = subL->_right;
	int bf = subLR->_bf;

	RotateL(subL);
	RotateR(parent);

	if (bf == 0)
	{
		parent->_bf = subL->_bf = subLR->_bf = 0;
	}
	else if (bf == -1)
	{
		subL->_bf = subLR->_bf = 0;
		parent->_bf = 1;
	}
	else if (bf == 1)
	{
		subLR->_bf = parent->_bf = 0;
		subL->_bf = -1;
	}
	else
	{
		assert(false); 
	}
}

template <class K, class V>
void AVLTree<K, V>::InOrder()
{
	 _InOrder(_root);
	 cout << endl;
}

template <class K, class V>
void AVLTree<K, V>::_InOrder(node* root)
{
	if (root == nullptr)
		return;
	_InOrder(root->_left);
	cout << root->_kv.first << " ";
	_InOrder(root->_right);
}

template <class K, class V>
int AVLTree<K, V>::Height()
{
	return _Height(_root);
}

template <class K, class V>
int AVLTree<K, V>::_Height(node* root)
{
	if (root == nullptr)
		return 0;

	int leftHeight = _Height(root->_left);
	int rightHeight = _Height(root->_right);

	return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
}

template <class K, class V>
bool AVLTree<K, V>::IsBalance()
{
	return _IsBalance(_root);
}

template <class K, class V>
bool AVLTree<K, V>::_IsBalance(node* root)
{
	if (root == nullptr)
		return true;

	int leftHeight = _Height(root->_left);
	int rightHeight = _Height(root->_right);

	if (rightHeight - leftHeight != root->_bf)
	{
		cout << "平衡因子异常！" << endl;
		return false;
	}

	return abs(rightHeight - leftHeight) < 2 && _IsBalance(root->_left) && _IsBalance(root->_right);
}


template <class K, class V>
bool AVLTree<K, V>::Delete(const pair<K, V>& kv)
{

}