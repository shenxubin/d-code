#include "AVLTree.h"

int main()
{
	int arr[] = { 1,2,34,5,678,256,23,10,110 ,9,8,11,12};
	AVLTree<int, int> AVLTree1;
	for (int i = 0; i < 11; i++)
	{
		AVLTree1.Insert(make_pair(arr[i], arr[i]));
	}
	AVLTree1.InOrder();

	int height = AVLTree1.Height();
	cout << height << endl;

	bool ret = AVLTree1.IsBalance();
	cout << ret << endl;
	return 0;
}