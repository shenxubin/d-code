class Solution {
public:
    bool lemonadeChange(vector<int>& bills)
    {
        int FiveDollarNumber = 0;
        int TenDollarNumber = 0;
        for (int i = 0; i < bills.size(); i++)
        {
            if (bills[i] == 5)
                FiveDollarNumber++;
            else if (bills[i] == 10)
            {
                if (FiveDollarNumber == 0)
                {
                    return false;
                }
                else
                {
                    TenDollarNumber++;
                    FiveDollarNumber--;
                }
            }
            else
            {
                if (TenDollarNumber >= 1 && FiveDollarNumber >= 1)
                {
                    TenDollarNumber--;
                    FiveDollarNumber--;
                    continue;
                }
                else if (FiveDollarNumber >= 3)
                {
                    FiveDollarNumber -= 3;
                }
                else
                    return false;
            }
        }

        return true;
    }
};