#include <iostream>
#include <vector>
using namespace std;

void _hanota(int n, vector<int>& A, vector<int>& B, vector<int>& C)
{
    if (n == 1)
    {
        C.push_back(A.back());
        A.pop_back();
        return;
    }
    _hanota(n - 1, A, C, B);
    C.push_back(A.back());
    A.pop_back();
    _hanota(n - 1, B, A, C);
}

void hanota(vector<int>& A, vector<int>& B, vector<int>& C)
{
    _hanota(A.size(), A, B, C);
}

int main()
{
    vector<int> A; vector<int> B; vector<int> C;
    A.push_back(1);
    A.push_back(0);
    hanota( A, B, C);
    for (auto e : C)
    {
        cout << e << " ";
    }
    cout << endl;
}