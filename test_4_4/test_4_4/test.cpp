//#include <iostream>
//using namespace std;
//
//class Base
//{
//public:
//    Base()
//    {
//        m_A = 100;
//    }
//    void func()
//    {
//        cout << "Base 下 func函数调用" << endl;
//    }
//    int m_A;
//};
//
//class Son : public Base
//{
//public:
//    Son()
//    {
//        m_A = 200;
//    }
//    void func()
//    {
//        cout << "Son 下 func函数调用" << endl;
//    }
//    int m_A;
//};
//
//void test01()
//{
//    Son s1;
//    cout << s1.m_A << endl;
//    cout << s1.Base::m_A << endl;
//}
//
//void test02()
//{
//    Son s2;
//    s2.func();
//    s2.Base::func();
//}
//int main()
//{
//    /*test01();*/
//    test02();
//    system("pause");
//    return 0;
//}


//#include <iostream>
//using namespace std;
//
//class Base
//{
//public:
//
//    static int m_A;
//    int m_B;
//    Base()
//    {
//        m_B = 100;
//    }
//};
//int Base::m_A = 100;
//
//class Son:public Base
//{
//public:
//    static int m_A;
//};
//int Son::m_A = 200;
//
//void test01()
//{
//    Son s1;
//    cout << s1.m_A << endl;
//    cout << s1.Base::m_A << endl;
//    cout << Son::m_A << endl;
//    cout << Son::Base::m_A << endl;
//    cout << s1.m_B << endl;
//}
//int main()
//{
//    test01();
//
//    system("pause");
//    return 0;
//}

//菱形继承
#include <iostream>
using namespace std;

class Animal
{
public:
    int m_A;
};

class Sheep :virtual public Animal
{


};

class Tuo :virtual public Animal
{


};

class SheepTuo :public Sheep, public Tuo
{


};

void test01()
{
    SheepTuo st;
    st.Sheep::m_A = 18;
    st.Tuo::m_A = 28;
    cout << st.Sheep::m_A << endl;
    cout << st.Tuo::m_A << endl;
    cout << st.m_A << endl;
}
int main()
{
    test01();
    return 0;
}














