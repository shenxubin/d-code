/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
ListNode* _reverseList(ListNode*& finish, ListNode* head)
{
    if (head == nullptr || head->next == nullptr)
    {
        finish = head;
        return head;
    }

    ListNode* cur = head->next;
    return _reverseList(finish, cur)->next = head;
}

ListNode* reverseList(ListNode*& finish, ListNode* head)
{
    _reverseList(finish, head);
    if (head)
        head->next = nullptr;
    return finish;
}

ListNode* _reverseList2(ListNode*& finish2, ListNode* head)
{
    if (head->next->next == nullptr)
    {
        finish2 = head;
        return head;
    }

    ListNode* cur = head->next->next;
    return _reverseList2(finish2, cur)->next->next = head;
}

ListNode* reverseList2(ListNode*& finish2, ListNode* head)
{
    _reverseList2(finish2, head);
    head->next->next = nullptr;
    return finish2;
}

ListNode* _reverseList21(ListNode*& finish2, ListNode* head)
{
    if (head->next->next == nullptr)
    {
        finish2 = head;
        return head;
    }

    ListNode* cur = head->next->next;
    return (_reverseList21(finish2, cur))->next->next = head;
}

ListNode* reverseList21(ListNode*& finish2, ListNode* head2, ListNode* head)
{
    _reverseList21(finish2, head);
    head->next->next = head2;
    head2->next = nullptr;
    return finish2;
}
class Solution {
public:
    ListNode* swapPairs(ListNode* head)
    {
        int len = 0;
        ListNode* finish = nullptr;
        ListNode* finish2 = nullptr;
        ListNode* test = head;
        while (test != nullptr)
        {
            len++;
            test = test->next;
        }
        if (head == nullptr || head->next == nullptr)
            return head;

        if (len % 2 == 0)
        {
            ListNode* ret1 = reverseList(finish, head);
            return reverseList2(finish2, ret1);
        }
        else
        {
            ListNode* ret1 = reverseList(finish, head);
            return reverseList21(finish2, ret1, ret1->next);
        }
    }
};