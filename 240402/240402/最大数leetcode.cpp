class Solution {
public:

    static bool cmp(const string& v1, const string& v2)
    {
        return v1 + v2 > v2 + v1;
    }

    string largestNumber(vector<int>& nums)
    {
        vector<string> vect;
        for (auto e : nums)
        {
            vect.push_back(to_string(e));
        }
        sort(vect.begin(), vect.end(), cmp);
        string str;
        for (auto& e : vect)
        {
            str += e;
        }
        if (str[0] == '0')
            return "0";
        return str;
    }
};