#include <iostream>
using namespace std;

class Rational
{
    friend Rational operator+(const Rational& r1, const Rational& r2);
    friend Rational operator-(const Rational& r1, const Rational& r2);
    friend Rational operator*(const Rational& r1, const Rational& r2);
    friend Rational operator/(const Rational& r1, const Rational& r2);
    friend ostream& operator<<(ostream& cout, Rational& r);
    friend istream& operator>>(istream& cin, Rational& r);
    friend bool operator<(const Rational& r1, const Rational& r2);
    friend bool operator>=(const Rational& r1, const Rational& r2);
    friend bool operator>(const Rational& r1, const Rational& r2);
    friend bool operator<=(const Rational& r1, const Rational& r2);
private:
    int iUp;
    int iDown;
    void Reduce()
    {
        this->iUp = this->iUp / Gcd(iUp, iDown);
        this->iDown = this->iUp / Gcd(iUp, iDown);
    }
    int Gcd(int l, int r)
    {
        int m = 2, x = l, y = r;
        while (m)
        {
            m = x % y;
            x = y;
            y = m;
        }
        if (x < 0)
        {
            x = -x;
        }
        return x;
    }
public:
    Rational(int up = 1, int down = 1)
    {
        this->iUp = up;
        this->iDown = down;
        Reduce();
    }
    Rational& operator-()
    {
        this->iUp = this->iUp - 2 * this->iUp;
        if ((this->iUp < 0 && this->iDown < 0) || (this->iUp > 0 && this->iDown < 0))
        {
            this->iUp = -this->iUp;
            this->iDown = -this->iDown;
        }
        return *this;
    }
    Rational& operator++()
    {
        this->iUp = this->iUp + iDown;
        if ((this->iUp < 0 && this->iDown < 0) || (this->iUp > 0 && this->iDown < 0))
        {
            this->iUp = -this->iUp;
            this->iDown = -this->iDown;
        }
        return *this;
    }
    Rational& operator--()
    {
        this->iUp = this->iUp - iDown;
        if ((this->iUp < 0 && this->iDown < 0) || (this->iUp > 0 && this->iDown < 0))
        {
            this->iUp = -this->iUp;
            this->iDown = -this->iDown;
        }
        return *this;
    }
    Rational operator++(int)
    {
        Rational r = *this;
        this->iUp = this->iUp + iDown;
        if ((this->iUp < 0 && this->iDown < 0) || (this->iUp > 0 && this->iDown < 0))
        {
            this->iUp = -this->iUp;
            this->iDown = -this->iDown;
        }
        return r;
    }
    Rational operator--(int)
    {
        Rational r = *this;
        this->iUp = this->iUp - iDown;
        if ((this->iUp < 0 && this->iDown < 0) || (this->iUp > 0 && this->iDown < 0))
        {
            this->iUp = -this->iUp;
            this->iDown = -this->iDown;
        }
        return r;
    }
};

Rational operator+(const Rational& r1, const Rational& r2)
{
    Rational r3;
    r3.iDown = r1.iDown * r2.iDown;
    r3.iUp = r1.iUp * r2.iDown + r2.iUp * r1.iDown;
    int r = 1, m = r3.iUp, n = r3.iDown;
    while (r)
    {
        r = m % n;
        m = n;
        n = r;
    }
    r3.iUp = r3.iUp / m;
    r3.iDown = r3.iDown / m;
    if ((r3.iUp > 0 && r3.iDown < 0) || (r3.iUp < 0 && r3.iDown < 0))
    {
        r3.iUp = -r3.iUp;
        r3.iDown = -r3.iDown;
    }
    return r3;
}
Rational operator-(const Rational& r1, const Rational& r2)
{
    Rational r3;
    r3.iDown = r1.iDown * r2.iDown;
    r3.iUp = r1.iUp * r2.iDown - r2.iUp * r1.iDown;
    int r = 1, m = r3.iUp, n = r3.iDown;
    while (r)
    {
        r = m % n;
        m = n;
        n = r;
    }
    r3.iUp = r3.iUp / m;
    r3.iDown = r3.iDown / m;
    if ((r3.iUp > 0 && r3.iDown < 0) || (r3.iUp < 0 && r3.iDown < 0))
    {
        r3.iUp = -r3.iUp;
        r3.iDown = -r3.iDown;
    }
    return r3;
}

Rational operator*(const Rational& r1, const Rational& r2)
{
    Rational r3;
    r3.iDown = r1.iDown * r2.iDown;
    r3.iUp = r1.iUp * r2.iUp;
    int r = 1, m = r3.iUp, n = r3.iDown;
    while (r)
    {
        r = m % n;
        m = n;
        n = r;
    }
    r3.iUp = r3.iUp / m;
    r3.iDown = r3.iDown / m;
    if ((r3.iUp > 0 && r3.iDown < 0) || (r3.iUp < 0 && r3.iDown < 0))
    {
        r3.iUp = -r3.iUp;
        r3.iDown = -r3.iDown;
    }
    return r3;
}

Rational operator/(const Rational& r1, const Rational& r2)
{
    Rational r3;
    r3.iDown = r1.iDown * r2.iUp;
    r3.iUp = r1.iUp * r2.iDown;
    int r = 1, m = r3.iUp, n = r3.iDown;
    while (r)
    {
        r = m % n;
        m = n;
        n = r;
    }
    r3.iUp = r3.iUp / m;
    r3.iDown = r3.iDown / m;
    if ((r3.iUp > 0 && r3.iDown < 0) || (r3.iUp < 0 && r3.iDown < 0))
    {
        r3.iUp = -r3.iUp;
        r3.iDown = -r3.iDown;
    }
    return r3;
}

bool operator<(const Rational& r1, const Rational& r2)
{
    int a = r1.iUp, b = r1.iDown, c = r2.iUp, d = r2.iDown;
    int m = a * d;
    int n = b * c;
    if (m < n)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator>=(const Rational& r1, const Rational& r2)
{
    return !(r1 < r2);
}

bool operator>(const Rational& r1, const Rational& r2)
{
    int a = r1.iUp,b = r1.iDown,c = r2.iUp,d = r2.iDown;
    int m = a * d;
    int n = b * c;
    if (m > n)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator<=(const Rational& r1, const Rational& r2)
{
    return !(r1 > r2);
}

ostream& operator<<(ostream& cout, Rational& r)
{
    if (r.iUp == 0)
    {
        cout << "0" << endl;
    }
    else if (r.iDown == 1)
    {
        cout << r.iUp << endl;
    }
    else
    {
        cout << r.iUp << "/" << r.iDown << endl;
    }
    return cout;
}

istream& operator>>(istream& cin, Rational& r)
{
    cin >> r.iUp >> r.iDown;
    return cin;
}
int main()
{
    Rational a, b;
    cin >> a >> b;
    Rational c = a + b;
    Rational d = a - b;
    Rational e = a * b;
    Rational f = a / b;
    Rational g = ++a;
    Rational h = --a;
    Rational i = a++;
    Rational j = a--;
    bool k = a < b;
    bool l = a <= b;
    bool m = a > b;
    bool n = a >= b;
    cout << "a+b: " << c ;
    cout << "a-b: " << d ;
    cout << "a*b: " << e ;
    cout << "a/b: " << f ;
    cout << "-a: " << -a ;
    cout << "++a: " << g ;
    cout << "--a: " << h ;
    cout << "a++: " << i ;
    cout << "a--: " << j ;
    cout << boolalpha;
    cout << "a<b: " << k <<endl;
    cout << "a<=b: " << l<<endl;
    cout << "a>b: " << m <<endl;
    cout << "a>=b: " << n<<endl;
    return 0;
}


