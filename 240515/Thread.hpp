#ifndef __THREAD_HPP__
#define __THREAD_HPP__

#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <functional>
#include <string>

namespace MyThread
{
    template<typename T>
    using func_t = std::function<void(T&)>;

    template <typename T>
    class Thread
    {
    private:
        pthread_t _tid;
        std::string _threadname;
        T _data;
        func_t<T> _func;
        bool _stop;
    public:
        Thread(T& data, func_t<T> func,const std::string& name = "none-name")
        :_threadname(name), _data(data), _func(func), _stop(true)
        {}

        ~Thread(){}

        void Excute()
        {
            _func(_data);
        }

        static void* threadroutine(void* args)
        {
            Thread<T>* self = (Thread<T>*) args;
            self->Excute();
            return nullptr;
        }

        bool Start()
        {
            int n = pthread_create(&_tid, nullptr, threadroutine, this);

            if(!n)
            {
                _stop = false;
                return true;
            }
            else
                return false;
        }

        void Detach()
        {
            if(!_stop)
                pthread_detach(_tid);
        }

        void Join()
        {
            if(!_stop)
                pthread_join(_tid, nullptr);
        }

        std::string name()
        {
            return _threadname;
        }

        void Stop()
        {
            _stop = true;
        }
    };
}

#endif