#include "Thread.hpp"
#include <vector>

using namespace MyThread;

int g_tickets = 10000;//没有保护地共享资源

void route(int& tickets)
{
    while(true)
    {
        if(tickets > 0)
        {
            usleep(1000);
            printf("get tickets：%d\n", tickets);
            tickets--;
        }
        else
            break;
    }
}

const int num = 4;

int main()
{
    std::vector<Thread<int>> threads;

    for(int i = 0; i<num; i++)
    {
        std::string name = "thread-" + std::to_string(i+1);
        threads.emplace_back( g_tickets, route,name);
    }

    //启动这批线程
    for(auto& thread : threads)
    {
        thread.Start();
    }

    //等待这批线程
    for(auto& thread : threads)
    {
        thread.Join();
        std::cout << "wait thread succeed, thread is: " << thread.name() << std::endl;
    }
    return 0;
}
