#include <iostream>
#include <vector>
#include <queue>
#include <functional>
using namespace std;

int main()
{
    long long n, k;
    cin >> n >> k;
    priority_queue<long long, vector<long long>, less<long long>> pq;
    long long sum = 0;
    long long x;
    for (long long int i = 0; i < n; i++)
    {
        cin >> x;
        sum += x;
        if (x % 2 == 0)
            pq.push(x);
    }

    for (long long int i = 0; i < k; i++)
    {
        if (pq.size() == 0)
            break;
        long long y = pq.top() / 2;
        pq.pop();
        sum -= y;
        if (y % 2 == 0)
            pq.push(y);
    }
    cout << sum << endl;
    return 0;
}