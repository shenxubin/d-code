#include <iostream>
#include <iterator>
#include <vector>
using namespace std;

void left_to_right(int& sum, int& left, int& right, int x, vector<int>& vect, int& left_min, int& right_min)
{
    while (sum >= x && left < vect.size() && right < vect.size())
    {
        sum -= vect[left];
        left++;
    }
    left--;
}

void right_to_left(int& sum, int& left, int& right, int x, vector<int>& vect, int& left_min, int& right_min)
{
    while (sum < x && left < vect.size() && right < vect.size())
    {
        sum += vect[right + 1];
        right++;
    }
}
int main() {
    int n, x;
    cin >> n >> x;
    vector<int> vect(n + 1);

    for (int i = 1; i < n + 1; i++)
        cin >> vect[i];

    int left_min = 1, right_min = 10000003;
    int retlen = right_min - left_min + 1;
    int sum = 0;
    int right = 1;
    int left = 1;

    while (right < vect.size())
    {
        sum += vect[right];
        while (sum >= x)
        {
            if (right - left + 1 < retlen)
            {
                left_min = left;
                right_min = right;
                retlen = right - left + 1;
            }
            sum -= vect[left++];
        }
        right++;
    }






    //     while (x > sum && j < n + 1)
    //         sum += vect[j++];
    //     j--;
    //     if (x > sum && j - i < right_min - left_min) 
    //     {
    //         left_min = i;
    //         right_min = j;
    //     }

    //     while(j < vect.size())
    //     {
    //         left_to_right(sum, i, j, x,vect, left_min, right_min);
    //         if(j - i < right_min-left_min)
    //         {
    //             left_min = i;
    //             right_min = j;
    //         }
    //         right_to_left(sum,i,j,x,vect, left_min, right_min);
    //         if(j - i < right_min-left_min)
    //         {
    //             left_min = i;
    //             right_min = j;
    //         }
    //     }


    cout << left_min << " " << right_min << endl;
    return 0;
}