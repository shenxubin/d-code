#include<iostream>
#include<algorithm>

using namespace std;
class Knap {
    friend double knapsack(double*, double*, double, int);
private:
    double Bound(int i)
    {
        //计算上界
        double cleft = c - cw; //剩余容量
        double b = cp;
        //以物品单位重量价值递减序装入物品
        while (i <= n && w[i] <= cleft) {
            cleft -= w[i];
            b += p[i];
            i++;
        }
        //装满背包
        if (i <= n) {
            b += p[i] * cleft / w[i];
        }
        return b;
    }
    void Backtrack(int i)
    {
        if (i > n) {
            //到达叶结点
            bestp = cp;
            return;
        }
        if (cw + w[i] <= c) { //进入左子树
            cw += w[i];
            cp += p[i];
            Backtrack(i + 1);
            cw -= w[i];
            cp -= p[i];
        }
        // float u=Bound(i + 1);
        // float bb=bestp;
        //当前的界是否大于背包当前的值
        if (Bound(i + 1) > bestp) { //进入右子树
            Backtrack(i + 1);
        }
    }

    double c; //背包容量
    int n; //物品数
    double* w; //物品重量数组
    double* p; //物品价值数组
    double cw; //当前重量
    double cp; //当前价值
    double bestp; //当前最优价值
};

class Object {
    friend double knapsack(double*, double*, double, int);
public:
    int operator<=(Object a) const {
        return (d >= a.d);
    }
private:
    int ID;
    float d;
};
double knapsack(double p[], double w[], double c, int n) {
    //为 Knap: Backtrack 初始化
    double W = 0;
    double P = 0;
    Object* Q = new Object[n];
    for (int i = 1; i <= n; i++) {
        Q[i - 1].ID = i;
        Q[i - 1].d = 1.0 * p[i] / w[i];
        //cout<<Q[i - 1].d<<endl;
        P += p[i];
        W += w[i];
    }
    if (W <= c) return P; //装入所有物品
    //所有物品的总重量大于背包容量 c,存在最佳装包方案
    //sort(Q,n);对物品以单位重量价值降序排序（不排序也可以，但是为了便于计
    //算上界，可将其按照单位重量价格从大到小排序）
        //对物品以单位重量价值降序排序
        for (int i = 1; i < n; i++)
            for (int j = 1; j <= n - i; j++)
            {
                if (Q[j - 1].d < Q[j].d)
                {
                    Object temp = Q[j - 1];
                    Q[j - 1] = Q[j];
                    Q[j] = temp;
                }
            }
    Knap K;
    K.p = new double[n + 1];
    K.w = new double[n + 1];
    for (int i = 1; i <= n; i++) {
        K.p[i] = p[Q[i - 1].ID];
        K.w[i] = w[Q[i - 1].ID];
    }
    K.cp = 0;
    K.cw = 0;
    K.c = c;
    K.n = n;
    K.bestp = 0;
    //回溯搜索
    K.Backtrack(1);
    delete[] Q;
    delete[] K.w;
    delete[] K.p;
    return K.bestp;
}
void input(int n, double p[], double w[])
{
    cout << "请输入每件商品价值:" << endl;
    for (int i = 1; i <= n; i++)
    {
        cin >> p[i];
    }
    cout << "请输入每件商品重量" << endl;
    for (int i = 1; i <= n; i++)
    {
        cin >> w[i];
    }
}
int main() {
    double c;
    int n;
    cout << "请输入背包容量:";
    cin >> c;
    cout << "请输入物品数量:";
    cin >> n;
    double p[100 + 1] = { 0 };
    double w[100 + 1] = { 0 };
    input(n, p, w);
    double m = knapsack(p, w, c, n);
    cout << "此 0-1 背包问题的最优值为：" << m << endl;
    return 0;
}