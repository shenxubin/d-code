#include <iostream>
using namespace std;


//class A
//{
//private:
//	int _a;
//
//public:
//	A(int a)
//	{
//		this->_a = a;
//	}
//};
//
//
//class Date
//{
//private:
//	int _year;
//	int _month;
//	int _day;
//	int* _aa;
//
//public:
//	Date(int year, int month, int day)
//		:_year(year)  //aa没有显示地调用初始化列表，会去调用它的默认构造函数
//		, _month(month)//剩下的三个成员没有写出来定义，但是它也会定义，只是内置类型给的随机值
//		, _day(day)  //自定义类型会去调用它的默认构造函数
//		,_aa(new int [10])
//	{
//
//		if (_aa == nullptr)
//		{
//			perror("new fail");
//			exit(-1);
//		}
//	}
//
//	~Date()
//	{
//		delete[] _aa;
//	}
//};

class A
{
public:
	A(int a)
		:_a1(a)
		, _a2(_a1)
	{}

	void Print() 
	{
		cout << _a1 << " " << _a2 << endl;
	}
private:
	int _a2;
	int _a1;
};


int main() 
{
	A aa(1);
	aa.Print();
}



