/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
class Solution {
public:
    ListNode* finish = nullptr;

    ListNode* _reverseList(ListNode* head)
    {
        if (head == nullptr || head->next == nullptr)
        {
            finish = head;
            return head;
        }

        ListNode* cur = head->next;
        return _reverseList(cur)->next = head;
    }
    ListNode* reverseList(ListNode* head)
    {
        _reverseList(head);
        if (head)
            head->next = nullptr;
        return finish;
    }
};