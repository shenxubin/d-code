#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <vector>
using namespace std;

class Task
{
private:
    int _x;
    int _y;
public:
    Task(int x, int y)
    :_x(x), _y(y)
    {}

    int Add()
    {
        return _x + _y;
    }
    ~Task()
    {}
};

class ThreadData
{
private:
    Task _t;
    string _name;
public:
    ThreadData(Task t, string name)
    :_t(t), _name(name)
    {}

    Task getTask()
    {
        return _t;
    }

    string getName()
    {
        return _name;
    }
    ~ThreadData()
    {}
};
//结果封装
class Res
{
private:
    int _result;
    string _name;
public:
    Res(int result, string name)
    :_result(result)
    ,_name(name)
    {}

    int getResult()
    {
        return _result;
    }

    string getName()
    {
        return _name;
    }
    ~Res()
    {}
};
//子线程执行的函数
void* handler(void* args)
{
    ThreadData* th = (ThreadData*)args;
    Res* res = new Res(th->getTask().Add(), th->getName());
    delete th;
    return res;
}

vector<pthread_t> vect_tid;
vector<Res*> vect_res;
int main()
{
    for(int i = 0; i<5; i++)
    {
        pthread_t tid;
        Task task(10, 20);
        char buffer[1024];
        snprintf(buffer, sizeof(buffer), "thread-%d", i+1);
        ThreadData* th = new ThreadData(task, buffer);
        pthread_create(&tid, nullptr, handler, th);
        vect_tid.push_back(tid);
    }

    for(auto& e : vect_tid)
    {
        void* ret = nullptr;
        pthread_join(e, &ret);
        vect_res.push_back((Res*)(ret));
    }

    for(auto& e : vect_res)
    {
        cout << e->getName() << " " << "res=" << e->getResult() << endl;
        delete e;
    }
    return 0;
}