#include <iostream>
#include <pthread.h>
#include <unistd.h>

void* handler(void* args)
{
    const std::string str = static_cast<const char*> (args);
    int cnt = 5;
    while(cnt--)
    {
        std::cout << str << std::endl;
        sleep(1);
    }

    return nullptr;
}

int main()
{
    pthread_t tid;
    pthread_create(&tid, nullptr, handler, (void*)"new Thread");
    //detach可以放在main函数中也可以放在handler函数中
    pthread_detach(tid);
    while(true)
    {}
    return 0;
}