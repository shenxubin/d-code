//#include <iostream>
//#include <assert.h>
//
//using namespace std;
//
//template <class T>
//struct Node
//{
//	T data;
//	struct Node* next;
//	Node()
//	{
//		this->data = 0;
//		this->next = nullptr;
//	}
//	Node(T data)
//	{
//		this->data = data;
//		this->next = nullptr;
//	}
//};
//
//template <class T>
//class SList
//{
//private:
//	Node<T>* head;
//	Node<T>* tail;
//public:
//	SList()
//	{
//		this->head = new Node<T>();
//		this->tail = head;
//	}
//
//	~SList()
//	{
//		Node<T>* p = head;
//		Node<T>* q = head;
//		while (p != tail)
//		{
//			q = p->next;
//			delete p;
//			p = q;
//		}
//		delete tail;
//	}
//
//	
//	 动态申请一个节点
//	Node<T>* BuySListNode(T x);
//
//	 单链表打印
//	void SListPrint();
//
//	 单链表尾插
//	void SListPushBack(T x);
//
//	 单链表的头插
//	void SListPushFront(T x);
//
//	 单链表的尾删
//	void SListPopBack();
//
//	 单链表头删
//	void SListPopFront();
//
//	 单链表查找
//	Node<T>* SListFind( T x);
//
//	 单链表在pos位置之后插入x
//	void SListInsertAfter(Node<T>* pos, T x);
//
//	 单链表删除pos位置之后的值
//	void SListEraseAfter(Node<T>* pos);
//
//};
//
//template <class T>
//Node<T>* SList<T>::BuySListNode(T x)
//{
//	Node<T>* tmp = new Node<T>;
//	tmp->data = x;
//	tmp->next = nullptr;
//	return tmp;
//}
//
//template <class T>
//void SList<T>::SListPrint()
//{
//	assert(head->next);//保证头节点后还有结点才打印，不然报错
//	Node<T>* cur = head->next;
//	while (cur != head)
//	{
//		cout << cur->data << "->";
//		cur = cur->next;
//	}
//	cout << "NULL" << endl;
//}
//
//template <class T>
//void SList<T>::SListPushBack(T x)
//{
//	Node<T>* newnode = BuySListNode(x);
//	tail->next = newnode;
//	tail = newnode;
//	tail->next = head;//尾节点的next指向头节点
//}
//
//template <class T>
//void SList<T>::SListPushFront(T x)
//{
//	Node<T>* newnode = BuySListNode(x);
//	if (head == tail)
//	{
//		head->next = newnode;
//		tail = newnode;
//		tail->next = head;
//	}
//	else
//	{
//		newnode->next = head->next;
//		head->next = newnode;
//	}
//}
//
//template <class T>
//void SList<T>::SListPopBack()
//{
//	assert(head->next);
//	Node<T>* cur = head;
//	while (cur->next != tail)
//	{
//		cur = cur->next;
//	}
//	delete tail;
//	tail = cur;
//	tail->next = head;
//
//}
//
//template <class T>
//void SList<T>::SListPopFront()
//{
//	assert(head->next);
//	Node<T>* cur = head->next;
//
//	if (head->next == tail)
//	{
//		delete tail;
//		tail = head;
//	}
//	else
//	{
//		head->next = cur->next;
//		delete cur;
//	}
//}
//
//template <class T>
//Node<T>* SList<T>::SListFind(T x)
//{
//	assert(head->next);
//	Node<T>* cur = head->next;
//	while (cur != head)
//	{
//		if (cur->data == x)
//		{
//			return cur;
//		}
//		cur = cur->next;
//	}
//	return nullptr;
//}
//
//template <class T>
//void SList<T>::SListInsertAfter(Node<T>* pos, T x)
//{
//	assert(pos);
//	if (pos->next == head)
//	{
//		SListPushBack(x);
//	}
//	else if(pos == head)
//	{
//		SListPushFront(x);
//	}
//	else
//	{
//		Node<T>* newnode = BuySListNode(x);
//		newnode->next = pos->next;
//		pos->next = newnode;
//	}
//}
//template <class T>
//void SList<T>::SListEraseAfter(Node<T>* pos)
//{
//	assert(pos);
//	尾节点后的头节点不能删
//	if (pos->next == head)
//	{
//		exit(-1);
//	}
//	else if (pos == head)
//	{
//		SListPopFront();
//	}
//	else
//	{
//		Node<T>* cur = pos->next;
//		pos->next = pos->next->next;
//		delete cur;
//	}
//}
//
//
//int main()
//{
//
//	SList<int> SL1;
//	SL1.SListPushBack(1);
//	SL1.SListPushBack(2);
//	SL1.SListPushBack(3);
//	SL1.SListPushBack(4);
//	SL1.SListPushBack(5);
//	SL1.SListPrint();
//
//	SL1.SListPushFront(10);
//	SL1.SListPrint();
//
//	SL1.SListPopFront();
//	SL1.SListPrint();
//
//	SL1.SListPopBack();
//	SL1.SListPrint();
//
//	Node<int>* cur = SL1.SListFind(2);
//	SL1.SListInsertAfter(cur, 20);
//	SL1.SListPrint();
//
//	SL1.SListEraseAfter(cur);
//	SL1.SListPrint();
//
//	return 0;
//}