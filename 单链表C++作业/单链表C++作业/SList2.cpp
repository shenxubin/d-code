#include <iostream>
#include <assert.h>

using namespace std;

template <class T>
struct Node
{
	T data;
	struct Node* next;
	Node()
	{
		this->data = 0;
		this->next = nullptr;
	}
	Node(T data)
	{
		this->data = data;
		this->next = nullptr;
	}
};

template <class T>
class SList
{
private:
	Node<T>* head;
public:
	SList()
	{
		this->head = new Node<T>();
	}

	~SList()
	{
		Node<T>* p = head;
		Node<T>* q = head;
		while (p->next)
		{
			q = p->next;
			delete p;
			p = q;
		}
		delete p;
	}

	Node<T>* getHead()
	{
		return this->head;
	}

	// 动态申请一个节点
	Node<T>* BuySListNode(T x);

	// 单链表打印
	void SListPrint(Node<T>* plist);

	// 单链表尾插
	void SListPushBack(Node<T>* plist, T x);

	// 单链表的头插
	void SListPushFront(Node<T>* plist, T x);

	// 单链表的尾删
	void SListPopBack(Node<T>* plist);

	// 单链表头删
	void SListPopFront(Node<T>* plist);

	// 单链表查找
	Node<T>* SListFind(Node<T>* plist, T x);

	// 单链表在pos位置之后插入x
	void SListInsertAfter(Node<T>* pos, T x);

	// 单链表删除pos位置之后的值
	void SListEraseAfter(Node<T>* pos);

};

template <class T>
Node<T>* SList<T>::BuySListNode(T x)
{
	Node<T>* tmp = new Node<T>;
	tmp->data = x;
	tmp->next = nullptr;
	return tmp;
}

template <class T>
void SList<T>::SListPrint(Node<T>* plist)
{
	Node<T>* cur = plist->next;
	while (cur)
	{
		cout << cur->data << "->";
		cur = cur->next;
	}
	cout << "NULL" << endl;
}

template <class T>
void SList<T>::SListPushBack(Node<T>* plist, T x)
{
	Node<T>* cur = plist;

	while (cur->next)
	{
		cur = cur->next;
	}
	Node<T>* newnode = BuySListNode(x);
	cur->next = newnode;
}
template <class T>
void SList<T>::SListPushFront(Node<T>* plist, T x)
{
	Node<T>* newnode = BuySListNode(x);
	newnode->next = (plist)->next;
	plist->next = newnode;
}

template <class T>
void SList<T>::SListPopBack(Node<T>* plist)
{
	assert(plist);
	Node<T>* tail = plist;
	while (tail->next->next != NULL)
	{
		tail = tail->next;
	}
	delete tail->next;
	tail->next = nullptr;
}

template <class T>
void SList<T>::SListPopFront(Node<T>* plist)
{
	assert(plist && plist->next);
	Node<T>* cur = (plist)->next;
	Node<T>* cur2 = plist->next->next;
	delete cur;
	plist->next = cur2;
}

template <class T>
Node<T>* SList<T>::SListFind(Node<T>* plist, T x)
{
	assert(plist);
	Node<T>* cur = plist->next;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return nullptr;
}

template <class T>
void SList<T>::SListInsertAfter(Node<T>* pos, T x)
{
	assert(pos);
	Node<T>* newnode = BuySListNode(x);
	newnode->next = pos->next;
	pos->next = newnode;
}
template <class T>
void SList<T>::SListEraseAfter(Node<T>* pos)
{
	assert(pos->next && pos);
	Node<T>* cur = pos->next;
	pos->next = pos->next->next;
	delete cur;;
}


int main()
{
	//SListNode<int>* head = new SListNode<int>;

	SList<int> SL1;
	SL1.SListPushBack(SL1.getHead(), 1);
	SL1.SListPushBack(SL1.getHead(), 2);
	SL1.SListPushBack(SL1.getHead(), 3);
	SL1.SListPushBack(SL1.getHead(), 4);
	SL1.SListPushBack(SL1.getHead(), 5);
	SL1.SListPrint(SL1.getHead());

	SL1.SListPushFront(SL1.getHead(), 10);
	SL1.SListPrint(SL1.getHead());

	SL1.SListPopFront(SL1.getHead());
	SL1.SListPrint(SL1.getHead());

	SL1.SListPopBack(SL1.getHead());
	SL1.SListPrint(SL1.getHead());

	Node<int>* cur = SL1.SListFind(SL1.getHead(), 2);
	SL1.SListInsertAfter(cur, 20);
	SL1.SListPrint(SL1.getHead());

	SL1.SListEraseAfter(cur);
	SL1.SListPrint(SL1.getHead());

	return 0;
}