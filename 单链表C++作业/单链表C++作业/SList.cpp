////无头单链表
//
//#include <iostream>
//#include <assert.h>
//
//using namespace std;
//
//template <class T>
//
////先定义链表中的节点
//struct SListNode
//{
//	T data;
//	SListNode* next;
//
//	SListNode(T x)
//	{
//		this->data = x;
//		this->next = nullptr;
//	}
//};
//
//template <class T>
//class SList
//{
//private:
//	//链表初始化后链表中就有一个节点
//	SListNode<T>* head;
//	
//public:
//	SList(T x)
//	{
//		this->head = new SListNode<T>(x);
//	}
//
//	~SList()
//	{
//		SListNode<T>* cur = head;
//		while (cur)
//		{
//			SListNode<T>* next = cur->next;
//			delete cur;
//			cur = next;
//		}
//	}
//
//
//	// 动态申请一个节点
//	SListNode<T>* BuySListNode(T x);
//
//	// 单链表打印
//	void SListPrint();
//
//	// 单链表尾插
//	void SListPushBack(T x);
//
//	// 单链表的头插
//	void SListPushFront(T x);
//
//	// 单链表的尾删
//	void SListPopBack();
//
//	// 单链表头删
//	void SListPopFront();
//
//	// 单链表查找
//	SListNode<T>* SListFind(T x);
//
//	// 单链表在pos位置之后插入x
//	void SListInsertAfter(SListNode<T>* pos, T x);
//
//	// 单链表删除pos位置之后的值
//	void SListEraseAfter(SListNode<T>* pos);
//
//};
//
//
//template <class T>
//SListNode<T>* SList<T>:: BuySListNode(T x)
//{
//	SListNode<T>* tmp = new SListNode<T>(x);
//	tmp->next = nullptr;
//	return tmp;
//}
//
//template <class T>
//void SList<T>::SListPrint()
//{
//	SListNode<T>* cur =head;
//	while (cur)
//	{
//		cout << cur->data << "->";
//		cur = cur->next;
//	}
//	cout << "NULL" << endl;
//}
//
//template <class T>
//void SList<T>::SListPushBack(T x)
//{
//	SListNode<T>* cur = head;
//
//	while (cur->next)
//	{
//		cur = cur->next;
//	}
//	SListNode<T>* newnode = BuySListNode(x);
//	cur->next = newnode;//连接
//}
//template <class T>
//void SList<T>::SListPushFront(T x)
//{
//	SListNode<T>* newnode = BuySListNode(x);
//	newnode->next = head;
//	head = newnode;
//}
//
//template <class T>
//void SList<T>::SListPopBack()
//{
//	assert(head);//头结点为空就不能继续删除了
//	SListNode<T>* tail = head;
//	//链表中只有一个节点就只能删除头结点
//	if (tail->next == nullptr)
//	{
//		delete head;
//	}
//	else
//	{
//		while (tail->next->next != NULL)
//		{
//			tail = tail->next;
//		}
//		delete tail->next;
//		tail->next = nullptr;
//	}
//
//}
//
//template <class T>
//void SList<T>::SListPopFront()
//{
//	assert(head);
//	SListNode<T>* cur = head->next;
//	delete head;
//	head = cur;
//}
//
//template <class T>
//SListNode<T>* SList<T>::SListFind(T x)
//{
//	assert(head);
//	SListNode<T>* cur = head;
//	while (cur)
//	{
//		if (cur->data == x)
//		{
//			return cur;
//		}
//		cur = cur->next;
//	}
//	return nullptr;
//}
//
//template <class T>
//void SList<T>::SListInsertAfter(SListNode<T>* pos, T x)
//{
//	assert(pos);
//	SListNode<T>* newnode = BuySListNode(x);
//	newnode->next = pos->next;
//	pos->next = newnode;
//}
//
//
//template <class T>
//void SList<T>::SListEraseAfter(SListNode<T>* pos)
//{
//	assert(pos->next && pos);
//	SListNode<T>* cur = pos->next;
//	pos->next = pos->next->next;
//	delete cur;
//}
//
//
//int main()
//{
//
//	SList<int> cur(1);
//
//	cur.SListPushBack(2);
//	cur.SListPushBack(3);
//	cur.SListPushBack(4);
//	cur.SListPushBack(5);
//	cur.SListPushBack(6);
//	cur.SListPrint();
//
//	cur.SListPopFront();
//	cur.SListPrint();
//
//	cur.SListPopBack();
//	cur.SListPrint();
//
//	SListNode<int>* p1 = cur.SListFind(2);
//	cur.SListInsertAfter(p1, 20);
//	cur.SListPrint();
//
//	cur.SListEraseAfter(p1);
//	cur.SListPrint();
//
//
//	
//	return 0;
//}