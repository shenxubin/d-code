#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdlib.h>
#include <assert.h>
using namespace std;

int main()
{
    pid_t id = fork();
    int pipeid[2];
    int n = pipe(pipeid);
    assert(n == 0);
    if(id == 0)
    {
        close(pipeid[1]);
        dup2(pipeid[0], STDIN_FILENO);

        execlp("grep", "grep", "7-5", NULL);
        exit(0);
    }
    else
    {
        close(pipeid[0]);
        dup2(pipeid[1], STDOUT_FILENO);

        execlp("ls", "ls", "-l", NULL);
    }
    
    if(id != 0)
        wait(NULL);

    return 0;
}
