#include <iostream>
#include <signal.h>
#include <unistd.h>
using namespace std;

int main()
{
    sigset_t set, oset;
    sigemptyset(&set);
    sigemptyset(&oset);

    sigaddset(&set, 2);

    //设置block位图
    int n = sigprocmask(SIG_SETMASK, &set, &oset);
    if(n == -1)
    {
        cout << "block 位图设置错误！" << endl;
        return -1;
    }
    
    int cnt = 10;

    while(cnt--)
    {
        cout << "进程正在运行！" << endl;
        sleep(1);
    }
    sigprocmask(SIG_UNBLOCK, &set, nullptr);

    return 0;
}
