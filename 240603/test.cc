#include <iostream>
#include <vector>
#include <stack>
using namespace std;

stack<pair<size_t, size_t>> st;
size_t n;
size_t value_sum = 0;

enum direction
{
    top,
    left,
};

//dp表中的元素
struct coordinate
{
    size_t value;//到该位置的最大样本总值
    bool walked = false;//该位置是否走过，默认为没走过
    int prev;//前一个位置在当前位置的哪一个方向
};

//输入个样本的价值
void inputValue(vector<vector<size_t>> &value)
{
    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            cin >> value[i][j];
        }
    }
}

//第一次填表
void fillingFirstDP(vector<vector<coordinate>> &dp, vector<vector<size_t>> &value)
{
    for (size_t i = 1; i < n + 1; i++)
    {
        for (size_t j = 1; j < n + 1; j++)
        {
            if (dp[i - 1][j].value > dp[i][j - 1].value)
            {
                dp[i][j].value = dp[i - 1][j].value + value[i - 1][j - 1];
                dp[i][j].prev = direction::top;
            }
            else
            {
                dp[i][j].value = dp[i][j - 1].value + value[i - 1][j - 1];
                dp[i][j].prev = direction::left;
            }
        }
    }
}

//第二次填表
void fillingSecondDP(vector<vector<coordinate>> &dp, vector<vector<size_t>> &value)
{
    for (size_t i = 1; i < n + 1; i++)
    {
        for (size_t j = 1; j < n + 1; j++)
        {
            if (dp[i][j].walked)
                continue;
            else if (dp[i - 1][j].walked)
            {
                dp[i][j].value = dp[i][j - 1].value + value[i - 1][j - 1];
                dp[i][j].prev = direction::left;
            }

            else if (dp[i][j - 1].walked)
            {
                dp[i][j].value = dp[i - 1][j].value + value[i - 1][j - 1];
                dp[i][j].prev = direction::top;
            }
            else
            {
                if (dp[i - 1][j].value > dp[i][j - 1].value)
                {
                    dp[i][j].value = dp[i - 1][j].value + value[i - 1][j - 1];
                    dp[i][j].prev = direction::top;
                }
                else
                {
                    dp[i][j].value = dp[i][j - 1].value + value[i - 1][j - 1];
                    dp[i][j].prev = direction::left;
                }
            }
        }
    }
}

//清空dp表
void clearDP(vector<vector<coordinate>> &dp)
{
    for (size_t i = 1; i < n + 1; i++)
    {
        for (size_t j = 1; j < n + 1; j++)
        {
            dp[i][j].value = 0;
        }
    }
}

//找路径
void findPath(vector<vector<coordinate>> &dp)
{
    size_t x = n;
    size_t y = n;

    st.push(make_pair(x - 1, y - 1));

    while ((x > 1 && y >= 1) || (x >= 1 && y > 1))
    {
        if (dp[x][y].prev == direction::top)
        {
            st.push(make_pair(x - 2, y - 1));
            dp[x - 1][y].walked = true;
            x -= 1;
        }
        else
        {
            st.push(make_pair(x - 1, y - 2));
            dp[x][y - 1].walked = true;
            y -= 1;
        }
    }
}

void printPath(stack<pair<size_t, size_t>> &st)
{
    while (!st.empty())
    {
        cout << st.top().first << " " << st.top().second << endl;
        st.pop();
    }
}

int main()
{
    cin >> n;

    vector<vector<size_t>> value(n, vector<size_t>(n));

    // 输入
    inputValue(value);

    vector<vector<coordinate>> dp(n + 1, vector<coordinate>(n + 1));

    // 第一次填dp表
    fillingFirstDP(dp, value);
    // 第一次找路径，填入st栈中
    findPath(dp);

    cout << "第一次经过的路线为：" << endl;
    printPath(st);
    cout << "第一次的样本总价值为：" << endl;
    cout << dp[n][n].value << endl;
    value_sum += dp[n][n].value;

    // 第二次开始走，将起点改成未走过
    dp[1][1].walked = false;

    // 清空dp表的value
    clearDP(dp);

    // 第二次填dp表
    fillingSecondDP(dp, value);
    // 第二次找路径，填入st栈中
    findPath(dp);

    cout << "第二次经过的路线为：" << endl;
    printPath(st);
    cout << "第二次的样本总价值为：" << endl;
    cout << dp[n][n].value - dp[1][1].value - value[n - 1][n - 1] << endl;
    value_sum += dp[n][n].value - dp[1][1].value - value[n - 1][n - 1];

    cout << "Rob的2条行走路径取得的样本总价值最大为：" << endl;
    cout << value_sum << endl;
    return 0;
}