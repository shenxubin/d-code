#include <iostream>
#include <vector>
using namespace std;

int main()
{
    int n, x;
    cin >> n >> x;
    vector<int> vect(n + 1);

    for (int i = 1; i < n + 1; i++)
        cin >> vect[i];

    int left_min = 1, right_min = 10000003;
    int sum = 0; int j;
    for (int i = 1; i < n + 1; i++)
    {
        j = i;
        sum = 0;
        while (j < n + 1)
        {
            sum += vect[j];
            if (sum >= x)
                break;
            j++;
        }
        if (j - i < right_min - left_min)
        {
            left_min = i;
            right_min = j;
        }
    }

    cout << left_min << " " << right_min << endl;
    return 0;
}