#include<iostream>
using namespace std;

class Node
{
	friend class LinkedList;

	int data;
	Node* next;

public:
	Node(int data = 0, Node* next = NULL)
	{
		this->data = data;
		this->next = next;
	}

	~Node()
	{
		this->data = 0;
		this->next = NULL;
	}
};

class LinkedList :public Node
{
	Node* head;
public:
	LinkedList()
	{
		this->head = new Node();
	}

	LinkedList(int* a, int length)
	{
		this->head = new Node();
		Node* q = this->head;
		for (int i = 0; i < length; i++)
		{
			Node* p = new Node(a[i]);
			q->next = p;
			q = q->next;
		}
	}

	bool search(int value)
	{
		Node* q = this->head->next;
		while (q != NULL)
		{
			if (q->data == value)
			{
				return true;
			}
			q = q->next;
		}
		return false;
	}

	bool Change(int x, int y)
	{
		Node* q = this->head->next;;
		while (q != NULL)
		{
			if (q->data == x)
			{
				q->data = y;
				return true;
			}
			q = q->next;
		}
		cout << "未找到该数" << endl;
		return false;
	}

	void Print()
	{
		if (this->head->next == NULL)
		{
			cout << "The list is empty" << endl;
		}
		Node* q = this->head->next;
		while (q != NULL)
		{
			cout << q->data << " ";
			q = q->next;
		}
		cout << endl;
	}

	void Push_back(int value)
	{
		Node* q = this->head;
		while (q->next != NULL)
		{
			q = q->next;
		}
		Node* p = new Node(value);
		q->next = p;
	}

	bool Pop_back()
	{
		if (this->head->next == NULL)
		{
			return false;
		}
		Node* q = this->head;
		while (q->next->next != NULL)
		{
			q = q->next;
		}
		Node* p = q->next;
		q->next = NULL;
		delete p;
		return true;
	}

	void Push_head(int value)
	{
		Node* p = new Node(value);
		p->next = this->head->next;
		this->head->next = p;
	}

	bool Pop_head()
	{
		if (this->head->next == NULL)
		{
			return false;
		}
		Node* p = this->head->next;
		this->head->next = p->next;
		delete p;
		return true;
	}
};

int main()
{
	int* a = new int[5];
	for (int i = 0; i < 5; i++)
	{
		a[i] = i + 1;
	}
	LinkedList list(a, 5);
	if (list.search(3))
	{
		cout << "找到" << endl;
	}
	else
	{
		cout << "未找到" << endl;
	}
	if (list.search(9))
	{
		cout << "找到" << endl;
	}
	else
	{
		cout << "未找到" << endl;
	}
	list.Print();
	list.Push_back(6);
	list.Print();
	list.Push_head(0);
	list.Print();
	list.Pop_head();
	list.Print();
	list.Pop_back();
	list.Print();
	list.Change(2, 9);
	list.Print();
	list.Change(10, 20);
	return 0;
}
