#include <iostream>
#include <signal.h>
#include <unistd.h>
using namespace std;

void print(sigset_t& pending)
{
    for(int i = 31; i>= 1; i--)
    {
        if(sigismember(&pending, i))
            cout << "1";
        else
            cout << "0";
    }
    cout << endl;
}

//自定义2号信号的处理方法，让进程不断打印pending位图
void handler(int sig)
{
    sigset_t pending;
    sigemptyset(&pending);

    while(true)
    {
        sigpending(&pending);
        print(pending);
        sleep(1);
    }
}

int main()
{
    struct sigaction act,oact;
    act.sa_handler = handler;
    act.sa_flags = 0;

    cout << getpid() << endl;

    //清空要添加的信号集
    sigemptyset(&act.sa_mask);
    //让进程收到2号信号的同时阻塞3,4,5号信号
    sigaddset(&act.sa_mask, 3);
    sigaddset(&act.sa_mask, 4);
    sigaddset(&act.sa_mask, 5);

    sigaction(2, &act, &oact);

    while(true)
    {}
    return 0;
}