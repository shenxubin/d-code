
#include <iostream>
#include <assert.h>
using namespace std;

class SeqList
{
public:
	SeqList(int* data, int n); //构造函数
	~SeqList(); //析构函数
	bool CheckSeqList();//检查
	bool Insert(int i, int x); //插入
	bool Remove(int i);//删除
	void SeqListPrint();//打印
	int SeqListFind(int x); //搜索
	int GetData(int i); //定位
	void Sort(); //排序
	void SetData(int i, int x); //赋值
	int Size(); //求表最大空间
	int Length(); //求表长度
	bool IsEmpty(); //判表空
	bool IsFull();//判表满
private:
	int* data;
	int maxSize;
	int n;
};

SeqList::SeqList(int* data, int n)
{
	this->data = new int[n];
	this->maxSize = n;
	this->n = n;
	for (int i = 0; i < this->n; i++)
	{
		this->data[i] = data[i];
	}
}

SeqList::~SeqList()
{
	delete[]this->data;
	this->maxSize = 0;
	this->n = 0;
}

bool SeqList::CheckSeqList()
{
	if (this->n == this->maxSize)
	{
		int newCapacity = this->maxSize * 2;
		int* tmp = new int[newCapacity];
		if (tmp == NULL)
		{
			perror("tmp new");
			exit(-1);
		}
		for (int i = 0; i < this->n; i++)
		{
			tmp[i] = this->data[i];
		}
		free(this->data);
		this->data = tmp;
		this->maxSize = newCapacity;
	}
}


bool SeqList::Insert(int i, int x)
{
	assert(i >= 0 && i <= n);
	CheckSeqList();
	for (int j = this->n; j > i; j--)  
	{
		this->data[j] = this->data[j - 1];
	}
	this->data[i] = x;
	this->n++;
	return true;
}

bool SeqList::Remove(int i)
{
	assert(i >= 0 && i < this->n);
	for (int j = i; j < this->n - 1; j++)
	{
		this->data[j] = this->data[j + 1];
	}
	this->n--;
	return true;
}


void SeqList::SeqListPrint()
{
	assert(this->data);
	int i = 0;
	for (i = 0; i < this->n; i++)
	{
		printf("%d ", this->data[i]);
	}
	printf("\n");
}

void SeqList::Sort()
{
	int tmp = 0;
	for (int i = 0; i < this->n - 1; i++)
	{
		int count = 0;
		for (int j = 0; j < this->n - 1-i; j++)
		{
			if (this->data[j] > this->data[j + 1])
			{
				tmp = this->data[j];
				this->data[j] = this->data[j + 1];
				this->data[j + 1] = tmp;
			}
			
		}
		if (count == 0)
			break;
	}
}


int SeqList::SeqListFind(int x)
{
	assert(this->data);
	int i = 0;
	for (i = 0; i < this->n; i++)
	{
		if (this->data[i] == x)
		{
			return i;
		}
	}
	return -1;
}

int SeqList::GetData(int i)
{
	assert(i >= 0 && i < this->n);
	return this->data[i];
}

void SeqList::SetData(int i, int x)
{
	assert(i >= 0 && i < this->n);
	this->data[i] = x;
}

int SeqList::Size()
{
	return this->maxSize;
}

int SeqList::Length()
{
	return this->n;
}

bool SeqList::IsEmpty()
{
	return this->n == 0;
}

bool SeqList::IsFull()
{
	return this->maxSize == this->n;
}


int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8 };
	SeqList sq(arr, 8);
	sq.SeqListPrint();

	sq.Insert(6, 10);
	sq.SeqListPrint();

	sq.Sort();
	sq.SeqListPrint();

	sq.SetData(0, 11);
	sq.SeqListPrint();

	return 0;
}











