#include <iostream>
using namespace std;

class Person
{
public:
    virtual void BuyTicket()
    {
        cout << "买票-全价" << endl;
    }
};

class Student : public Person
{
public:
    //特例1：子类虚函数不加virtual，依旧构成重写（实际最好加上）
    //特例2：重写的协变：返回值可以不同，要求必须是父子关系的指针或者引用
    virtual void BuyTicket()
    {
        cout << "买票-半价" << endl;
    }
    
};

class Soldier : public Person
{
public:
    //虚函数重写（覆盖）->虚函数 + 函数名、返回值、参数都要求相同
    //不符合重写就是隐藏关系
    virtual void BuyTicket()
    {
        cout << "买票-优先" << endl;
    }
};

//多态的两个条件
//1.虚函数重写
//2.父类指针或者引用去调用虚函数

void Func(Person& p)
{
    p.BuyTicket();
}

int main()
{
    Person ps;
    Student st;
    Soldier sd;
    Func(ps);
    Func(st);
    Func(sd);
    return 0;
}