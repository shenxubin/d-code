//先将根结点放入一对圆括号中， 然后把它的子树按由左而右的顺序放入括号中，
//而对子树也采用同样方法处理：同层子树与它的根结点用圆括号括起来，
//同层子树之间用逗号隔开，最后用闭括号括起来。例如下图可写成如下形式
//
//(a(b, c, d, e))
//
//现在给定一个多叉树的括号表示法，要求你创建多叉树，并按层序序列输出。

#include <iostream>
#include <vector>
#include <string.h>
using namespace std;

struct node
{
    char ch;
    int num;
};

vector<node*> st1;

node* buyNode(char ch)
{
    node* tmp = new node;
    tmp->ch = ch;
    tmp->num = 0;
    return tmp;
}

int main()
{
    char arr[1000];
    cin >> arr;
    int len = strlen(arr);
    int charnum = 0, charlayers = 0;
    for (int i = 0; i < len; i++)
    {
        if (arr[i] == '(')
        {
            charnum++;
        }
        else if (arr[i] == ')')
        {
            charnum--;
        }
        if (arr[i] != '(' && arr[i] != ')' && arr[i] != ',')
        {
            if (charlayers < charnum)
            {
                charlayers = charnum;
            }
            node* tmp = buyNode(arr[i]);
            tmp->num = charnum;
            st1.push_back(tmp);
        }
    }
    for (int i = 1; i <= charlayers; i++)
    {
        for (int j = 0; j < st1.size(); j++)
        {
            if (st1[j]->num == i)
            {
                cout << st1[j]->ch;
            }
        }
    }
    cout << endl;

    for (int i = 0; i < st1.size(); i++)
    {
        delete st1[i];
    }
    return 0;
}
// 64 位输出请用 printf("%lld")