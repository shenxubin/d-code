//对输入的英文大写字母进行统计概率，
//然后构建哈夫曼树，输出是按照概率降序输出哈夫曼编码

#include<iostream>
#include <vector>
using namespace std;

struct node
{
	char _ch;
	int _num;
};

vector<node*> v1;

node* buyNode(char ch, int num)
{
	node* tmp = new node;
	tmp->_ch = ch;
	tmp->_num = num;
	return tmp;
}

template<class Type>
class hfTree {
private:
	struct Node {
		Type data;//节点值
		int weight;//节点的权值
		int parent, left, right;//父节点下标，左右孩子下标
	};
	Node* elem;
	int length;//数组长度
public:
	struct hfCode {
		Type data;
		string code;//哈夫曼编码
	};
	hfTree(const Type* x, const int* w, int size);
	void getCode(hfCode result[]);
	~hfTree() {
		delete[]elem;
	}
};

//构造函数
template<class Type>
hfTree<Type>::hfTree(const Type* v, const int* w, int size) {
	const int MAX_INT = 32767;
	int min1, min2;//最小树、次小数的权值
	int x, y;//最小树、次最小树的下标

	//置初值
	length = 2 * size;//数组长度
	elem = new Node[length];
	for (int i = size; i < length; ++i) {
		elem[i].weight = w[i - size];
		elem[i].data = v[i - size];
		elem[i].parent = elem[i].left = elem[i].right = 0;
	}

	//构造新的二叉树
	for (int i = size - 1; i > 0; --i) {
		min1 = min2 = MAX_INT;
		x = y = 0;
		for (int j = i + 1; j < length; ++j)
		{
			if (elem[j].parent == 0)
				if (elem[j].weight < min1) {//如果有元素的权值比当前最小值小，则更新次小值和最小值
					min2 = min1;
					min1 = elem[j].weight;
					y = x;
					x = j;
				}
				else if (elem[j].weight < min2) {//元素的权值大于当前最小值但小于次小值，更新次小值
					min2 = elem[j].weight;
					y = j;
				}
		}
		elem[i].weight = min1 + min2;
		//elem[i].left = y;//次小值（作为左子）的下标
		//elem[i].right = x;//最小值的下标
		elem[i].left = x;//次小值（作为左子）的下标
		elem[i].right = y;//最小值的下标
		elem[i].parent = 0;
		elem[x].parent = i;
		elem[y].parent = i;
	}
}

//求哈夫曼编码
template<class Type>
void hfTree<Type>::getCode(hfCode result[]) {
	int size = length / 2;
	int p, s;//s是正在处理的节点，p是s的父节点下标
	for (int i = size; i < length; ++i) {
		result[i - size].data = elem[i].data;
		result[i - size].code = "";
		p = elem[i].parent;
		s = i;
		while (p) {//即当p！=0时
			if (elem[p].left == s) {//如果s所对应的数为p的左子，则加0
				result[i - size].code = '0' + result[i - size].code;
			}
			else result[i - size].code = '1' + result[i - size].code;
			s = p;
			p = elem[p].parent;
		}
	}
}

int main()
{
    int n;
    cin >> n;
    int charArrSum[26] = { 0 };
    char tmp;
    for (int i = 0; i < n; i++)
    {
        cin >> tmp;
        charArrSum[tmp - 65]++;
    }
    int effectiveNum = 0;
    for (int i = 0; i < 26; i++)
    {
        if (charArrSum[i] != 0)
        {
            node* tmp = buyNode(65 + i, charArrSum[i]);
            v1.push_back(tmp);
            effectiveNum++;
        }
    }
    for (int i = 0; i < effectiveNum - 1; i++)
    {
        for (int j = 0; j < effectiveNum - 1 - i; j++)
        {
            if (v1[j]->_num < v1[j + 1]->_num)
            {
                swap(v1[j], v1[j + 1]);
            }
        }
    }
    char ch[100];
    for (int i = 0; i < effectiveNum; i++)
    {
        ch[i] = v1[i]->_ch;
    }
    int w[100];
    for (int i = 0; i < effectiveNum; i++)
    {
        w[i] = v1[i]->_num;
    }

    /*char ch[] = { "aeistdn" };
int w[] = { 10,15,12,3,4,13,1 };*/
    hfTree<char> tree(ch, w, effectiveNum);
    hfTree<char>::hfCode result[effectiveNum];

    tree.getCode(result);
    for (int i = 0; i < effectiveNum; ++i)
        cout << result[i].data << " " << v1[i]->_num << " "
        << result[i].code << endl;

    return 0;
}
