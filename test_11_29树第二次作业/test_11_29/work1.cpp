//按顺序方式存储的一棵完全二叉树的结点记录， 结点个数为n。
//根据所输入的顺序结构的结点记录建立二叉树，输出树的先序， 
//中序和后序遍历结果。
//
//注：数字“0”表示不存在此结点， 没有孩子结点。

#include <iostream>
using namespace std;

struct BTNode
{
    int val;
    BTNode* left;
    BTNode* right;
};


BTNode* buyBTNode(int x)
{
    BTNode* node = new BTNode;
    node->left = nullptr;
    node->right = nullptr;
    node->val = x;
    return node;
}

void CreatBinaryTree(BTNode** tmp, int n)
{
    int child = n - 1;
    int parent = (child - 1) / 2;
    while (child > 0)
    {
        if (child % 2 == 0)
        {
            tmp[parent]->right = tmp[child];
        }
        else
        {
            tmp[parent]->left = tmp[child];
        }
        child--;
        parent = (child - 1) / 2;
    }
}

void BinaryTreePrevOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    if (root->val != 0)
        cout << root->val << " ";
    BinaryTreePrevOrder(root->left);
    BinaryTreePrevOrder(root->right);
}


void BinaryTreeInOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreeInOrder(root->left);
    if (root->val != 0)
        cout << root->val << " ";
    BinaryTreeInOrder(root->right);
}


void BinaryTreePostOrder(BTNode* root)
{
    if (root == nullptr)
        return;
    BinaryTreePostOrder(root->left);
    BinaryTreePostOrder(root->right);
    if (root->val != 0)
        cout << root->val << " ";
}

void BinaryTreeDestroy(BTNode* root)
{
    if (root == nullptr)
    {
        return;
    }
    BinaryTreeDestroy(root->left);
    BinaryTreeDestroy(root->right);
    delete root;
}



int main()
{
    int n;
    cin >> n;
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        cin >> arr[i];
    }

    BTNode* tmp[n];
    for (int i = 0; i < n; i++)
    {
        tmp[i] = buyBTNode(arr[i]);
    }
    CreatBinaryTree(tmp, n);
    // int child = n-1;
    // int parent = (child-1)/2;
    // while(child > 0)
    // {
    //     if(child % 2 == 0)
    //     {
    //         tmp[parent]->right = tmp[child];
    //     }
    //     else 
    //     {
    //          tmp[parent]->left = tmp[child];
    //     }
    //     child--;
    //     parent = (child-1)/2;
    // }


    BinaryTreePrevOrder(tmp[0]);
    cout << endl;
    BinaryTreeInOrder(tmp[0]);
    cout << endl;
    BinaryTreePostOrder(tmp[0]);
    cout << endl;
    BinaryTreeDestroy(tmp[0]);
    return 0;
}
// 64 位输出请用 printf("%lld")