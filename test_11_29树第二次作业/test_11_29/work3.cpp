//给定一棵二叉树的中序和层序输出， 生成这棵树并按先序和后序输出
//
//其中树结构中结点信息为整数

#include <iostream>
#include <vector>
using namespace std;

struct node
{
    node* left;
    node* right;
    int val;
};

vector<int> level;
int pos[10000];

node* CreatTree(int index, node*& root)
{
    if (root == nullptr)
    {
        root = new node;
        root->left = nullptr;
        root->right = nullptr;
        root->val = level[index];
        return root;
    }
    else
    {
        if (pos[root->val] > pos[level[index]])
            CreatTree(index, root->left);
        else
            CreatTree(index, root->right);
        return root;
    }
}

void DestroyTree(node* root)
{
    if (root == nullptr)
        return;
    DestroyTree(root->left);
    DestroyTree(root->right);
    delete root;
}

void PrevOrder(node* root)
{
    if (root == nullptr)
        return;
    cout << root->val << " ";
    PrevOrder(root->left);
    PrevOrder(root->right);
}

void PostOrder(node* root)
{
    if (root == nullptr)
        return;
    PostOrder(root->left);
    PostOrder(root->right);
    cout << root->val << " ";
}

int main()
{
    int n;
    cin >> n;
    level.resize(n + 1);
    int num;
    for (int i = 1; i <= n; i++)
    {
        cin >> level[i];
    }
    for (int i = 1; i <= n; i++)
    {
        cin >> num;
        pos[num] = i;
    }

    node* root = nullptr;
    for (int index = 1; index <= n; index++)
    {
        CreatTree(index, root);
    }
    PrevOrder(root);
    cout << endl;
    PostOrder(root);
    cout << endl;

    DestroyTree(root);
    return 0;
}
// 64 位输出请用 printf("%lld")