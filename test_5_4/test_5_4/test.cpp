//#include <iostream>
//using namespace std;
//
//template<class _T>
//class Point3D 
//{
//    template<class _Ty>
//    friend ostream& operator<<(ostream& cout, const Point3D<_T> p);
//public:
//    _T x, y, z;
//    Point3D<_T>(_T x = 0, _T y = 0, _T z = 0)
//    {
//        this->x = x;
//        this->y = y;
//        this->z = z;
//    }
//
//    ~Point3D<_T>() {};
//};
//
//template<class _T, int length>
//class Skeleton 
//{
//    Point3D<_T> joints[length];
//public:
//    Skeleton(Point3D<_T>* j);
//    Point3D<_T>& operator[](string sign)
//    {
//        if (sign == "HEAD")
//        {
//            return this->joints[0];
//        }
//        else if (sign == "NECK")
//        {
//            return this->joints[1];
//        }
//    }
//};
//
//template<class _T, int length>
//Skeleton<_T, length>::Skeleton(Point3D<_T>* j)
//{
//    for (int i = 0; i < length; i++)
//    {
//        joints[i] = j[i];
//    }
//}
//
//template<class _T>
//ostream& operator<<(ostream& cout, const Point3D<_T> p)
//{
//    cout << p.x << " " << p.y << " " << p.z << endl;
//    return cout;
//}
//int main()
//{
//    Point3D<float> p1(0.0f, 0.0f, 0.0f);
//    Point3D<float> p2(1.1f, 1.1f, 1.1f);
//    Point3D<float> pSet[2];
//    pSet[0] = p1; pSet[1] = p2;
//    Skeleton<float, 2> sk(pSet);
//    cout << sk["HEAD"] << sk["NECK"];
//
//    return 0;
//}


//#include<iostream>
//#include<vector>
//using namespace std;
//int main() {
//	vector<int> v;
//	for (int i = 0; i < 6; i++)
//	{
//		v.push_back(i);//尾插
//	}
//	//标准迭代器的使用
//	vector<int>::iterator it;
//	for (it = v.begin(); it != v.end(); it++)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	for (int i = 0; i < 3; i++)
//	{
//		v.pop_back();//尾删
//	}
//	for (int i = 0; i < (int)v.size(); i++)
//	{
//		cout << v[i] << " ";
//	}
//	cout << endl;
//	return 0;
//}

//#include <iostream>
//#include <vector>
//using namespace std;
//int main()
//{
//    vector<int> v(6, 1);
//    /*for (int i = 0; i < 6; i++)
//    {
//        v.push_back(i);
//    }*/
//    for (int i = 0; i < (int)v.size(); i++)
//    {
//        cout << v[i] << " ";
//    }
//    cout << endl;
//    for (int i = 0; i < 6; i++)
//    {
//        v[i] = i;
//    }
//    for (int i = 0; i < (int)v.size(); i++)
//    {
//        cout << v[i] << " ";
//    }
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//#include <list>
//#include <vector>
//using namespace std;
//int main()
//{
//	list<int> mylist;
//	list<int>::iterator it;
//	for (int i = 1; i <= 5; i++)
//	{
//		mylist.push_back(i);
//	}
//	it = mylist.begin();
//	++it;
//	mylist.insert(it, 10);
//	mylist.insert(it, 2, 20); 
//	--it;
//	vector<int> myvector(2, 30);
//	mylist.insert(it, myvector.begin(), myvector.end());
//	for (it = mylist.begin(); it != mylist.end(); it++)
//	{
//		cout << " " << *it;
//	}
//	cout << endl;
//}


//#include <iostream>
//#include <list>
//#include <vector>
//using namespace std;
//int main()
//{
//    list<int> mylist;
//    list<int>::iterator it;
//    for (int i = 1; i <= 5; i++)
//    {
//        mylist.push_back(i);
//    }
//    it = mylist.begin();
//    it++;
//    mylist.insert(it, 10);
//    mylist.insert(it, 2, 20);
//    it--;
//    mylist.insert(it, 2, 30);
//    for (it = mylist.begin(); it != mylist.end(); it++)
//    {
//        cout << " " << *it;
//    }
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//int main()
//{
//	vector <int>v;
//	vector <int>::reverse_iterator p;
//	for (int i = 0; i < 10; i++)
//		v.push_back(i);
//	for (p = v.rbegin(); p != v.rend(); p++)
//		cout << *p << " ";
//	cout << endl;
//}


//#include <iostream>
//#include <vector>
//using namespace std;
//int main()
//{
//    vector<int> mylist;
//    vector<int>::reverse_iterator it;
//    for (int i = 0; i < 10; i++)
//    {
//        mylist.push_back(i);
//    }
//    for (it = mylist.rbegin(); it != mylist.rend(); it++)
//    {
//        cout << *it << " ";
//    }
//    cout << endl;
//    return 0;
//}


//#include <iostream>
//#include <vector>
//#include <list>
//using namespace std;
//int main() 
//{
//	list<double> first, second;
//	first.push_back(3.1);//first数列尾插入三个数
//	first.push_back(2.2);
//	first.push_back(2.9);
//	for (list<double>::iterator it = first.begin(); it != first.end(); ++it)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	second.push_back(3.7);//second数列尾插入三个数
//	second.push_back(7.1);
//	second.push_back(1.4);
//	first.sort();//对first进行排序
//	for (list<double>::iterator it = first.begin(); it != first.end(); ++it)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	second.sort();//对second进行排序
//	first.merge(second);//合并两个数列
//	for (list<double>::iterator it = first.begin(); it != first.end(); ++it)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	second.push_back(2.1);
//	first.merge(second);
//	for (list<double>::iterator it = first.begin(); it != first.end(); ++it)
//	{
//		cout << *it << " ";
//	}
//	cout << endl;
//	return 0;
//}


#include <iostream>
#include <deque>
using namespace std;
int main()
{
    deque<double> d;
    deque<double>::iterator it;
    for (int i = 0; i < 5; i++)
    {
        d.push_back(i);
    }
    it = d.begin();
    it++;
    d.insert(it, 30);//在it前面插入30
    for (it = d.begin(); it != d.end(); it++)
    {
        cout << *it << " ";
    }
    cout << endl;
    return 0;
}


//#include <iostream>
//#include <vector>
//#include <set>
//using namespace std;
//int main() 
//{
//	set<int> myset;
//	set<int>::iterator it;
//	pair<set<int>::iterator, bool> ret;
//	for (int i = 1; i <= 5; i++)
//	{
//		myset.insert(i * 10);
//	}
//	ret = myset.insert(20);
//	if (ret.second == false)
//	{
//		it = ret.first;
//	}
//	myset.insert(it, 25); 
//	myset.insert(it, 24); 
//	myset.insert(it, 26);
//	int myints[] = { 5,10,15 };
//	myset.insert(myints, myints + 3);
//	for (it = myset.begin(); it != myset.end(); it++)
//	{
//		cout << " " << *it;
//	}
//	cout << endl;
//	return 0;
//}







