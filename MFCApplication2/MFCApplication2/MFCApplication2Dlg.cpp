﻿// MFCApplication2Dlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "MFCApplication2.h"
#include "MFCApplication2Dlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据

#include "pch.h"
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCApplication2Dlg 对话框



CMFCApplication2Dlg::CMFCApplication2Dlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_MFCAPPLICATION2_DIALOG, pParent)
	, M_id(_T(""))
	, m_name(_T(""))
	, m_Cplusplus(_T(""))
	, m_math(_T(""))
	, m_zuyuan(_T(""))
	, m_physics(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCApplication2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, M_id);
	DDX_Text(pDX, IDC_EDIT2, m_name);
	DDX_Text(pDX, IDC_EDIT3, m_Cplusplus);
	DDX_Text(pDX, IDC_EDIT4, m_math);
	DDX_Text(pDX, IDC_EDIT5, m_zuyuan);
	DDX_Text(pDX, IDC_EDIT6, m_physics);
	DDX_Control(pDX, IDC_LIST1, M_studentList);
}

BEGIN_MESSAGE_MAP(CMFCApplication2Dlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST1, &CMFCApplication2Dlg::OnLvnItemchangedList1)
	ON_BN_CLICKED(IDC_BUTTON1, &CMFCApplication2Dlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CMFCApplication2Dlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CMFCApplication2Dlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CMFCApplication2Dlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CMFCApplication2Dlg::OnBnClickedButton5)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CMFCApplication2Dlg::OnNMClickList1)
	ON_BN_CLICKED(IDC_BUTTON6, &CMFCApplication2Dlg::OnBnClickedButton6)
	ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST1, &CMFCApplication2Dlg::OnLvnColumnclickList1)
	ON_EN_CHANGE(IDC_EDIT3, &CMFCApplication2Dlg::OnEnChangeEdit3)
	ON_EN_CHANGE(IDC_EDIT6, &CMFCApplication2Dlg::OnEnChangeEdit6)
END_MESSAGE_MAP()


// CMFCApplication2Dlg 消息处理程序

BOOL CMFCApplication2Dlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	//1.为列表控件加入表头
	CString strHeader[] = { _T("学号"), _T("姓名"),_T("C++"),_T("高数"),_T("组原"),_T("大物") };
	int nWidth[] = { 60, 60, 60, 60, 60, 60 };
	for (int i = 0; i < 6; i++)
	{
		M_studentList.InsertColumn(i, strHeader[i], LVCFMT_LEFT, nWidth[i]);
	}
	//2.设置列表空间显示风格，整行选中，显示网格
	M_studentList.SetExtendedStyle(M_studentList.GetExtendedStyle() | LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCApplication2Dlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCApplication2Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCApplication2Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCApplication2Dlg::OnLvnItemchangedList1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
}

Manager manager;

void CMFCApplication2Dlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码

	//将编辑框内容同步到变量
	UpdateData(TRUE);

	//1.往列表最后一行添加数据， count用来记录学生变量的个数
	int newindex = M_studentList.InsertItem(M_studentList.GetItemCount(), M_id);
	M_studentList.SetItemText(newindex, 1, m_name);
	M_studentList.SetItemText(newindex, 2, m_Cplusplus);
	M_studentList.SetItemText(newindex, 3, m_math);
	M_studentList.SetItemText(newindex, 4, m_zuyuan);
	M_studentList.SetItemText(newindex, 5, m_physics);
	//2.将编辑框的内容从CString转string

	string ID = CW2A(M_id.GetString());
	string NAME = CW2A(m_name.GetString());
	string CPLUSPLUS = CW2A(m_Cplusplus.GetString());
	string MATH = CW2A(m_math.GetString());
	string ZUYUAN = CW2A(m_zuyuan.GetString());
	string PHYSICS = CW2A(m_physics.GetString());

	//3.往类添加数据

	Student* pSt = new Student;
	pSt->setID(ID);
	pSt->setName(NAME);
	pSt->setCplusplus(CPLUSPLUS);
	pSt->setMath(MATH);
	pSt->setZuyuan(ZUYUAN);
	pSt->setPhysics(PHYSICS);
	manager.Add(pSt);

	//4.返回成功页面

	MessageBox(TEXT("添加成功！"));
}





void CMFCApplication2Dlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码

	//0.将编辑框的内容同步到变量

	UpdateData(TRUE);
	//1.获取列表选定索引
	int nIndex = M_studentList.GetSelectionMark();

	//2.更改列表内容
	M_studentList.SetItemText(nIndex, 0, M_id);
	M_studentList.SetItemText(nIndex, 1, m_name);
	M_studentList.SetItemText(nIndex, 2, m_Cplusplus);
	M_studentList.SetItemText(nIndex, 3, m_math);
	M_studentList.SetItemText(nIndex, 4, m_zuyuan);
	M_studentList.SetItemText(nIndex, 5, m_physics);

	//3.更改类的内容
	string ID = CW2A(M_id.GetString());
	string NAME = CW2A(m_name.GetString());
	string CPLUSPLUS = CW2A(m_Cplusplus.GetString());
	string MATH = CW2A(m_math.GetString());
	string ZUYUAN = CW2A(m_zuyuan.GetString());
	string PHYSICS = CW2A(m_physics.GetString());
	manager.Modify(ID, NAME, CPLUSPLUS, MATH, ZUYUAN, PHYSICS);

	//4.返回成功页面
	MessageBox(TEXT("修改成功！"));
}


void CMFCApplication2Dlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码

	//1.获取列表选定索引
	int nIndex = M_studentList.GetSelectionMark();

	//2.从列表中删除
	M_studentList.DeleteItem(nIndex);

	//3.从类中删除
	string ID = CW2A(M_id.GetString());
	manager.Delete(ID);

	//4.返回成功页面
	MessageBox(TEXT("删除成功！"));
}


void CMFCApplication2Dlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码

	//1.保存信息
	manager.Save("student.txt");
	//2.返回成功页面
	MessageBox(TEXT("保存成功！"));
	//3.关闭窗口
	CDialog::OnOK();
}


void CMFCApplication2Dlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码

	CDialog::OnCancel();
}


void CMFCApplication2Dlg::OnNMClickList1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;

	//1.获取列表选定索引
	int nIndex = M_studentList.GetSelectionMark();

	//2.获取索引指定内容
	CString ID = M_studentList.GetItemText(nIndex, 0);
	CString NAME = M_studentList.GetItemText(nIndex, 1);
	CString CPLUSPLUS = M_studentList.GetItemText(nIndex, 2);
	CString MATH = M_studentList.GetItemText(nIndex, 3);
	CString ZUYUAN = M_studentList.GetItemText(nIndex, 4);
	CString PHYSICS = M_studentList.GetItemText(nIndex, 5);

	//3.将内容赋值给变量
	M_id = ID;
	m_name = NAME;
	m_Cplusplus = CPLUSPLUS;
	m_math = MATH;
	m_zuyuan = ZUYUAN;
	m_physics = PHYSICS;

	//4.将变量内容显示到编辑框
	UpdateData(FALSE);
}


void CMFCApplication2Dlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码

	//1.打开文件，对学生列表进行更新
	manager.Open("student.txt");


	//2.将原来的列表内容进行清空
	M_studentList.DeleteAllItems();

	//3.获取类中的学生信息
	list<Student*> arr = *(manager.GetStudent());

	//4.将学生列表内容展现在列表上
	int count = 0;
	CString student[6];
	for (auto iter = arr.begin(); iter != arr.end(); iter++)
	{
		student[0] = (*iter)->getID().c_str();
		student[1] = (*iter)->getName().c_str();
		student[2] = (*iter)->getCplusplus().c_str();
		student[3] = (*iter)->getMath().c_str();
		student[4] = (*iter)->getZuyuan().c_str();
		student[5] = (*iter)->getPhysics().c_str();
		for (int i = 0; i < 6; i++)
			if (i == 0)
				M_studentList.InsertItem(count, student[0]);
			else
				M_studentList.SetItemText(count, i, student[i]);
		count++;
	}

	//5.返回成功页面
	MessageBox(TEXT("加载成功！"));
}



void CMFCApplication2Dlg::OnLvnColumnclickList1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;

	//1.获取点击列
	int column = pNMLV->iSubItem;
	//2.C++排序
	if (column == 2)
	{
		manager.sortByCplusplus();
	}
	//3.获取类中的学生信息
	list <Student*> arr = *(manager.GetStudent());

	//4.列表内容清空
	M_studentList.DeleteAllItems();

	//5.将排序好的学生列表内容展现在列表上
	int count = 0;
	CString student[6];
	for (auto iter = arr.begin(); iter != arr.end(); iter++)
	{
		student[0] = (*iter)->getID().c_str();
		student[1] = (*iter)->getName().c_str();
		student[2] = (*iter)->getCplusplus().c_str();
		student[3] = (*iter)->getMath().c_str();
		student[4] = (*iter)->getZuyuan().c_str();
		student[5] = (*iter)->getPhysics().c_str();
		for (int i = 0; i < 6; i++)
			if (i == 0)
				M_studentList.InsertItem(count, student[0]);
			else
				M_studentList.SetItemText(count, i, student[i]);
		count++;
	}
}






//void CMFCApplication2Dlg::OnEnChangeEdit3()
//{
//	// TODO:  如果该控件是 RICHEDIT 控件，它将不
//	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
//	// 函数并调用 CRichEditCtrl().SetEventMask()，
//	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
//
//	// TODO:  在此添加控件通知处理程序代码
//}


//void CMFCApplication2Dlg::OnEnChangeEdit6()
//{
//	// TODO:  如果该控件是 RICHEDIT 控件，它将不
//	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
//	// 函数并调用 CRichEditCtrl().SetEventMask()，
//	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。
//
//	// TODO:  在此添加控件通知处理程序代码
//}

