#include "pch.h"
#include "System.h"

void Student::setName(string name)
{
	this->name = name;
}
void Student::setID(string ID)
{
	this->ID = ID;
}
void Student::setCplusplus(string Cplusplus)
{
	this->Cplusplus = Cplusplus;
}
void Student::setMath(string math)
{
	this->math = math;
}
void Student::setZuyuan(string zuyuan)
{
	this->zuyuan = zuyuan;
}
void Student::setPhysics(string physics)
{
	this->physics = physics;
}

string Student::getName()
{
	return this->name;
}
string Student::getID()
{
	return this->ID;
}
string Student::getCplusplus()
{
	return this->Cplusplus;
}
string Student::getMath()
{
	return this->math;
}
string Student::getZuyuan()
{
	return this->zuyuan;
}
string Student::getPhysics()
{
	return this->physics;
}
istream& operator>>(istream& is, Student& st)
{
	is >> st.ID >> st.name >> st.Cplusplus >> st.math >> st.zuyuan >> st.physics;
	return is;
}

ostream& operator<<(ostream& os, Student& st)
{
	os << st.ID << "  " << st.name << "  " << st.Cplusplus << "  " << st.math << "  " << st.zuyuan << "  " << st.physics << endl;
	return os;
}

bool LessCplusplus(const Student* st1, const Student* st2)
{
	return st1->Cplusplus > st2->Cplusplus;
}

//删除数据
void Manager::Clear()
{
	for (auto iter = studentArray.begin(); iter != studentArray.end(); iter++)
	{
		delete* iter;
	}
}

//增加数据
void Manager::Add(Student* tmp)
{
	studentArray.push_back(tmp);
}

//删除指定数据
Student* Manager::Delete(string id)
{
	Student* pSt = NULL;
	for (auto iter = studentArray.begin(); iter != studentArray.end(); iter++)
	{
		if ((*iter)->getID() == id)
		{
			pSt = *iter;
			break;
		}
	}
	if (pSt != NULL)
	{
		studentArray.remove(pSt);
	}
	return pSt;
}

void Manager::Modify(string _name, string _ID, string _Cplusplus, string _math, string _zuyuan, string _physics)
{
	Student* pSt = NULL;
	for (auto iter = studentArray.begin(); iter != studentArray.end(); iter++)
	{
		if ((*iter)->getID() == _ID)
		{
			pSt = *iter;
			break;
		}
	}
	if (pSt != NULL)
	{
		pSt->setName(_name);
		pSt->setCplusplus(_Cplusplus);
		pSt->setMath(_math);
		pSt->setZuyuan(_zuyuan);
		pSt->setPhysics(_physics);
	}
}

list<Student*>* Manager::GetStudent()
{
	return &studentArray;
}

bool Manager::Save(const char* fileName)
{
	ofstream file(fileName, ios::out);
	//创建文件失败返回
	if (!file)
	{
		return false;
	}
	//写入学生个数
	file << studentArray.size() << endl;

	for (auto iter = studentArray.begin(); iter != studentArray.end(); iter++)
	{
		file << (**iter) << endl;
	}
	//文件关闭
	file.close();
	return true;
}


bool Manager::Open(const char* fileName)
{
	//原列表内容清空
	Clear();

	//创建文件读入流
	ifstream file(fileName, ios::in);

	//读入失败返回
	if (!file)
	{
		return false;
	}

	//接收文件学生人数
	int n = 0;
	file >> n;

	//为每个学生创建存储空间，并更新原有列表内容
	for (int k = 0; k < n; k++)
	{
		Student* pSt = new Student;
		file >> *pSt;
		studentArray.push_back(pSt);
	}
	file.close();
	return true;
}


void Manager::sortByCplusplus()
{
	studentArray.sort(LessCplusplus);
}













