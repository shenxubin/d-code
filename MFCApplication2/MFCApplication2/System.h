#pragma once

#include <iostream>
#include <fstream>
#include <list>
using namespace std;
class Student
{
public:
	string name;
	string ID;
	string Cplusplus;
	string math;
	string zuyuan;
	string physics;
public:
	void setName(string name);
	void setID(string ID);
	void setCplusplus(string Cplusplus);
	void setMath(string math);
	void setZuyuan(string zuyuan);
	void setPhysics(string physics);
	string getName();
	string getID();
	string getCplusplus();
	string getMath();
	string getZuyuan();
	string getPhysics();
	friend ostream& operator<<(ostream& os, Student& st);
	friend istream& operator>>(istream& is, Student& st);
	friend bool LessCplusplus(const Student* st1, const Student* st2);
};

class Manager
{
private:
	list<Student*>studentArray;
public:
	void Clear();//删除数据
	void Add(Student* tmp);//增加数据
	Student* Delete(string id);//删除指定数据
	void Modify(string _name, string _ID, string _Cplusplus, string _math, string _zuyuan, string _physics);
	list<Student*>* GetStudent();
	bool Save(const char* fileName);
	bool Open(const char* fileName);
	void sortByCplusplus();
};












