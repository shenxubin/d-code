#include <iostream>
#include <string>
#include <vector>
#include <boost/filesystem.hpp>
#include "util.hpp"
const std::string src_path = "./data/input";
const std::string output = "./data/raw_html/raw.txt";

typedef struct DocInfo
{
    std::string title;
    std::string content;
    std::string url;
}DocInfo_t;

bool EnumFile(const std::string& src_path, std::vector<std::string>* files_list)
{
    namespace fs = boost::filesystem;
    fs::path root_path(src_path);
    //判断路径是否存在
    if(!fs::exists(root_path))
    {
        std::cerr << src_path << "not exists" << std::endl;
        return false;
    }
    //定义一个空的迭代器，用来判断递归结束
    fs::recursive_directory_iterator end;
    for(fs::recursive_directory_iterator iter(root_path); iter != end; iter++)
    {
        //判断文件是否为普通文件
        if(!fs::is_regular_file(*iter)) continue;
        //判断文件路径后缀是否符合条件
        if(iter->path().extension() != ".html") continue;
        //std::cout << "debug: " << iter->path().string() << std::endl;
        files_list->push_back(iter->path().string());
    }
    return true;
}
static bool ParseTitle(const std::string& file, std::string *title)
{
    std::size_t begin = file.find("<title>");
    if(begin == std::string::npos) return false;
    std::size_t end = file.find("</title>");
    if(end == std::string::npos) return false;
    begin += std::string("<title>").size();
    if(begin > end) return false;
    *title = file.substr(begin, end-begin);
    return true;
}
static bool ParseContent(const std::string &file, std::string *content)
{
    //去标签，基于一个简易的状态机
    enum status
    {
        LABLE,
        CONTENT
    };
    enum status s = LABLE;
    for(char c : file)
    {
        switch (s)
        {
        case LABLE:
            if(c == '>') s = CONTENT;
            break;
        case CONTENT:
            if(c == '<') s = LABLE;
            else
            {
                //我们不想保留原始文本中的\n，因为我们想用\n作为html解析后文本的分隔符
                if(c == '\n') c = ' ';
                content->push_back(c);
            }
            break;
        default:
            break;
        }
    }
    return true;
}
static bool ParseUrl(const std::string &file_path, std::string *url)
{
    std::string url_head = "https://www.boost.org/doc/libs/1_87_0/doc/html";
    std::string url_tail = file_path.substr(src_path.size());
    *url = url_head + url_tail;
    return true;
}
bool ParseHtml(const std::vector<std::string>& files_list, std::vector<DocInfo_t>* results)
{
    for(const std::string& file : files_list)
    {
        //读取文件
        std::string result;
        if(!ns_util::file_util::ReadFile(file, &result))
        {
            continue;
        }
        //解析文件，提取title
        DocInfo_t doc;
        if(!ParseTitle(result, &doc.title))
        {
            continue;
        }
        //解析文件，提取content，就是去标签
        if(!ParseContent(result, &doc.content))
        {
            continue;
        }
        //解析指定的文件路径，构建url
        if(!ParseUrl(file, &doc.url))
        {
            continue;
        }
        results->push_back(std::move(doc));
    }
    return true;
}
bool SaveHtml(const std::vector<DocInfo_t>& results, const std::string& output)
{
    #define SEP '\3'
    std::ofstream out(output, std::ios::out | std::ios::binary);
    if(!out.is_open())
    {
        std::cerr << "open " << output << " failed!" << std::endl;
        return false;
    }
    for(auto& e : results)
    {
        std::string outString;
        outString = e.title;
        outString += SEP;
        outString += e.content;
        outString += SEP;
        outString += e.url;
        outString += '\n';

        out.write(outString.c_str(), outString.size());
    }
    out.close();
    return true;
}
int main()
{
    std::vector<std::string> files_list;
    //第一步：递归式地把每个html文件名带路径保存到files_list中，方便后期进行一个一个的文件进行读取
    if(!EnumFile(src_path, &files_list))
    {
        std::cerr << "enum file name error!" << std::endl;
        return 1;
    }
    //第二步：按照files_list读取每个文件的内容，并进行解析
    std::vector<DocInfo_t> results;
    if(!ParseHtml(files_list, &results))
    {
        std::cerr << "parse html error!" << std::endl;
        return 2;
    }
    //第三步: 把解析完毕的各个文件内容，写入到output,按照\3作为每个文档的分割符
    if(!SaveHtml(results, output)){
        std::cerr << "sava html error" << std::endl;
        return 3;
    }
    return 0;
}