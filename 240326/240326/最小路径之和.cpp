class Solution {
public:
    int minPathSum(vector<vector<int>>& grid)
    {
        int row = grid.size(); int col = grid[0].size();
        vector<vector<int>> vect(row, vector<int>(col));

        for (int i = 0; i < row; i++)
        {
            if (i == 0)
                vect[i][0] = grid[i][0];
            else
            {
                vect[i][0] = vect[i - 1][0] + grid[i][0];
            }
        }
        for (int i = 0; i < col; i++)
        {
            if (i == 0)
                vect[0][i] = grid[0][i];
            else
            {
                vect[0][i] = vect[0][i - 1] + grid[0][i];
            }
        }

        for (int i = 1; i < row; i++)
        {
            for (int j = 1; j < col; j++)
            {
                if (vect[i - 1][j] < vect[i][j - 1])
                    vect[i][j] = grid[i][j] + vect[i - 1][j];
                else
                    vect[i][j] = grid[i][j] + vect[i][j - 1];
            }
        }

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                cout << vect[i][j] << " ";
            }
            cout << endl;
        }

        return vect[row - 1][col - 1];
    }
};