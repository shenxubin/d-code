class Solution {
public:

    int findMin(int x, int y, int z)
    {
        if (x < y)
        {
            if (z < x)
                return z;
            else
                return x;

        }
        else
        {
            if (z < y)
                return z;
            else
            {
                return y;
            }
        }
    }
    int minFallingPathSum(vector<vector<int>>& matrix) {


        //以某个位置为结尾
        vector<vector<int>> vect(matrix.size(), vector<int>(matrix.size()));
        for (int i = 0; i < matrix.size(); i++)
        {
            //vect[i].resize(matrix.size());
            for (int j = 0; j < matrix.size(); j++)
            {
                vect[i][j] = matrix[i][j];
            }
        }

        for (int i = 1; i < vect.size(); i++)
        {
            for (int j = 0; j < vect[i].size(); j++)
            {
                if (j == 0)
                {
                    vect[i][j] += min(vect[i - 1][j], vect[i - 1][j + 1]);
                }
                else if (j == vect[i].size() - 1)
                {
                    vect[i][j] += min(vect[i - 1][j - 1], vect[i - 1][j]);
                }
                else
                {
                    vect[i][j] += findMin(vect[i - 1][j - 1], vect[i - 1][j], vect[i - 1][j + 1]);
                }
            }
        }

        int min = vect[vect.size() - 1][0];
        for (int i = 1; i < vect.size(); i++)
        {
            if (vect[vect.size() - 1][i] < min)
                min = vect[vect.size() - 1][i];
        }
        return min;

    }
};