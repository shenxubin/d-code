#include <iostream>
#include <vector>
using namespace std;

int cards[14] = { 0,1,2,3,4,5,6,7,8,9,10,11,12,13 };
int times[14] = { 0 };
int no_zero_subcript[5] = { 0 };
int j = 0;
bool IsContinuous(vector<int>& numbers)
{
    for (int i = 0; i < numbers.size(); i++)
    {
        times[numbers[i]]++;
        if (times[numbers[i]] == 2 && numbers[i] != 0)
            return false;
    }
    for (int i = 1; i < 14; i++)
    {
        if (times[i] != 0)
            no_zero_subcript[j++] = i;
    }

    if (times[0] == 4)
        return true;

    else
    {
        if (no_zero_subcript[j - 1] - no_zero_subcript[0] <= 4)
            return true;
        else
            return false;

    }
    return true;
}

int main()
{
    vector<int> vect = { 6,0,2,0,4 };
    bool ret = IsContinuous(vect);

    cout << ret << endl;
    return 0;
}
