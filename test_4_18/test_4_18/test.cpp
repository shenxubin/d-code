#include <iostream>
using namespace std;
class Person
{
public:
    string Name;
protected:
    string Gender;
    int Age;
public:
    Person(string name, string gender, int age)
    {
        Name = name;
        Gender = gender;
        Age = age;
        cout << "Person" << Name << "constructed" << endl;
    }
    ~Person()
    {
        cout << "Person" << Name << "destructed" << endl;
    }
};

class StudentRecord : virtual public Person
{
public:
    string Number;
    string ClassName;
    static int TotalCount;
protected:
    int Score;
public:
    StudentRecord(string name, string gender ,int age,string number, string classname, int score):
        Person(name, gender, age)
    {
        Number = number;
        ClassName = classname;
        Score = score;
        TotalCount++;
        cout << "Student" << Name << "constructed" << endl;
    }
    ~StudentRecord()
    {
        cout << "Student" << Name << "destructed" << endl;
    }
};
int StudentRecord::TotalCount = 0;

class TeacherRecord : virtual public Person
{
public:
    string CollegeName;
    string DepartmentName;
protected:
    int Year;
public:
    TeacherRecord(string name, string gender, int age, 
        string collegename, string departmentname, int year)
        :Person(name, gender, age)
    {
        CollegeName = collegename;
        DepartmentName = departmentname;
        Year = year;
        cout << "teacher" << Name << "constructed" << endl;
    }
    ~TeacherRecord()
    {
        cout << "teacher" << Name << "destructed" << endl;
    }
};

class TeachingAssistant : public StudentRecord, public TeacherRecord
{
public:
    string LectureName;
    void Show()
    {
        cout << "Name:" << Name << " Gender:" << Gender << " Age:" << Age << " Number:" << Number
            << " ClassName:" << ClassName << " TotalCount:" << TotalCount << " Score:" << Score
            << " CollegeName:" << CollegeName << " DepartmentName:" << DepartmentName
            << " Year:" << Year << " LectureName:" << LectureName << endl;
            
    }
    TeachingAssistant(string name, string gender, int age,
        string number, string classname, int score,
        string collegename, string departmentname, int year, string lectureName)
        :Person(name,gender,age), StudentRecord(name,gender,age,number,classname,score),
        TeacherRecord(name,gender,age,collegename,departmentname,year)
    {
        LectureName = lectureName;
        cout << "teachingassistant" << Name << "constructed" << endl;
    } 
    void SetName(string name)
    {
        Name = name;
    }
    ~TeachingAssistant()
    {
        cout << "teachingassistant" << Name << "destructed" << endl;
    }
};

int main()
{
    TeachingAssistant p1("郑七", "男", 22, "2010123", "软20101", 89, "信息",
        "软件", 1, "数据结构");
    p1.Show();
    p1.SetName("郑八");
    p1.Show();
    return 0;
}