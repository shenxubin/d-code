#include <iostream>
using namespace std;
//
//class Person
//{
//public:
//    virtual void BuyTivkets()
//    {
//        cout << "买全票！" << endl;
//    }
//};
//
//
//class Student :public Person
//{
//public:
//    /*注意：在重写基类虚函数时，派生类的虚函数在不加virtual关键字时，虽然也可以构成重写(因
//为继承后基类的虚函数被继承下来了在派生类依旧保持虚函数属性),但是该种写法不是很规范，不建议
//这样使用*/
///*void BuyTicket() { cout << "买半票!" << endl; }*/
//    virtual void BuyTivkets()
//    {
//        cout << "买半票！" << endl;
//    }
//};
//
//void Func(Person& p)
//{
//    p.BuyTivkets();
//}
//
//int main()
//{
//    Person p1;
//    Func(p1);
//    Student s1;
//    Func(s1);
//    return 0;
//}




//class Person 
//{
//public:
//	virtual Person* f() 
//	{ 
//		return new Person;
//	}
//};
//class Student : public Person
//{
//public:
//	virtual Student* f() 
//	{ 
//		return new Student;
//	}
//};
//
//
//int main()
//{
//
//	return 0;
//}


//class Person
//{
//public:
//	virtual ~Person() 
//	{
//		cout << "~Person()" << endl;
//	}
//};
//class Student : public Person
//{
//public:
//	virtual ~Student() 
//	{
//		cout << "~Student()" << endl;
//	}
//};
//// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函
////数，才能构成多态，才能保证p1和p2指向的对象正确的调用析构函数。
//int main()
//{
//	Person* p1 = new Person;
//	Person* p2 = new Student;
//	delete p1;
//	delete p2;
//	return 0;
//}


//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive()  //报错
//
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
	          

//class Car 
//{
//public:
//	virtual void Drive() {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() override
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};

class Car
{
public:
	virtual void Drive() = 0;
};
class Benz :public Car
{
public:
	virtual void Drive()
	{
		cout << "Benz-舒适" << endl;
	}
};
class BMW :public Car
{
public:
	virtual void Drive()
	{
		cout << "BMW-操控" << endl;
	}
};
void Test()
{
	Car* pBenz = new Benz;
	pBenz->Drive();
	Car* pBMW = new BMW;
	pBMW->Drive();
}


int main()
{
	Test();
	return 0;
}



