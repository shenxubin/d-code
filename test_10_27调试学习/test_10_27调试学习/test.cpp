#include <iostream>
using namespace std;

void print()
{
	cout << "学习调试技巧" << endl;
}
int main()
{
	int arr[] = { 1,2,3,4,5 };

	print();
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", arr[i]);
	}
	cout << endl;

	return 0;
}

//void test02()
//{
//	cout << "学习调试" << endl;
//}
//
//void test01()
//{
//	test02();
//}
//
//void test()
//{
//	test01();
//}
//
//int main()
//{
//	test();
//
//	return 0;
//}

//栈区内存的使用习惯：先使用高地址处的空间，再使用低地址处的空间。