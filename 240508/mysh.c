#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
int main(int argc, char* argv[])
{
    pid_t id = fork();
    if(id == 0)
    {
	char* argv2[argc];
	for(int i = 1; i<argc; i++)
	{
	    argv2[i-1] = argv[i];
	}
	argv2[argc-1] = NULL;
        execvp(argv2[0], argv2);	
        exit(0);    
    }         
    pid_t rid = waitpid(id, NULL, 0);
    if(rid > 0)
        printf("wait succeed!\n");
    return 0; 
}             
              
