#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
int main()
{
   pid_t id = fork();
   if(id == 0)
   {
       //execl("./mytest2", "./mytest2", NULL);
       execl("/usr/bin/ls", "ls", "-l", NULL);
       exit(0);
   }
   pid_t rid = waitpid(id, NULL, 0);
   if(rid > 0)
   printf("wait succeed!\n");
    return 0;
}

