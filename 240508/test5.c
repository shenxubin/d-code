#include <stdio.h>  
#include <stdlib.h>  
#include <unistd.h>  
#include <string.h>  
#include <fcntl.h>  
#include <sys/types.h>  
#include <sys/wait.h>  
  
#define MAILBOX "/var/spool/mail/$(whoami)" 
#define CHECK_INTERVAL 60 // 检查邮件的间隔时间（秒）  
#define BEEP_COMMAND "beep -f 1000 -l 1000"
  
// 尝试通过/dev/pcspkr发出声音
void beep_via_pcspkr() {  
    int fd = open("/dev/pcspkr", O_WRONLY);  
    if (fd >= 0) {  
        const unsigned char beep[] = {0x07, 0}; // 简单的蜂鸣声  
        write(fd, beep, sizeof(beep));  
        close(fd);  
    } else {  
        perror("Failed to open /dev/pcspkr");  
    }  
}  
  
// 检查邮件并发出声音  
void check_mail_and_beep() {  
    pid_t pid = fork();  
  
    if (pid == 0) {
        // 调用mail命令检查新邮件
        execlp("mail", "mail", "-N", NULL);  
  
        // 如果mail命令失败，则退出  
        _exit(EXIT_FAILURE);  
    } else if (pid > 0) { 
        int status;  
        waitpid(pid, &status, 0); // 等待子进程结束  
  
        // 检查mail命令的退出状态  
        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {  
            // 如果有新邮件，尝试发出声音  
            pid_t beep_pid = fork();  
            if (beep_pid == 0) {  
                execlp("beep", "beep", "-f", "1000", "-l", "1000", NULL);  
                _exit(EXIT_FAILURE); // 如果beep命令失败，则退出  
            } else if (beep_pid > 0) {  
                waitpid(beep_pid, NULL, 0); // 等待beep命令结束  
            }  
        }  
    } else {  
        perror("Failed to fork");  
    }  
}  
  
int main() 
{  
    while (1) 
    {  
        check_mail_and_beep();  
        sleep(CHECK_INTERVAL); // 等待指定的时间间隔后再次检查  
    }  
  
    return 0; 
}
