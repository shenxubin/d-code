#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sys/types.h>
int main()
{
    pid_t id = fork();
    if(id == 0)
    {
        char* envp[] = {"PATH=/tmp", "USER=liu", NULL};
	char* args[] = {"env", NULL};
	execve("/usr/bin/env", args, envp);
	exit(0);
    }
    pid_t rid = waitpid(id, NULL,0);
    if(rid > 0)
        printf("wait succeed!\n");
    return 0;
}
