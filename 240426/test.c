#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<string.h>
#include<time.h>
#include<pwd.h>
#include<grp.h>
void filebuf(mode_t mode,char *rs)
{ //判断文件类型以及所有者，组和其他的权限；
	if(S_ISDIR(mode))
	{
	    rs[0]='d';
	}
	else if(S_ISREG(mode))
	{
            rs[0]='-';
	}
	else if(S_ISFIFO(mode))
	{
            rs[0]='p';
	}
	else if(S_ISLNK(mode))
	{
       	    rs[0]='l';
	}
	else if(S_ISBLK(mode))
	{
	    rs[0]='b';
	}
	else if(S_ISSOCK(mode))
	{
	    rs[0]='s';
	}
	else if(S_ISCHR(mode))
	{
	    rs[0]='c';
	}
    if(mode & S_IRUSR)  rs[1]='r';
    else rs[1]='-';
    if(mode & S_IWUSR)  rs[2]='w';
    else rs[2]='-';
    if(mode & S_IXUSR)  rs[3]='x';
    else rs[3]='-';
    if(mode & S_IRGRP)  rs[4]='r';
    else rs[4]='-';
    if(mode & S_IWGRP)  rs[5]='w';
    else rs[5]='-';
    if(mode & S_IXGRP)  rs[6]='x';
    else rs[6]='-';
    if(mode & S_IROTH)  rs[7]='r';
    else rs[7]='-';
    if(mode & S_IWOTH)  rs[8]='w';
    else rs[8]='-';
    if(mode & S_IXOTH)  rs[9]='x';
    else rs[9]='-';
    rs[10]='\0';
}

int main(int argc, char *argv[]) 
{	
    struct stat fst;
    struct tm *mytime=(struct tm *)malloc(sizeof(struct tm));
    char rs[12];
    if(argc!=2)
    {
        fprintf(stderr,"Usage: %s <pathname>\n",argv[0]);
        exit(EXIT_FAILURE);
    }
    if(stat(argv[1],&fst)==-1)
    {
        perror("stat");
        exit(EXIT_FAILURE);
    }
    filebuf(fst.st_mode,rs);
    printf("%s",rs); //输出文件类型与权限信息；
    printf(" %lu",fst.st_nlink);//输出文件的硬链接数；
    printf(" %s",getpwuid(fst.st_uid)->pw_name);//输出所属用户名；
    printf(" %s",getgrgid(fst.st_gid)->gr_name);//输出用户所在组;
    printf(" %1ld",fst.st_size);//输出文件大小；
    mytime=localtime(&fst.st_mtime);//获取文件修改时间；
    printf(" %d-%02d-%02d %02d:%02d",
    mytime->tm_year+1900,mytime->tm_mon+1,mytime->tm_mday,mytime->tm_hour,mytime->tm_min);
    printf(" %s",argv[1]);//输出文件名；
    printf("\n");
    return 0;
}
