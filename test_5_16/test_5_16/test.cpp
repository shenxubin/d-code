#include <iostream>
#include <string>
using namespace std;

class Person
{
protected:
    string szName;
public:
    Person(string szName)
    {
        this->szName = szName;
    }
    virtual void Print()
    {
        cout << "Person " << this->szName << endl;
    }
};

class Student :public Person
{
protected:
    int iNumber;
public:
    Student(string szName, int iNumber) :Person(szName)
    {
        this->iNumber = iNumber;
    }
    virtual void Print()
    {
        cout << "Student " << this->szName << " " << this->iNumber << endl;
    }
};

class Teacher :public Person
{
protected:
    int iYear;
public:
    Teacher(string szName, int iYear) :Person(szName)
    {
        this->iYear = iYear;
    }
    virtual void Print()
    {
        cout << "Teacher " << this->szName << " " << this->iYear << endl;
    }
};

class Graduate :public Student
{
protected:
    string szResearch;
public:
    Graduate(string szName, int iYear, string szResearch) :Student(szName,iYear)
    {
        this->szResearch = szResearch;
    }
    virtual void Print()
    {
        cout << "Graduate " << this->szName << " "  << this->iNumber<< " " << this->szResearch << endl;
    }
};
int main()
{
    int n, m;
    string szType;
    string szName;
    int iNumber;
    int iYear;
    string szResearch;
    string tmp;
    cin >> n;
    Person** p = new Person * [n];
    for (int i = 0; i < n; i++)
    {
        cin >> szType;
        if (szType == "Person")
        {
            cin >> szName;
            p[i] = new Person(szName);
            continue;
        }
        else if (szType == "Student")
        {
            cin >> szName >> iNumber;
            p[i] = new Student(szName, iNumber);
            continue;
        }
        else if (szType == "Teacher")
        {
            cin >> szName >> iYear;
            p[i] = new Teacher(szName, iYear);
            continue;
        }
        else if (szType == "Graduate")
        {
            cin >> szName >> iNumber >> szResearch;
            p[i] = new Graduate(szName, iNumber, szResearch);
            continue;
        }
    }
    
    return 0;
}


//#include <iostream>
//#include <string>
//using namespace std;
//
//class Person
//{
//protected:
//	string szName;
//
//public:
//	Person(string szName)
//	{
//		this->szName = szName;
//	}
//
//	virtual void Print()
//	{
//		cout << "Person " << szName << endl;
//	}
//};
//
//class Student : public Person
//{
//protected:
//	int iNumber;
//
//public:
//	Student(string szName, int iNumber) : Person(szName)
//	{
//		this->iNumber = iNumber;
//	}
//
//	void Print()
//	{
//		cout << "Student " << szName << " " << iNumber << endl;
//	}
//};
//
//class Teacher : public Person
//{
//protected:
//	int iYear;
//
//public:
//	Teacher(string szName, int iYear) : Person(szName)
//	{
//		this->iYear = iYear;
//	}
//
//	void Print()
//	{
//		cout << "Teacher " << szName << " " << iYear << endl;
//	}
//};
//
//class Graduate : public Student
//{
//protected:
//	string szResearch;
//
//public:
//	Graduate(string szName, int iNumber, string szResearch) : Student(szName, iNumber)
//	{
//		this->szResearch = szResearch;
//	}
//
//	void Print()
//	{
//		cout << "Graduate " << szName << " " << iNumber << " " << szResearch << endl;
//	}
//};
//
//int main()
//{
//	int n;
//	cin >> n;
//
//	Person** arr = new Person * [n];
//
//	for (int i = 0; i < n; i++)
//	{
//		string personname;
//		cin >> personname;
//		if (personname == "Person")
//		{
//			string name;
//			cin >> name;
//			arr[i] = new Person(name);
//		}
//		else if (personname == "Student")
//		{
//			string name;
//			int number;
//			cin >> name >> number;
//			arr[i] = new Student(name, number);
//		}
//		else if (personname == "Teacher")
//		{
//			string name;
//			int year;
//			cin >> name >> year;
//			arr[i] = new Teacher(name, year);
//		}
//		else if (personname == "Graduate")
//		{
//			string name;
//			int number;
//			string research;
//			cin >> name >> number >> research;
//			arr[i] = new Graduate(name, number, research);
//		}
//	}
//
//	/*while (true)
//	{
//		int m;
//		cin >> m;
//		if (m >= 0 && m < n)
//		{
//			arr[m]->Print();
//		}
//		else if (m == -1)
//		{
//			break;
//		}
//	}*/
//
//	while (true)
//	{
//		string m;
//		cin >> m;
//
//		if (m.length() == 1)
//		{
//			const char* p = m.c_str();
//			int q = atoi(p);
//			if (q >= 0 && q < n)
//			{
//				arr[q]->Print();
//			}
//		}
//		else if (m == "exit")
//		{
//			break;
//		}
//	}
//
//	return 0;
//}


//#include <iostream>
//using namespace std;
//class Rectangle
//{
//	friend ostream& operator<<(ostream& cout, Rectangle& r);
//private:
//	double x[4];
//	double y[4];
//public:
//	Rectangle(double _x[4], double _y[4])
//	{
//		for (int i = 0; i < 4; i++)
//		{
//			this->x[i] = _x[i];
//			this->y[i] = _y[i];
//		}
//	}
//	double l1 = sqrt((x[0] - x[1]) * (x[0] - x[1]) + (y[0] - y[1]) * (y[0] - y[1]));
//	double l2 = sqrt((x[1] - x[2]) * (x[1] - x[2]) + (y[1] - y[2]) * (y[1] - y[2]));
//	double l3 = sqrt((x[0] - x[2]) * (x[0] - x[2]) + (y[0] - y[2]) * (y[0] - y[2]));
//	double GetSideLength(int i)
//	{
//		if (i == 0 || i == 2)
//		{
//			return l1;
//		}
//		else if (i == 1 || i == 3)
//		{
//			return l2;
//		}
//	}
//	double GetSquare()
//	{
//		double cos = (l1 * l1 + l2 * l2 - l3 * l3) / (2 * l1 * l2);
//		double sin = sqrt(1 - cos * cos);
//		return l1 * l2 * sin;
//	}
//
//};
//
//ostream& operator<<(ostream& cout, Rectangle& r)
//{
//	cout << r.GetSquare() << endl;
//	return cout;
//}
//int main()
//{
//	double x[4], y[4];
//	for (int i = 0; i < 4; i++)
//	{
//		cin >> x[i] >> y[i];
//	}
//	Rectangle r(double x[4], double y[4]);
//	cout << r;
//	return 0;
//}




