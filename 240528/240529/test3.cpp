#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define SEGMENT_NUM 4  // 段表大小
#define PAGE_NUM 16    // 页表大小
#define PAGE_SIZE 1024 // 页大小

// 段表项
typedef struct {
    int base;         // 段基址
    int limit;        // 段长度
} segment_t;

// 页表项
typedef struct {
    int frame;        // 物理帧号
} page_t;

segment_t segment_table[SEGMENT_NUM]; // 段表
page_t page_table[SEGMENT_NUM][PAGE_NUM]; // 页表

// 初始化段表和页表
void init_tables() {
    int i = 0;
    int j = 0;
    for (i = 0; i < SEGMENT_NUM; i++) {
        for (j = 0; j < PAGE_NUM; j++) {
            page_table[i][j].frame = rand() % 64;
        }
    }
}

// 查找段表项
int find_segment(int addr) {
    int segment_index = -1;
    int i = 0;
    for (i = 0; i < SEGMENT_NUM; i++) {
        if (addr >= segment_table[i].base && addr < segment_table[i].base + segment_table[i].limit) {
            segment_index = i;
            break;
        }
    }
    return segment_index;
}

// 查找页表项
int find_page(int segment_index, int addr) {
    int page_index = addr / PAGE_SIZE;
    return page_table[segment_index][page_index].frame * PAGE_SIZE + (addr % PAGE_SIZE);
}

// 地址转换
int translate_address(int addr) {
    int segment_index = find_segment(addr);
    return find_page(segment_index, addr);
}

int main() {
    init_tables();
    // 添加一些段表和页表项
    segment_table[0].base = 0;
    segment_table[0].limit = 4096;

    segment_table[1].base = 4096;
    segment_table[1].limit = 8192;

    // 模拟地址转换
    int test_addr[] = { 500, 4500, 8000, 6500 };
    int i = 0;
    for (i = 0; i < sizeof(test_addr) / sizeof(int); i++) {
        int translated_addr = translate_address(test_addr[i]);
        printf("待转换地址：%d\n", test_addr[i]);
        printf("段表项：基址=%d，长度=%d\n", segment_table[find_segment(test_addr[i])].base,
            segment_table[find_segment(test_addr[i])].limit);
        printf("页表项：物理帧号=%d\n", page_table[find_segment(test_addr[i])][test_addr[i] / PAGE_SIZE].frame);
        printf("转换后的地址：%d\n", translated_addr);
        printf("\n");
    }
    return 0;
}