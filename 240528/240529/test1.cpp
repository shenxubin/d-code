//#include <stdio.h>
//
//#define MAX_PARTITIONS 10
//
//// 定义分区说明表项
//struct Partition {
//    int partitionNumber;
//    int size;
//    int startAddress;
//    int isAllocated;
//};
//
//// 定义分区说明表
//struct Partition partitionTable[MAX_PARTITIONS];
//
//// 初始化分区说明表
//void initializePartitionTable() {
//    int i = 0;
//    for (i = 0; i < MAX_PARTITIONS; i++) {
//        partitionTable[i].partitionNumber = i + 1;
//        partitionTable[i].isAllocated = 0;
//    }
//
//    // 设置已分配的分区
//    partitionTable[0].size = 25;
//    partitionTable[0].startAddress = 16;
//    partitionTable[0].isAllocated = 1;
//
//    partitionTable[1].size = 38;
//    partitionTable[1].startAddress = 40;
//    partitionTable[1].isAllocated = 1;
//
//    // 设置未分配的分区
//    partitionTable[2].size = 57;
//    partitionTable[2].startAddress = 110;
//    partitionTable[2].isAllocated = 0;
//}
//
//// 分配内存
//int allocateMemory(int size) {
//    int i = 0;
//    for (i = 0; i < MAX_PARTITIONS; i++) {
//        if (!partitionTable[i].isAllocated && partitionTable[i].size >= size) {
//            partitionTable[i].isAllocated = 1;
//            return partitionTable[i].startAddress;
//        }
//    }
//
//    return -1;  // 分区不可用
//}
//
//// 释放内存
//void deallocateMemory(int partitionNumber) {
//    int i = 0;
//    for (i = 0; i < MAX_PARTITIONS; i++) {
//        if (partitionTable[i].partitionNumber == partitionNumber) {
//            partitionTable[i].isAllocated = 0;
//            return;
//        }
//    }
//}
//
//// 打印分区说明表
//void printPartitionTable() {
//    printf("分区号\t大小(KB)\t始址(KB)\t状态\n");
//    int i = 0;
//    for (i = 0; i < MAX_PARTITIONS; i++) {
//        printf("%d\t%d\t\t%d\t\t%s\n", partitionTable[i].partitionNumber,
//            partitionTable[i].size, partitionTable[i].startAddress,
//            partitionTable[i].isAllocated ? "已分配" : "未分配");
//    }
//}
//
//int main() {
//    initializePartitionTable();
//
//    printf("初始分区说明表:\n");
//    printPartitionTable();
//
//    int allocatedPartition = allocateMemory(30);
//    if (allocatedPartition != -1) {
//        printf("\n成功分配内存，分配的分区号为%d\n", allocatedPartition);
//    }
//    else {
//        printf("\n无可用分区进行内存分配\n");
//    }
//
//    printf("\n分配后的分区说明表:\n");
//    printPartitionTable();
//
//    deallocateMemory(1);
//    printf("\n回收分区1后的分区说明表:\n");
//    printPartitionTable();
//
//    return 0;
//}