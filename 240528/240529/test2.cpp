//#include <stdio.h>
//
//#define MAX_PARTITIONS 100
//
//// 定义分区结构体
//typedef struct {
//    int id;             // 分区号
//    int size;           // 分区大小（KB）
//    int start;          // 分区始址（KB）
//    int status;         // 分区状态，0-不可用，1-可???
//} Partition;
//
//Partition partitions[MAX_PARTITIONS];   // 空闲分区表
//int numPartitions = 0;                   // 当前空闲分区数量
//
//// 初始化空闲分区表
//void initialize() {
//    numPartitions = 5;
//
//    partitions[0].id = 1;
//    partitions[0].size = 64;
//    partitions[0].start = 44;
//    partitions[0].status = 1;
//
//    partitions[1].id = 2;
//    partitions[1].size = 24;
//    partitions[1].start = 132;
//    partitions[1].status = 1;
//
//    partitions[2].id = 3;
//    partitions[2].size = 40;
//    partitions[2].start = 210;
//    partitions[2].status = 1;
//
//    partitions[3].id = 4;
//    partitions[3].size = 30;
//    partitions[3].start = 270;
//    partitions[3].status = 1;
//
//}
//
//// 首次适应算法分配空闲分区
//int allocatePartition(int size) {
//    int i;
//    for (i = 0; i < numPartitions; i++) {
//        if (partitions[i].status == 1 && partitions[i].size >= size) {
//            partitions[i].status = 0;   // 将该分区状态设置为不可用
//            return partitions[i].start;
//        }
//    }
//    return -1;  // 分配失败，返回-1
//}
//
//// 回收分区
//void deallocatePartition(int start) {
//    int i;
//    for (i = 0; i < numPartitions; i++) {
//        if (partitions[i].start == start) {
//            partitions[i].status = 1;   // 将该分区状态设置为可用
//            return;
//        }
//    }
//}
//
//// 打印空闲分区表
//void printPartitionTable() {
//    int i;
//    printf("空闲分区表\n");
//    printf("序号\t分区大小（KB）\t分区始址（KB）\t状态\n");
//    for (i = 0; i < numPartitions; i++) {
//        printf("%d\t%d\t\t%d\t\t%s\n", partitions[i].id, partitions[i].size,
//            partitions[i].start, partitions[i].status == 1 ? "可用" : "不可用");
//    }
//}
//
//int main() {
//    initialize();
//
//    printPartitionTable();
//
//    int allocatedPartition = allocatePartition(20);
//    if (allocatedPartition != -1) {
//        printf("分配成功，分配的分区始址为：%d\n", allocatedPartition);
//    }
//    else {
//        printf("分配失败，无足够的空闲分区\n");
//    }
//
//    deallocatePartition(210);
//
//    printPartitionTable();
//
//    return 0;
//}