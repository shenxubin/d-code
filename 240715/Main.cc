#include "RingQueue.hpp"
#include "Thread.hpp"
#include "Task.hpp"
#include <string>
#include <vector>
#include <unistd.h>
#include <ctime>

// 我们需要的是向队列中投递任务
using namespace ThreadModule;
using ringQueue_t = RingQueue<Task>;

void Consumer(ringQueue_t &rq, std::string name)
{
    while (true)
    {
        sleep(2);
        // 1. 消费任务
        Task t;
        rq.PopQueue(&t);
        std::cout << "Consumer handler task: " << "[" << name << "]" << std::endl;
        // 2. 处理任务
        t();
    }
}

void Productor(ringQueue_t &rq, std::string name)
{
    srand(time(nullptr) ^ pthread_self());
    //int cnt = 10;
    while (true)
    {
        // 获取任务
        // 生产任务
        rq.EnQueue(Download);
        std::cout << "Productor : " << "[" << name << "]" << std::endl;
        // cnt--;
    }
}

void InitComm(std::vector<Thread<ringQueue_t>> *threads, int num, ringQueue_t &rq, func_t<ringQueue_t> func, const std::string &who)
{
    for (int i = 0; i < num; i++)
    {
        std::string name = "thread-" + std::to_string(i + 1) + "-" + who;
        threads->emplace_back(func, rq, name);
        // threads->back()->Start();
    }
}

void InitConsumer(std::vector<Thread<ringQueue_t>> *threads, int num, ringQueue_t &rq)
{
    InitComm(threads, num, rq, Consumer, "consumer");
}

void InitProductor(std::vector<Thread<ringQueue_t>> *threads, int num, ringQueue_t &rq)
{
    InitComm(threads, num, rq, Productor, "productor");
}

void WaitAllThread(std::vector<Thread<ringQueue_t>> &threads)
{
    for (auto &thread : threads)
    {
        thread.Join();
    }
}

    
void StartAll(std::vector<Thread<ringQueue_t>> &threads)
{
    for(auto &thread : threads)
    {
        std::cout << "start: " << thread.name() << std::endl;
        thread.Start();
    }
}

int main()
{
    ringQueue_t *rq = new ringQueue_t(10);
    std::vector<Thread<ringQueue_t>> threads;
    // std::vector<Thread<ThreadData>> threads;

    InitProductor(&threads, 1, *rq);
    InitConsumer(&threads, 1, *rq);

    StartAll(threads);

    WaitAllThread(threads);

    return 0;
}