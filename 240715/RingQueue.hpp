#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <semaphore.h>
#include <pthread.h>

// "321":
// 3: 三种关系
// a: 生产和消费互斥和同步
// b: 生产者之间:
// c: 消费者之间:
// 解决方案：加锁
// 1. 需要几把锁？2把

template<typename T>
class RingQueue
{
private:
    // 1. 环形队列
    std::vector<T> _ring_queue;
    int _cap; // 环形队列的容量上限

    // 2. 生产和消费的下标
    int _productor_step;
    int _consumer_step;

    // 3. 定义信号量
    sem_t _room_sem; // 生产者关心
    sem_t _data_sem; // 消费者关心

    // 4. 定义锁，维护多生产多消费之间的互斥关系
    pthread_mutex_t _productor_mutex;
    pthread_mutex_t _consumer_mutex;
private:
    void P(sem_t& sem)
    {
        sem_wait(&sem);
    }
    void V(sem_t& sem)
    {
        sem_post(&sem);
    }
public:
    RingQueue(int cap):_cap(cap),_productor_step(0),_consumer_step(0),_ring_queue(cap)
    {
        sem_init(&_room_sem, 0, cap);
        sem_init(&_data_sem, 0, 0);
        pthread_mutex_init(&_productor_mutex, nullptr);
        pthread_mutex_init(&_consumer_mutex, nullptr);
    }
    void EnQueue(const T& in)
    {
        P(_room_sem);
        pthread_mutex_lock(&_productor_mutex);
        _ring_queue[_productor_step++] = in;
        _productor_step %= _cap;
        pthread_mutex_unlock(&_productor_mutex);
        V(_data_sem);
    }
    void PopQueue(T* out)
    {
        P(_data_sem);
        pthread_mutex_lock(&_consumer_mutex);
        *out = _ring_queue[_consumer_step++];
        _consumer_step %= _cap;
        pthread_mutex_unlock(&_consumer_mutex);
        V(_room_sem);
    }
    ~RingQueue()
    {
        sem_destroy(&_room_sem);
        sem_destroy(&_data_sem);
        pthread_mutex_destroy(&_productor_mutex);
        pthread_mutex_destroy(&_consumer_mutex);
    }
};