#include "string.h"

void test1()
{
	sxb::string s1("xxxxx aaaxxxxx");
	sxb::string s(s1);
	//std::cin >> s;
	std::cout << s << std::endl;
}

void test2()
{
	sxb::string s;
	std::cin >> s;
	std::cout << s << std::endl;
}

void test3()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;
	std::cout << s << std::endl;
}

void test4()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;
	sxb::string::iterator it = s.begin();
	while (it != s.end())
	{
		std::cout << *it;
		it++;
	}
}

void test5()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;
	s.push_back('a');
	sxb::string::iterator it = s.begin();
	while (it != s.end())
	{
		std::cout << *it;
		it++;
	}
}

void test6()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;

	s.append("aaabbbbbbbbb");
	sxb::string::iterator it = s.begin();
	while (it != s.end())
	{
		std::cout << *it;
		it++;
	}
}

void test7()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;

	s += "999999999999999";
	sxb::string::iterator it = s.begin();
	while (it != s.end())
	{
		std::cout << *it;
		it++;
	}
}

void test8()
{
	sxb::string s1("xxxxx111111111");
	sxb::string s = s1;

	s.resize(30);
	sxb::string::iterator it = s.begin();
	while (it != s.end())
	{
		std::cout << *it;
		it++;
	}
}

void test9()
{
	sxb::string s1("xxxxx111111111");

	sxb::string s2("xxxxx111111111");

	bool ret = (s1 != s2);
	std::cout << ret << std::endl;
}

void test10()
{
	sxb::string s1("xxxxx111111111");

	size_t pos = s1.find('2');
	std::cout << pos << std::endl;
}

void test11()
{
	sxb::string s1("xxxxx111111111");
	size_t pos = s1.find('1112');
	std::cout << pos << std::endl;
}

void test12()
{
	sxb::string s1("xxxxx111111111");

	s1.insert(5, 'a');
	std::cout << s1 << std::endl;
}

void test13()
{
	sxb::string s1("xxxxx111111111");

	s1.insert(5, "aaaaaaaaa");
	std::cout << s1 << std::endl;
}

void test14()
{
	sxb::string s1("1234567890");

	s1.erase(5, 2);
	std::cout << s1 << std::endl;
}

int main()
{
	test14();
	return 0;
}