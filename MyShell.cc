#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <vector>
#include "namePipe.hpp"
#include "testPipe.hpp"

#define SIZE 1024
#define MAX_ARGV_SIZE 32
#define NONE_REDIR 0
#define STDIN_REDIR 1
#define STDOUT_REDIR 2

#define MAXLINE 4096
#define MAXPIPE 16
#define MAXARG 8

char command[SIZE];
char *argv[MAX_ARGV_SIZE];
int redirType = NONE_REDIR;
char *filename = nullptr;
int pipe_num = 0;

char *buffer[10][10] = {0};
const char *filename2[] = {"./file1.txt", "./file2.txt", "./file3.txt", "./file3.txt", "./file4.txt", "./file5.txt"};
pid_t child_process[10];

const char *getEnvVa(char *env_name)
{
    if (strcmp(env_name, "HOSTNAME") == 0)
        return getenv("HOSTNAME");
    else if (strcmp(env_name, "USER") == 0)
        return getenv("USER");
    else if (strcmp(env_name, "PWD") == 0)
        return getenv("PWD");
    else if (strcmp(env_name, "HOME") == 0)
        return getenv("HOME");
}

void printCommandLine()
{
    char host_name[1024];
    char pwd[1024];
    gethostname(host_name, 1024);
    getcwd(pwd, 1024);
    printf("[%s@%s:%s]$ ", getlogin(), host_name, pwd);
}

size_t inputCommand(char command[], size_t size)
{
    fgets(command, SIZE, stdin);
    command[strlen(command) - 1] = '\0';
    size_t len = strlen(command);
    return len;
}

void splitCommand(char command[], char *argv[])
{
    int i = 0;
    argv[i++] = strtok(command, " ");
    while (argv[i++] = strtok(NULL, " "))
    {
    }
}
// export PATH=$PATH:/home/sxb/240610 path gpath=$gpath:/home/sxb/240610
int dealCdExitPathCommand(char *argv[])
{
    int res = 0;
    if (strcmp(argv[0], "cd") == 0)
    {
        res = 1;
        const char *target = argv[1];
        if (!target)
            target = getEnvVa((char *)"HOME");
        chdir(target);
    }
    else if (strcmp(argv[0], "path") == 0)
    {
        res = 1;
        char *path;
        path = getenv("gpath");
        char new_path[2048];
        char buffer[1024] = {0};
        for (int i = 0; i < strlen(argv[1]) - 12; i++)
            buffer[i] = argv[1][13 + i];
        snprintf(new_path, sizeof(new_path), "%s:%s", path, buffer);
        setenv("gpath", new_path, 1);
        setenv("PATH", getenv("gpath"), 1);
    }
    else if (strcmp(argv[0], "exit") == 0)
    {
        exit(0);
    }
    return res;
}

void checkRedir(char command[])
{
    // ls -l > log.txt
    redirType = NONE_REDIR;
    filename = nullptr;
    int pos = 0;
    while (pos < strlen(command))
    {
        if (command[pos] == '>')
        {
            redirType = STDOUT_REDIR;
            command[pos] = '\0';
            while (command[++pos] == ' ')
            {
            }
            filename = command + pos;
            break;
        }
        else if (command[pos] == '<')
        {
            redirType = STDIN_REDIR;
            command[pos] = '\0';
            while (command[++pos] == ' ')
            {
            }
            filename = command + pos;
            break;
        }
        else
            pos++;
    }
}

void checkPipe(char command[] /*, char *argv[]*/)
{
    pipe_num = 0;
    int pos = 0;
    while (pos < strlen(command))
    {
        if (command[pos] == '|')
        {
            pipe_num++;
        }
        pos++;
    }
}

void deal_Cd_Exit_Path_And_Other(char *command)
{
    // 检查是否有输入输出重定向
    checkRedir(command);
    // 切割命令

    splitCommand(command, argv);

    // 处理cd、exit和path命令
    int n = dealCdExitPathCommand(argv);
    if (n == 1)
        return;

    // 执行普通命令&&重定向输入输出
    pid_t id = fork();
    if (id == 0)
    {
        int fd = -1;
        if (redirType == STDIN_REDIR)
        {
            fd = open(filename, O_RDONLY);
            dup2(fd, STDIN_FILENO);
        }
        else if (redirType == STDOUT_REDIR)
        {
            fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0666);
            dup2(fd, STDOUT_FILENO);
        }
        execvp(argv[0], argv);
        exit(0);
    }
    pid_t rid = waitpid(id, nullptr, 0);
}

void dealBackgroundprocess(char command2[])
{
    if (command2[strlen(command2) - 1] == '&')
    {
        splitCommand(command, argv);
        pid_t id = fork();
        if (id == 0)
        {
            execvp(argv[0], argv);
            exit(-1);
        }
        else
        {
            printf("父进程退出！\n");
        }
    }
}

int main()
{
    int x = 0;
    char buf[MAXLINE];
    pid_t pid;
    char command2[1024];
    while (true)
    {
        // 打印命令行
        printCommandLine();
        pipe_num = 0;
        // 设置一个新的环境变量gpath,初始化为PATH环境变量中的内容
        if (x == 0)
        {
            setenv("gpath", getenv("PATH"), 1);
            x = 1;
        }

        // 输入命令
        int n = inputCommand(command, SIZE);
        // 处理空串
        if (n == 0)
            continue;

        snprintf(command2, sizeof(command2), "%s", command);
        dealBackgroundprocess(command2);
        if (command2[strlen(command2) - 1] == '&')
            continue;

        // 检查需要几个管道
        vector<int> pos(1, -1);
        for (int i = 0; i < strlen(command); i++)
        {
            if (command[i] == '|')
            {
                pipe_num++;
                command[i] = '\0'; // 直接截断
                pos.push_back(i);
            }
        }
        if (pipe_num == 0)
        {
            deal_Cd_Exit_Path_And_Other(command);
            continue;
        }

        vector<int> child_id;
        vector<pair<int, int>> v;          // v[i].first:i号管道的读 second:写
        for (int i = 0; i < pipe_num; i++) // 创建sz个进程,sz-1个管道
        {
            int pipefd[2];
            int n = pipe(pipefd);
            pid_t id = fork();
            if (id == 0)
            {
                close(pipefd[0]); // 关闭当前管道的读
                int prev_fd_sz = v.size();
                for (int j = 0; j < prev_fd_sz; j++)
                {
                    if (j < prev_fd_sz - 1)
                    {
                        // 把前面打开的读都关上(除了最近的那一个管道)
                        close(v[j].first);
                    }
                    // 把前面打开的管道的写都关上
                    close(v[j].second);
                }
                dup2(pipefd[1], 1); // 当前管道的写重定向到我的1
                if (!v.empty())
                    dup2(v.back().first, 0); // 上一个管道的读重定向到我的0
                deal_Cd_Exit_Path_And_Other(command + pos[i] + 1);
                exit(0);
            }
            else
            {
                v.push_back({pipefd[0], pipefd[1]});
                child_id.push_back(id);
            }
        }
        int sz = pos.size();
        // 最后一个进程:
        pid_t id = fork();
        if (id == 0)
        {
            int prev_fd_sz = v.size();
            for (int j = 0; j < prev_fd_sz; j++)
            {
                if (j < prev_fd_sz - 1)
                {
                    // 把前面打开的读都关上(除了最近的那一个管道)
                    close(v[j].first);
                }
                // 把前面打开的管道的写都关上
                close(v[j].second);
            }
            dup2(v.back().first, 0);
            deal_Cd_Exit_Path_And_Other(command + pos[sz - 1] + 1);
            exit(0);
        }
        child_id.push_back(id);
        for (auto &e : v)
        {
            // 最后的时候父亲关闭所有的读写端
            close(e.first);
            close(e.second);
        }
        for (auto &e : child_id)
        {
            pid_t rid = waitpid(e, nullptr, 0);
            // if (rid > 0)
            //     cout << "wait success, pid: " << rid << endl;
        }
    }
    return 0;
}
