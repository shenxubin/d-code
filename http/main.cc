#include <iostream>
#include "http.hpp"
#include "TcpServer.hpp"
void Usage(std::string proc)
{
    std::cout << "Usage:\n\t" << proc << " local_port\n" << std::endl;
}
int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        return 1;
    }
    uint16_t port = std::stoi(argv[1]);
    HttpServer httpServer;
    TcpServer tcpserver(port, std::bind(&HttpServer::HandlerHttpRequest, &httpServer, std::placeholders::_1));
    tcpserver.Loop();
    return 0;
}