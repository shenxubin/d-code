#pragma once

#include <iostream>
#include <vector>
#include <map>

using namespace std;

namespace matrix {
	template<class V, class W, W MAX_W = INT_MAX, bool Direction = false>
	class Graph
	{
	private:
		vector<V> _vertexs;   //顶点集合
		map<V, int> _indexMap;//顶点映射下标
		vector<vector<W>> _matrix;//领接矩阵

	public:
		//图的创建
		Graph(const V* a, size_t n)
		{
			_vertexs.reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				_vertexs.push_back(a[i]);
				_indexMap[a[i]] = i;
			}

			_matrix.resize(n);
			for (size_t i = 0; i < _matrix.size(); i++)
			{
				_matrix[i].resize(n, MAX_W);
			}
		}

		size_t GetVertexIndex(const V& v)
		{
			auto it = _indexMap.find(v);
			if (it != _indexMap.end())
			{
				return it->second;
			}
			else
			{
				return -1;
			}
		}

		void AddEdge(const V& src, const V& dst, const W& w)
		{
			size_t srci = GetVertexIndex(src);
			size_t dsti = GetVertexIndex(dst);

			_matrix[srci][dsti] = w;

			if (Direction == false)
			{
				_matrix[dsti][srci] = w;
			}
		}

		void Print()
		{
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << "[" << i << "]" << "->" << _vertexs[i] << endl;
			}
			cout << endl;

			//打印矩阵
			//打印下标
			cout << "  ";
			for (size_t i = 0; i < _vertexs.size(); i++)
			{
				cout << _vertexs[i] << " ";
			}
			cout << endl;

			for (size_t i = 0; i < _matrix.size(); i++)
			{
				cout << _vertexs[i] << " ";
				for (size_t j = 0; j < _matrix[i].size(); j++)
				{
					if (_matrix[i][j] == MAX_W)
					{
						cout << "*" << " ";
					}
					else
					{
						cout << _matrix[i][j] << " ";
					}
				}
				cout << endl;
			}
			cout << endl;
		}
	};
}