#include <iostream>
#include <vector>
#include "LockGuard.hpp"
#include "Thread.hpp"
using namespace ThreadModule;

int g_tickets = 10000;
const int num = 4;

class ThreadData
{
public:
    int &_tickets; // 所有的线程，最后都会引用同一个全局的g_tickets
    std::string _name;
    int _total;
    pthread_mutex_t &_mutex;

public:
    ThreadData(int &tickets, const std::string &name, pthread_mutex_t &mutex)
        : _tickets(tickets), _name(name), _total(0), _mutex(mutex)
    {
    }
    ~ThreadData()
    {
    }
};

void route(ThreadData *td)
{
    while (true)
    {
        LockGuard guard(&td->_mutex);
        if (td->_tickets > 0)
        {
            usleep(1000);
            printf("%s running, get tickets: %d\n", td->_name.c_str(), td->_tickets); 
            td->_tickets--;                                                          
            td->_total++;
        }
        else
            break;
    }
}



int main()
{
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex,nullptr);

    std::vector<Thread<ThreadData*>> threads;
    std::vector<ThreadData *> datas;

    //1、创建一批线程
    for(int i = 0; i<num; i++)
    {
        std::string name = "thread-" + std::to_string(i+1);
        ThreadData* td = new ThreadData(g_tickets, name, mutex);
        threads.emplace_back(route, td, name);
        datas.emplace_back(td);
    }

    // 2. 启动 一批线程
    for (auto &thread : threads)
    {
        thread.Start();
    }

    // 3. 等待一批线程
    for (auto &thread : threads)
    {
        thread.Join();
    }
    sleep(1);
    // 4. 输出统计数据
    for (auto data : datas)
    {
        std::cout << data->_name << " : " << data->_total << std::endl;
        delete data;
    }

    pthread_mutex_destroy(&mutex);
    
    return 0;
}