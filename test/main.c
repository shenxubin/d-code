#include "processbar.h"
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#define FILESIZE 1024*1024*1024

void download(callback_t cd) // 回调函数
{
    srand(time(NULL));
    int total = FILESIZE;

    while(total)
    {
        usleep(10000);//下载
        int one = rand() % (1024*1024*10);
        total -= one;
        if(total < 0)
        {
            total = 0;
        }

        //当前进度
        int download = FILESIZE - total;
        double rate = (download*1.0/(FILESIZE))*100.0;
        cd(rate);
    }
}

int main()
{
    download(process_flush);
    return 0;
}
