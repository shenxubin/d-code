class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int cur = 0;
        int prev = 0;
        while (cur < nums.size())
        {
            if (nums[prev] != 0)
            {
                prev++;
            }
            else if (nums[prev] == 0 && nums[cur] != 0)
            {
                swap(nums[prev], nums[cur]);
                prev++;
            }
            cur++;
        }
    }
};