class Solution {
public:
    bool isHappy(int n) {
        int x = n;
        int sum = 10;
        while (sum >= 10)
        {
            sum = 0;
            while (x > 0)
            {
                sum += pow(x % 10, 2);
                x /= 10;
            }

            x = sum;
        }
        if (sum == 1 || sum == 7)
            return true;
        else
            return false;
    }
};