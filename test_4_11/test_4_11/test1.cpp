#include <iostream>
using namespace std;

//类域影响访问，局部域会 影响生命周期
//不同的域可以定义同名的函数
//

int a = 10; //定义
class Person
{
public:
	void PrintPersonInfo();
private:
	char _name[20];  //声明 //针对变量，开辟空间就是定义，不开辟空间就是声明
	char _gender[5];
	int _age;
};
//类跟结构体一样，是不占用空间的，只有实例化以后才会占用空间
void Person::PrintPersonInfo() 
{
	cout << _name << endl;
}

//实例化的每个A对象成员都是独立空间，是不同变量，但是每个A对象，调用Print成员函数都是同一个
//类中只保存成员变量，成员函数存放在公共的代码段

class A
{
public:
	void PrintA()
	{
		cout << _a << endl;
	}
	void func()
	{
		cout << "hehe" << endl;
	}
	int _a;
	int i;
};
int main()
{
	//A aa1;
	//A aa2;
	//aa1._a = 1;
	//aa2._a = 2;
	//aa1.PrintA();
	//aa2.PrintA();
	//A* ptr = nullptr;
	//ptr->func();
	cout << sizeof(A) << endl;
	return 0;
}