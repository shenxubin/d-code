///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     ListNode *next;
// *     ListNode() : val(0), next(nullptr) {}
// *     ListNode(int x) : val(x), next(nullptr) {}
// *     ListNode(int x, ListNode *next) : val(x), next(next) {}
// * };
// */
//class Solution {
//public:
//    ListNode* cur = nullptr;
//    ListNode* initial = nullptr;
//    ListNode* mergeTwoLists(ListNode* list1, ListNode* list2)
//    {
//        if (list1 == nullptr)
//        {
//            if (cur == nullptr)
//            {
//                cur = list2;
//                return cur;
//            }
//
//            else
//                cur->next = list2;
//            return initial;
//        }
//        if (list2 == nullptr)
//        {
//            if (cur == nullptr)
//            {
//                cur = list1;
//                return cur;
//            }
//            else
//                cur->next = list1;
//            return initial;
//        }
//        if (list1->val < list2->val)
//        {
//            if (cur == nullptr)
//            {
//                cur = list1;
//                initial = cur;
//            }
//            else
//            {
//                cur->next = list1;
//                cur = cur->next;
//            }
//            return mergeTwoLists(list1->next, list2);
//        }
//        else
//        {
//
//            if (cur == nullptr)
//            {
//                cur = list2;
//                initial = cur;
//            }
//            else
//            {
//                cur->next = list2;
//                cur = cur->next;
//            }
//            return mergeTwoLists(list1, list2->next);
//        }
//    }
//};