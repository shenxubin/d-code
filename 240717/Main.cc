#include "ThreadPool.hpp"
#include "Task.hpp"
#include "Log.hpp"
#include <iostream>
#include <string>
#include <memory>
#include <ctime>

int main()
{
    // 获取单例的可能是多线程啊！！！
    // 程序已经加载
    LOG(DEBUG, "程序已经加载");
    sleep(3);
    ThreadPool<Task>::GetInstance();
    sleep(2);

    ThreadPool<Task>::GetInstance();
    sleep(2);

    ThreadPool<Task>::GetInstance();
    sleep(2);

    ThreadPool<Task>::GetInstance();
    sleep(2);

    ThreadPool<Task>::GetInstance()->Wait();
    sleep(2);

    return 0;
}