#include <stdio.h>  
#include <time.h>  
  
int main() {  
    time_t rawtime;  
    struct tm * timeinfo;  
  
    time(&rawtime); // 获取当前时间的时间戳  
    timeinfo = localtime(&rawtime); // 将时间戳转换为本地时间  
  
    // 使用timeinfo结构体中的各个字段来打印本地时间  
    printf("当前本地时间和日期: %d-%02d-%02d %02d:%02d:%02d\n",  
           timeinfo->tm_year + 1900, // 年份  
           timeinfo->tm_mon + 1,     // 月份  
           timeinfo->tm_mday,        // 日期  
           timeinfo->tm_hour,        // 小时  
           timeinfo->tm_min,         // 分钟  
           timeinfo->tm_sec);         // 秒  
  
    return 0;  
}