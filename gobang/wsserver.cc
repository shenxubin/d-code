#include <iostream>
#include <string>
#include <websocketpp/server.hpp>
#include <websocketpp/config/asio_no_tls.hpp>
#include "util.hpp"

void print(const std::string& str)
{
    std::cout << str <<std::endl;
}

typedef websocketpp::server<websocketpp::config::asio> wssvr_t;
void http_callback(wssvr_t* svr, websocketpp::connection_hdl hdl)
{

    std::string filename = "register.html";
    std::string pathname = "./wwwroot/webroot/" + filename;
    std::string body;
    file_util::read(pathname, body);
    //获取通信连接对象
    wssvr_t::connection_ptr con = svr->get_con_from_hdl(hdl);
    std::cout << "body: " << con->get_request_body() << std::endl;
    websocketpp::http::parser::request req = con->get_request();
    std::cout << "method: " << req.get_method() << std::endl;
    std::cout << "uri: " << req.get_uri() << std::endl;

    //std::string body = "<html><body><h1>Hello World</h1></body></html>";
    con->set_body(body);
    con->append_header("Content-Type", "text/html");
    con->set_status(websocketpp::http::status_code::value::ok);
    svr->set_timer(5000, std::bind(print, "hello webscket"));
}
void wsopen_callback(wssvr_t* svr, websocketpp::connection_hdl hdl)
{
    std::cout << "websocket握手成功！" << std::endl;
}
void wsclose_callback(wssvr_t* svr, websocketpp::connection_hdl hdl)
{
    std::cout << "websocket断开连接成功！" << std::endl;

}
void wsmsg_callback(wssvr_t* svr, websocketpp::connection_hdl hdl, wssvr_t::message_ptr msg)
{
    wssvr_t::connection_ptr con = svr->get_con_from_hdl(hdl);
    std::cout << "wsmsg: " << msg->get_payload() << std::endl;
    std::string resp = "client say: " + msg->get_payload() ;
    con->send(resp, websocketpp::frame::opcode::text);
}

int main()
{
    //1.实例化server对象
    wssvr_t wssvr;
    //2.设置日志等级
    wssvr.set_access_channels(websocketpp::log::alevel::none);//不需要输出日志
    //3.初始化asio调度器
    wssvr.init_asio();
    wssvr.set_reuse_addr(true);//设置地址复用
    //4.设置回调函数
    wssvr.set_http_handler(std::bind(http_callback, &wssvr, std::placeholders::_1));
    wssvr.set_open_handler(std::bind(wsopen_callback, &wssvr, std::placeholders::_1));
    wssvr.set_close_handler(std::bind(wsclose_callback, &wssvr, std::placeholders::_1));
    wssvr.set_message_handler(std::bind(wsmsg_callback, &wssvr, std::placeholders::_1, std::placeholders::_2));

    //5.设置监听窗口
    wssvr.listen(3389);
    //6.开始获取新链接
    wssvr.start_accept();
    //7.服务器开始运行
    wssvr.run();
    return 0;
}