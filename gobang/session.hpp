#pragma once
#include <iostream>
#include "online.hpp"
#include "Log.hpp"

class session
{
private:
    uint64_t _ssid;//标识符
    uint64_t _uid;//session对应的用户ID
    int _statu;//用户状态,0表示未登录，1表示已登录
    websocket_server::timer_ptr _tp;//session关联的定时器
public:
    session(uint64_t ssid):_ssid(ssid)
    {
        LOG(INFO, "session %lu create successfully!\n", _ssid);
    }
    ~session()
    {}
    void setUser(uint64_t uid)
    {
        _uid = uid;
    }
    void setStatu(int statu)
    {
        _statu = statu;
    }
    uint64_t getUser()
    {
        return _uid;
    }
    uint64_t getSsid()
    {
        return _ssid;
    }
    bool isLogin()
    {
        return _statu == 1;
    }
    void setTimer(const websocket_server::timer_ptr& tp)
    {
        _tp = tp;
    }
    websocket_server::timer_ptr& getTimer()
    {
        return _tp;
    }
};
using session_ptr = std::shared_ptr<session>;
#define SESSION_PERMANENT -1
#define SESSION_TIMEOUT 30000
class session_manager
{
private:
    uint64_t _next_ssid;
    std::mutex _mutex;
    std::unordered_map<std::uint64_t, session_ptr> _session;
    websocket_server* _server;
public:
    session_manager(websocket_server* srv):_next_ssid(1),_server(srv)
    {
        LOG(INFO, "session manager %lu create successfully!\n", _next_ssid);
    }
    ~session_manager()
    {}
    session_ptr createSession(uint64_t uid, int statu)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        session_ptr sp(new session(_next_ssid));
        sp->setStatu(statu);
        _session.insert(std::make_pair(_next_ssid, sp));
        _next_ssid++;
        return sp;
    }
    void append_session(const session_ptr& ssp)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _session.insert(std::make_pair(ssp->getSsid(), ssp));
    }
    session_ptr getSessionBySsid(uint64_t ssid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _session.find(ssid);
        if(it == _session.end())
            return session_ptr();
        return it->second;
    }
    void setSessionExpireTime(uint64_t ssid, int ms)
    {
        //登录之后创建session,session需要在指定时间无通信后是删除
        //但是进入游戏大厅或者是游戏房间这个session就应该永久存在
        //等到退出游戏大厅或者是游戏房间这个session应该被重新置为临时，在长时间无通信后被删除
        session_ptr ssp = getSessionBySsid(ssid);
        if(ssp.get() == nullptr)
            return ;
        websocket_server::timer_ptr tp = ssp->getTimer();
        //1.session永久存在，设置永久
        if(tp.get() == nullptr && ms == SESSION_PERMANENT)
        {
            return ;
        }
        //2.在session永久存在的时候下，设置定时时间之后被删除的定时任务
        else if(tp.get() == nullptr && ms != SESSION_PERMANENT)
        {
            websocket_server::timer_ptr tmp_tp = 
            _server->set_timer(ms, std::bind(&session_manager::removeSession,this, ssid));
            ssp->setTimer(tmp_tp);
        }
        //3.在session设置了定时删除的时候下，将session设置为永久
        else if(tp.get() != nullptr && ms == SESSION_PERMANENT)
        {
            tp->cancel();//定时任务不是立即取消的，定时任务会立即执行
            ssp->setTimer(websocket_server::timer_ptr());//定时器置空
            //添加时需要使用定时器而不是立即添加
            _server->set_timer(0, std::bind(&session_manager::append_session, this, ssp));
        }
        else if(tp.get() != nullptr && ms != SESSION_PERMANENT)
        {
            tp->cancel();//定时任务不是立即取消的，定时任务会立即执行
            ssp->setTimer(websocket_server::timer_ptr());//定时器置空
            //添加时需要使用定时器而不是立即添加
            websocket_server::timer_ptr tmp_tp = 
            _server->set_timer(ms, std::bind(&session_manager::removeSession,this, ssid));
            //重新设置session关联的定时器
            ssp->setTimer(tmp_tp);
        }
    }
    bool removeSession(uint64_t ssid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _session.find(ssid);
        if(it == _session.end())
        {
            LOG(WARNING, "session %lu dont exists, remove fail!", ssid);
            return false;
        }
        _session.erase(ssid);
        return true;
    }
};