#include <iostream>
#include <sstream>
#include <string>
#include <jsoncpp/json/json.h>
#include <memory>
#include <vector>

std::string Serialize()
{
    Json::Value root;
    root["name"] = "小明";
    root["age"] = 20;
    root["score"].append(89);
    root["score"].append(90);

    Json::StreamWriterBuilder swb;

    // 通过工厂生产一个streamwriter类
    Json::StreamWriter *sw = swb.newStreamWriter();
    std::stringstream ss;
    int res = sw->write(root, &ss);
    if (res)
        std::cerr << "serialize fail!" << std::endl;
    delete sw;
    return ss.str();
}

void Deserialize(const std::string& str)
{
    Json::CharReaderBuilder crb;
    // Json::CharReader* cr = crb.newCharReader();
    std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
    Json::Value root;
    std::string cerr;
    bool res = cr->parse(str.c_str(), str.c_str()+str.size(), &root, &cerr);
    if(!res)
        return ;
    std::cout << root["name"].asString() << std::endl;
    std::cout << root["age"].asInt() << std::endl;
    int sz = root["score"].size();
    for(int i = 0; i<sz; i++)
    {
        std::cout << "成绩: " << root["score"][i] << std::endl;
    }
    //delete cr;
}
int main()
{
    std::string str = Serialize();
    Deserialize(str);
    return 0;
}