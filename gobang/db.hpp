#pragma once
#include <mutex>
#include <assert.h>
#include "util.hpp"
#include "LockGuard.hpp"
class user_table
{
private:
    MYSQL *_mysql;     // mysql操作句柄
    std::mutex _mutex; // 锁保证线程安全
public:
    user_table(const std::string &host,
               const std::string &user,
               const std::string &pass,
               const std::string &dbname,
               uint16_t port = 3306)
    {
        pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
        _mysql = mysql_util::mysql_create(host, user, pass, dbname, port);
        assert(_mysql != NULL);
    }
    ~user_table()
    {
        mysql_util::mysql_destroy(_mysql);
        _mysql = NULL;
    }
    bool insert(Json::Value &user) // 注册时新增用户
    {
        Json::Value val;
        if(user["username"].isNull() || user["pass"].isNull())
        {
            LOG(ERROR, "please input username or password!\n");
            return false;
        }
        bool ret = select_by_name(user["username"].asCString(), val);
        if (ret)
        {
            LOG(DEBUG, "user %s exists!, insert failed\n", user["username"].asCString());
            return false;
        }
        {
            LockGuard lockGuard(&lock);
            char sql[1024] = {0};
            #define INSERT_T "insert into user values(null, '%s',hex(aes_encrypt(('%s'),'112233')), 1000, 0, 0);"
            sprintf(sql, INSERT_T,
                    user["username"].asCString(), user["pass"].asCString());
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "insert user failed\n");
            return false;
        }
        return true;
    }
    bool login(Json::Value &user) // 登录验证，并返回用户的详细信息
    {
// 以用户名和密码作为查询过滤条件
#define LOGIN_USER "select id, score, total_count, win_count from user \
        where username = '%s' and pass = hex(aes_encrypt(('%s'),'112233'))"
        char sql[1024];
        sprintf(sql, LOGIN_USER, user["username"].asCString(), user["pass"].asCString());
        bool ret = false;
        {
            LockGuard lockGuard(&lock);
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "user find error\n");
            return false;
        }
        // 如果有数据，只能有一条数据
        MYSQL_RES *res = mysql_store_result(_mysql);
        if (!res)
        {
            LOG(DEBUG, "login error\n");
            return false;
        }
        int row_num = mysql_num_rows(res);
        if (row_num == 0)
        {
            LOG(DEBUG, "user %s dont exists\n", user["username"].asCString());
            return false;
        }
        if (row_num != 1)
        {
            LOG(ERROR, "the user info queried is not unique");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["id"] = std::stoi(row[0]);
        user["score"] = std::stoi(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true; // 用户登录成功
    }
    bool select_by_name(const std::string &name, Json::Value &user) // 通过用户名获取用户信息
    {
#define FIND_BY_NAME "select id, score, total_count, win_count from user \
        where username = '%s'"
        char sql[1024];
        sprintf(sql, FIND_BY_NAME, name.c_str());
        bool ret = false;
        {
            LockGuard lockGuard(&lock);
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "user find error\n");
            return false;
        }
        // 如果有数据，只能有一条数据
        MYSQL_RES *res = mysql_store_result(_mysql);
        if (res == NULL)
        {
            LOG(DEBUG, "select error\n");
            return false;
        }
        int row_num = mysql_num_rows(res);
        if (row_num == 0)
            return false;
        if (row_num != 1)
        {
            LOG(ERROR, "the user info queried is not unique\n");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["id"] = std::stoi(row[0]);
        user["score"] = std::stoi(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true;
    }
    bool select_by_id(const uint64_t &id, Json::Value &user) // 通过id获取用户信息
    {
#define FIND_BY_ID "select username, score, total_count, win_count from user \
        where id = '%lu'"
        char sql[1024];
        sprintf(sql, FIND_BY_ID, id);
        bool ret = false;
        {
            LockGuard lockGuard(&lock);
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "user find error\n");
            return false;
        }
        // 如果有数据，只能有一条数据
        MYSQL_RES *res = mysql_store_result(_mysql);
        if (!res)
        {
            LOG(DEBUG, "select_by_id error\n");
            return false;
        }
        int row_num = mysql_num_rows(res);
        if (row_num == 0)
        {
            LOG(DEBUG, "id %d user dont exists\n", id);
            return false;
        }
        if (row_num != 1)
        {
            LOG(ERROR, "the user info queried is not unique");
            return false;
        }
        MYSQL_ROW row = mysql_fetch_row(res);
        user["username"] = row[0];
        user["score"] = std::stoi(row[1]);
        user["total_count"] = std::stoi(row[2]);
        user["win_count"] = std::stoi(row[3]);
        mysql_free_result(res);
        return true;
    }
    bool win(const uint64_t &id) // 胜利，天梯分数增加3分，战斗次数增加，胜利次数增加
    {
#define UPDATE_BY_ID_WIN "update user set score=score+3, total_count=total_count\
        +1, win_count=win_count+1 where id=%lu"
        char sql[1024];
        sprintf(sql, UPDATE_BY_ID_WIN, id);

        bool ret = false;
        {
            LockGuard lockGuard(&lock);
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "user data change failed when user win\n");
            return false;
        }
        return true;
    }
    bool lose(const uint64_t &id) // 失败，天梯分数减少3分，战斗次数增加
    {
#define UPDATE_BY_ID_LOSE "update user set score=score-3, total_count=total_count\
        +1 where id=%lu"
        char sql[1024];
        sprintf(sql, UPDATE_BY_ID_LOSE, id);
        bool ret = false;
        {
            LockGuard lockGuard(&lock);
            ret = mysql_util::mysql_exec(_mysql, sql);
        }
        if (!ret)
        {
            LOG(ERROR, "user data change failed when user lose\n");
            return false;
        }
        return true;
    }
};