#include "server.hpp"
#define HOST "127.0.0.1"
#define PORT 3306
#define USER "root"
#define PASS ""
#define DBNAME "gobang"

int mysql_util_test()
{
    MYSQL *mysql = mysql_util::mysql_create(HOST, USER, PASS, DBNAME, PORT);
    if (mysql == NULL)
        return -1;
    const char *sql = "insert stu values(90, '小明', 18, 66, 77, 99);";
    mysql_util::mysql_exec(mysql, sql);
    mysql_util::mysql_destroy(mysql);
    return 0;
}

void json_util_test()
{
    Json::Value root;
    root["name"] = "小明";
    root["age"] = 20;
    root["score"].append(89);
    root["score"].append(90);
    std::string str;
    json_util::json_seriliaze(root, str);
    std::cout << str << std::endl;

    Json::Value root2;
    json_util::json_deseriliaze(root2, str);

    std::cout << root2["name"].asString() << std::endl;
    std::cout << root2["age"].asInt() << std::endl;
    int sz = root2["score"].size();
    for(int i = 0; i<sz; i++)
    {
        std::cout << "成绩: " << root2["score"][i] << std::endl;
    }
}

void string_util_test()
{
    std::string str = "123,3333333333,44344";
    std::string sep = ",";
    std::vector<std::string> vect;
    string_util::split(str, sep, vect);
    for(size_t i = 0; i<vect.size(); i++)
    {
        std::cout << vect[i] << std::endl;
    } 
}

void file_util_test()
{
    std::string filename = "./makefile";
    std::string str;
    file_util::read(filename, str);
    std::cout << str << std::endl;
}

void db_test()
{
    user_table u1(HOST, USER, PASS, DBNAME, PORT);
    Json::Value root;
    root["username"] = "沈旭彬3";
    //root["pass"] = "hhggjj";
    u1.insert(root);
    // bool ret = u1.login(root);
    // if(ret)
    // {
    //     std::cout << root["id"] << std::endl;
    //     std::cout << root["score"] << std::endl;
    //     std::cout << root["total_count"] << std::endl;
    //     std::cout << root["win_count"] << std::endl;
    // }
    // uint64_t id = 10;
    // bool ret = u1.lose(id);
    // if(ret)
    // {
    //     std::cout << root["username"] << std::endl;
    //     std::cout << root["score"] << std::endl;
    //     std::cout << root["total_count"] << std::endl;
    //     std::cout << root["win_count"] << std::endl;
    // }
}

void online_user_test()
{
    online_manager om;
    websocket_server::connection_ptr conn;
    uint64_t uid = 2;
    om.enter_game_room(uid, conn);
    if(om.is_in_game_room(uid))
        LOG(INFO, "user %d in game hall\n", uid);
    else
        LOG(INFO, "user %d not in game hall\n", uid);
    om.exit_game_room(uid);
    if(om.is_in_game_room(uid))
        LOG(INFO, "user %d in game hall\n", uid);
    else
        LOG(INFO, "user %d not in game hall\n", uid);
}

void room_test()
{
    user_table u1(HOST, USER, PASS, DBNAME, PORT);
    online_manager om;
    room r1(10, &u1, &om);
}
void room_manager_test()
{
    user_table u1(HOST, USER, PASS, DBNAME, PORT);
    online_manager om;
    room_manager rm1(&u1, &om);
}

void session_test()
{
    session s1(1);
}
void matcher_test()
{
    user_table u1(HOST, USER, PASS, DBNAME, PORT);
    online_manager om;
    room_manager rm1(&u1, &om);
    matcher mq1(&rm1, &u1, &om);
}
void gobang_server_test()
{
    gobang_server gs1(HOST, USER, PASS, DBNAME, PORT);
    gs1.start(3389);
}
int main()
{
    //json_util_test();
    //string_util_test();
    //file_util_test(); 
    gobang_server_test();
    return 0;
}