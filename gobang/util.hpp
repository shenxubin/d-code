#pragma once
#include "Log.hpp"
#include <iostream>
#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <mysql/mysql.h>
#include <jsoncpp/json/json.h>
#include <fstream>
class mysql_util
{
public:
    static MYSQL *mysql_create(const std::string &host,
                               const std::string &user,
                               const std::string &pass,
                               const std::string &dbname,
                               uint16_t port = 3306)
    {
        // 1.初始化mysql句柄
        MYSQL *mysql = mysql_init(NULL);
        if (mysql == NULL)
        {
            LOG(ERROR, "mysql init failed!\n");
            return NULL;
        }
        // 2.连接服务器
        if (mysql_real_connect(mysql, "localhost", user.c_str(), pass.c_str(), dbname.c_str(), port, NULL, 0) == NULL)
        {
            LOG(ERROR, "connect mysql server failed : %s\n", mysql_error(mysql));
            return NULL;
        }
        // 3.设置客户端字符集
        if (mysql_set_character_set(mysql, "utf8") != 0)
        {
            LOG(ERROR, "set client charater failed : %s\n", mysql_error(mysql));
            mysql_close(mysql);
            return NULL;
        }
        return mysql;
    }
    static bool mysql_exec(MYSQL *mysql, const std::string &sql)
    {
        int ret = mysql_query(mysql, sql.c_str());
        if (ret != 0)
        {
            LOG(ERROR, "mysql query failed : %s\n", mysql_error(mysql));
            //mysql_close(mysql);
            return false;
        }
        return true;
    }
    static void mysql_destroy(MYSQL *mysql)
    {
        if (mysql != NULL)
            mysql_close(mysql);
        return;
    }
};

class json_util
{
public:
    static bool json_seriliaze(const Json::Value &root, std::string &str)
    {
        Json::StreamWriterBuilder swb;
        // 通过工厂生产一个streamwriter类
        std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter());
        // Json::StreamWriter *sw = swb.newStreamWriter();
        std::stringstream ss;
        int res = sw->write(root, &ss);
        if (res)
        {
            // std::cerr << "serialize fail!" << std::endl;
            LOG(ERROR, "serialize fail!\n");
            return false;
        }
        str = ss.str();
        return true;
    }
    static bool json_deseriliaze(Json::Value &root, const std::string &str)
    {
        Json::CharReaderBuilder crb;
        // Json::CharReader* cr = crb.newCharReader();
        std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
        std::string cerr;
        bool res = cr->parse(str.c_str(), str.c_str() + str.size(), &root, &cerr);
        if (!res)
        {
            LOG(ERROR, "deseriliaze failed! : %s\n", cerr.c_str());
            return false;
        }
        return true;
    }
};

class string_util
{
public:
    static bool split(const std::string& str, const std::string& sep, std::vector<std::string>& res)
    {
        size_t pos = 0, index = 0;
        while(index < str.size())
        {
            pos = str.find(sep, index);
            if(pos == std::string::npos)
            {
                res.push_back(str.substr(index));
                break;
            }
            if(pos == index)
            {
                index += sep.size();
                continue;
            }
            res.push_back(str.substr(index, pos - index));
            index = pos + sep.size();
        }
        return true;
    }
};

class file_util
{
public:
    static bool read(const std::string& filename, std::string& body)
    {
        //打开文件
        std::ifstream ifs(filename, std::ios::binary);
        //获取文件大小
        size_t fsize = 0;
        ifs.seekg(0, std::ios::end);
        fsize = ifs.tellg();
        ifs.seekg(0, std::ios::beg);
        //读取文件所有数据
        body.resize(fsize);
        ifs.read(&body[0], fsize);
        if(!ifs.good())
        {
            LOG(ERROR, "file read failed!\n");
            ifs.close();
            return false;
        }
        //关闭文件
        ifs.close();
        return true; 
    }
};

