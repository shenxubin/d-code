#pragma once
#include <iostream>
#include <list>
#include <mutex>
#include <condition_variable>
#include <thread>
#include "room.hpp"
#include "online.hpp"
#include "db.hpp"
template <class T>
class match_queue
{
private:
    std::list<T> _list;
    std::mutex _mutex;
    //为了阻塞消费者，后边使用的时候队列中元素的个数小于2则阻塞
    std::condition_variable _cond;
public:
    match_queue()
    {}
    ~match_queue()
    {}
    //获取元素个数
    int size()
    {
        return _list.size();
    }
    bool isEmpty()
    {
        return _list.empty();
    }
    //阻塞线程
    void wait()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cond.wait(lock);
    }
    //入队数据并唤醒线程
    bool push(const T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _list.push_back(data);
        _cond.notify_all();
        return true;
    }
    bool pop(T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        if(_list.empty() == true)
            return false;
        data = _list.front();
        _list.pop_front();
        return true;
    }
    //移除指定的数据
    bool remove(const T& data)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _list.remove(data);
        return true;
    }
};

class matcher
{
private:
    match_queue<uint64_t> _q_bronze;
    match_queue<uint64_t> _q_silver;
    match_queue<uint64_t> _q_gold;
    std::thread _th_bronze;
    std::thread _th_silver;
    std::thread _th_gold;
    room_manager* _rm;
    user_table* _ut;
    online_manager* _om;
private:
    void handle_match(match_queue<uint64_t>& mqueue)
    {
        while(true)
        {
            //1.判断队列人数是否大于2，小于2则阻塞等待
            while(mqueue.size() < 2){mqueue.wait();}
            //2.人数够就出队两个玩家
            uint64_t uid1,uid2;
            bool ret = mqueue.pop(uid1);
            if(ret == false) continue;
            ret = mqueue.pop(uid2);
            if(ret == false) {add(uid1); continue;}
            //3.校验两个玩家是否都在线，如果有人掉线就把另外一个人重新添加入队列中
            websocket_server::connection_ptr conn1 = _om->get_conn_from_hall(uid1);
            if(conn1.get() == nullptr)
            {
                add(uid2);
                continue;
            }
            websocket_server::connection_ptr conn2 = _om->get_conn_from_hall(uid2);
            if(conn1.get() == nullptr)
            {
                add(uid1);
                continue;
            }
            //4.为两个玩家创建房间，并将玩家加入房间中
            room_ptr rp = _rm->createRoom(uid1, uid2);
            if(rp.get() == nullptr)
            {
                add(uid1);
                add(uid2);
                continue;
            }
            //5.对两个玩家进行响应
            Json::Value resp;
            resp["optye"] = "match_success";
            resp["result"] = true;
            std::string body;
            json_util::json_seriliaze(resp, body);
            conn1->send(body);
            conn2->send(body);
        }

    }
    void th_bronze_entry()
    {
        handle_match(_q_bronze);
    }
    void th_silver_entry()
    {
        handle_match(_q_silver);
    }
    void th_gold_entry()
    {
        handle_match(_q_gold);
    }
public:
    matcher(room_manager* rm, user_table* ut, online_manager* om)
    :_rm(rm),_ut(ut),_om(om),
     _th_bronze(std::thread(&matcher::th_bronze_entry, this)),
     _th_silver(std::thread(&matcher::th_silver_entry, this)),
     _th_gold(std::thread(&matcher::th_gold_entry, this))
    {
        LOG(INFO, "matcher initializes well\n");
    }
    ~matcher()
    {}
    bool add(uint64_t uid)
    {
        //根据玩家的天梯分数添加到不同的匹配队列中
        Json::Value user;
        bool ret = _ut->select_by_id(uid, user);
        if(ret == false)
        {
            LOG(INFO, "get player %d information wrong\n", uid);
            return false;
        }
        int score = user["score"].asInt();
        if(score < 2000)
            _q_bronze.push(uid);
        else if(score >= 2000 && score < 3000)
            _q_silver.push(uid);
        else
            _q_gold.push(uid);
        return true;
    }
    bool del(uint64_t uid)
    {
        Json::Value user;
        bool ret = _ut->select_by_id(uid, user);
        if(ret == false)
        {
            LOG(INFO, "get player %d information wrong\n", uid);
            return false;
        }
        int score = user["score"].asInt();
        if(score < 2000)
            _q_bronze.remove(uid);
        else if(score >= 2000 && score < 3000)
            _q_silver.remove(uid);
        else
            _q_gold.remove(uid);
    }
};