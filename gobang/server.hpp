#pragma once
#include "online.hpp"
#include "db.hpp"
#include "session.hpp"
#include "room.hpp"
#include "util.hpp"
#include "matcher.hpp"
#define WWWROOT "./wwwroot/webroot/"
class gobang_server
{
private:
    //静态资源根目录
    std::string _web_root;
    websocket_server _wssrv;
    user_table _ut;
    online_manager _om;
    room_manager _rm;
    session_manager _sm;
    matcher _mc;
private:
void http_callback(websocketpp::connection_hdl hdl)
{
    std::string filename = "register.html";
    std::string pathname = _web_root + filename;
    std::string body;
    file_util::read(pathname, body);
    websocket_server::connection_ptr conn = _wssrv.get_con_from_hdl(hdl);
    conn->set_status(websocketpp::http::status_code::ok);
    conn->append_header("Content-Type", "text/html");
    conn->append_header("Content-Length", std::to_string(body.size()));
    conn->set_body(body);
}
void wsopen_callback(websocketpp::connection_hdl hdl)
{
}
void wsclose_callback(websocketpp::connection_hdl hdl)
{
}
void wsmsg_callback(websocketpp::connection_hdl hdl, websocket_server::message_ptr msg)
{
}
public:
    //进行成员初始化，以及服务器回调函数的设置
    gobang_server(const std::string &host,
               const std::string &user,
               const std::string &pass,
               const std::string &dbname,
               uint16_t port = 3306,std::string web_root = WWWROOT)
               :_web_root(web_root),_ut(host,user,pass,dbname,port)
               ,_rm(&_ut, &_om),_sm(&_wssrv),_mc(&_rm, &_ut, &_om)
    {
        _wssrv.set_access_channels(websocketpp::log::alevel::none);//不需要输出日志
        //3.初始化asio调度器
        _wssrv.init_asio();
        _wssrv.set_reuse_addr(true);//设置地址复用
        //4.设置回调函数
        _wssrv.set_http_handler(std::bind(&gobang_server::http_callback, this, std::placeholders::_1));
        _wssrv.set_open_handler(std::bind(&gobang_server::wsopen_callback, this, std::placeholders::_1));
        _wssrv.set_close_handler(std::bind(&gobang_server::wsclose_callback,this, std::placeholders::_1));
        _wssrv.set_message_handler(std::bind(&gobang_server::wsmsg_callback,this, std::placeholders::_1, std::placeholders::_2));
    }
    ~gobang_server()
    {}
    void start(int port)
    {
        _wssrv.listen(port);
        //6.开始获取新链接
        _wssrv.start_accept();
        //7.服务器开始运行
        _wssrv.run();
    }
};
