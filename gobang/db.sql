create database if not exists gobang;
use gobang;
create table if not exists user(
    id int unsigned primary key auto_increment,
    username varchar(32) unique key not null,
    pass varchar(128) not null,
    score int unsigned,
    total_count int unsigned,
    win_count int unsigned
);