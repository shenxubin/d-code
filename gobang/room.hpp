#pragma once
#include <iostream>
#include "util.hpp"
#include <vector>
#include "online.hpp"
#include "db.hpp"

#define BOARD_ROW 15
#define BOARD_COL 15
#define WHITE_CHESS 1
#define BLACK_CHESS 2
enum room_status
{
    IN_GAME,
    NOT_IN_GAME
};
class room
{
private:
    uint64_t _room_id;
    room_status _room_status;
    size_t _player_count;
    uint64_t _white_player_id;
    uint64_t _black_player_id;
    user_table* _tb_user;
    online_manager* _online_user;
    std::vector<std::vector<int>> _board;
private:
    bool five(int row, int col, int row_off, int col_off, int color)
    {
        size_t success_count = 1;
        int search_row = row + row_off;
        int search_col = col + col_off;
        while(search_row >= 0 && search_row < BOARD_ROW
            &&search_col >= 0 && search_col < BOARD_COL
            &&_board[search_row][search_col] == color)
        {
            //同色棋子++
            success_count++;
            search_row += row_off;
            search_col += col_off;
        }
        search_row = row - row_off;
        search_col = col - col_off;
        while(search_row >= 0 && search_row < BOARD_ROW
            &&search_col >= 0 && search_col < BOARD_COL
            &&_board[search_row][search_col] == color)
        {
            //同色棋子++
            success_count++;
            search_row -= row_off;
            search_col -= col_off;
        }
        return success_count >= 5 ? true : false;
    }
    uint64_t checkWin(int row, int col, int color)
    {
        //检查四个方向上是否有一个方向存在五星连珠
        if(five(row, col, 1, 1, color) || five(row, col, 1, -1, color) ||
           five(row, col, 0, 1, color) || five(row, col, 1, 0, color))
        {
            return color == WHITE_CHESS ? _white_player_id : _black_player_id;
        }
        return 0;
    }
public:
    room(uint64_t room_id, user_table* tb_user, online_manager* online_user)
    :_room_id(room_id),_tb_user(tb_user),_online_user(online_user),
    _room_status(NOT_IN_GAME),_player_count(0),_board(BOARD_ROW, std::vector<int>(BOARD_COL, 0))
    {
        LOG(INFO, "room %lu create successfully!", _room_id);
    }
    ~room()
    {
        LOG(INFO, "room %lu destroy successfully!", _room_id);
    }
    uint64_t getRoomID(){return _room_id;}
    room_status getRoomStatus(){return _room_status;}
    size_t getPlayerCount(){return _player_count;}
    bool addWhitePlayer(uint64_t uid){_white_player_id = uid; _player_count++; return true;}
    bool addBlackPlayer(uint64_t uid){_black_player_id = uid; _player_count++; return true;}
    uint64_t getWhitePlayerID() {return  _white_player_id;}
    uint64_t getBlackPlayerID() {return  _black_player_id;}

    //处理下棋动作
    Json::Value handleChess(Json::Value& req)
    {
        Json::Value json_resp;
        //1.当前请求的房间号是否与当前房间的房间号相匹配
        uint64_t room_id = req["room_id"].asUInt64();
        if(room_id != _room_id)
        {
            json_resp["optype"] = "put_chess";
            json_resp["result"] = false;
            json_resp["reason"] = "房间号不匹配！";
            return json_resp;
        }
        //2.判断房间中两个玩家是否都在线
        int chess_row = req["row"].asInt();
        int chess_col = req["col"].asInt();
        uint64_t cur_uid = req["uid"].asInt64();
        if(_online_user->is_in_game_room(_white_player_id) == false)
        {
            json_resp["optype"] = "put_chess";
            json_resp["result"] = true;
            json_resp["reason"] = "对方掉线，恭喜你获胜！";
            json_resp["room_id"] = (Json::UInt64)_room_id;
            json_resp["winner"] = (Json::UInt64)_black_player_id;
            _tb_user->win(_black_player_id);
            _tb_user->lose(_white_player_id);
            return json_resp;
        }
        if(_online_user->is_in_game_room(_black_player_id) == false)
        {
            json_resp["optype"] = "put_chess";
            json_resp["result"] = true;
            json_resp["reason"] = "对方掉线，恭喜你获胜！";
            json_resp["room_id"] = (Json::UInt64)_room_id;
            json_resp["winner"] = (Json::UInt64)_white_player_id;
            _tb_user->win(_white_player_id);
            _tb_user->lose(_black_player_id);
            return json_resp;
        }
        //3.获取下棋位置，判断玩家下棋是否合理 
        if(_board[chess_row][chess_col] != 0)
        {
            json_resp["optype"] = "put_chess";
            json_resp["result"] = true;
            json_resp["reason"] = "下棋位置错误！";
            return json_resp;
        }
        int cur_color = cur_uid == _white_player_id ? WHITE_CHESS : BLACK_CHESS;
        _board[chess_row][chess_col] = cur_color;
        //4.判断是否有玩家已经胜利
        uint64_t win_id = checkWin(chess_row, chess_col, cur_color);
        //更新数据库
        if(win_id != 0)
        {
            uint64_t lose_id = win_id == _white_player_id ? _black_player_id : _white_player_id;
            _tb_user->win(win_id);
            _tb_user->lose(lose_id);
        }
        json_resp["optype"] = "put_chess";
        json_resp["result"] = true;
        json_resp["reason"] = "五星连珠，恭喜你获得本场比赛胜利！";
        json_resp["room_id"] = (Json::UInt64)_room_id;
        json_resp["winner"] = (Json::UInt64)win_id;
        return json_resp;
    }
    //处理聊天动作
    Json::Value handleChat(Json::Value& req)
    {
        Json::Value json_resp = req;
        //检测房间是否一致
        uint64_t room_id = json_resp["room_id"].asUInt64();
        if(room_id != _room_id)
        {
            json_resp["optype"] = "chat";
            json_resp["result"] = false;
            json_resp["reason"] = "房间号不匹配！";
            return json_resp;
        }
        //检测消息中是否有敏感词
        std::string msg = req["message"].asString();
        size_t pos = msg.find("FW");
        if(pos != std::string::npos)
        {
            json_resp["optype"] = "chat";
            json_resp["result"] = false;
            json_resp["reason"] = "消息中包含敏感词，不允许发送！";
            return json_resp;
        }
        //广播消息
        json_resp["result"] = true;
        return json_resp;
    }
    //处理玩家退出房间动作
    void handleExit(uint64_t uid)
    {
        Json::Value json_resp;
        if(_room_status == IN_GAME)
        {
            json_resp["optype"] = "put_chess";
            json_resp["result"] = true;
            json_resp["reason"] = "你掉线，恭喜对方获胜！";
            json_resp["room_id"] = (Json::UInt64)_room_id;
            uint64_t win_id = uid == _white_player_id ? _black_player_id : _white_player_id;
            json_resp["winner"] = (Json::UInt64)win_id;
            broadcast(json_resp);
        }
        _player_count--;
        return ;
    }
    //总的请求处理函数，在函数内部区分请求类型，根据不同的请求调用不同的处理函数，得到响应进行广播
    void handleRequest(Json::Value& req)
    {
        //1.校验房间号是否匹配
        Json::Value json_resp;
        uint64_t room_id = req["room_id"].asUInt64();
        if(room_id != _room_id)
        {
            json_resp["optype"] = req["optype"].asString();
            json_resp["result"] = false;
            json_resp["reason"] = "房间号不匹配！";
            return broadcast(json_resp);
        }
        //2.根据不同的请求类型调用不同的处理函数
        if(req["optye"].asString() == "put_chess")
        {
            _room_status = IN_GAME;
            json_resp = handleChess(req);
            _room_status = NOT_IN_GAME;
        }
        else if(req["optye"].asString() == "chat")
        {
            _room_status = NOT_IN_GAME;
            json_resp = handleChat(req);
        }
        else
        {
            json_resp["optype"] = req["optype"].asString();
            json_resp["result"] = false;
            json_resp["reason"] = "未知请求类型！";
        }
        broadcast(json_resp);
        return ;
    }
    //将指定的消息广播给房间中所有玩家
    void broadcast(Json::Value& req)
    {
        //1.序列化
        std::string body;
        json_util::json_seriliaze(req, body);
        //2.获取房间中所有的通信连接，发送响应信息
        websocket_server::connection_ptr wconn = _online_user->get_conn_from_room(_white_player_id);
        if(wconn != nullptr)
            wconn->send(body);
        wconn = _online_user->get_conn_from_room(_black_player_id);
        if(wconn != nullptr)
            wconn->send(body);
        
        return ;
    }
};

using room_ptr = std::shared_ptr<room>;

class room_manager
{
private:
    uint64_t _next_rid;
    std::mutex _mutex;
    user_table* _tb_user;
    online_manager* _online_manager;
    std::unordered_map<uint64_t, room_ptr> _rooms;
    std::unordered_map<uint64_t, uint64_t> _user_room;
public:
    //初始化房间ID计数器
    room_manager(user_table* tb_user, online_manager* online_user)
    :_next_rid(1),_tb_user(tb_user),_online_manager(online_user)
    {
        LOG(INFO, "房间管理模块初始化完成！\n");
    }
    ~room_manager()
    {}
    //为两个用户创建房间，返回房间管理指针
    room_ptr createRoom(uint64_t uid1, uint64_t uid2)
    {
        //1.检验两个玩家是否都在游戏大厅中
        if(!_online_manager->is_in_game_hall(uid1))
        {
            LOG(INFO, "用户%lu不在游戏大厅中，创建房间失败！\n", uid1);
            return room_ptr();
        }
        if(!_online_manager->is_in_game_hall(uid2))
        {
            LOG(INFO, "用户%lu不在游戏大厅中，创建房间失败！\n", uid2);
            return room_ptr();
        }
        //2.创建房间，将玩家信息添加到房间中
        std::unique_lock<std::mutex> lock(_mutex);
        room_ptr rp(new room(_next_rid, _tb_user, _online_manager));
        rp->addBlackPlayer(uid1);
        rp->addWhitePlayer(uid2);
        //3.将房间管理起来
        _rooms.insert(std::make_pair(_next_rid, rp));
        _user_room.insert(std::make_pair(uid1, _next_rid));
        _user_room.insert(std::make_pair(uid2, _next_rid));
        _next_rid++; 
        //4.返回房间信息
        return rp;
    }
    //通过房间ID获取房间信息
    room_ptr getRoomByRid(uint64_t rid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _rooms.find(rid);
        if(it == _rooms.end())
        {
            return room_ptr();
        }
        return it->second;
    }
    //通过用户ID获取房间信息
    room_ptr getRoomByUid(uint64_t uid)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        auto it = _user_room.find(uid);
        if(it == _user_room.end())
        {
            return room_ptr();
        }
        uint64_t rid = it->second;
        auto it2 = _rooms.find(rid);
        if(it2 == _rooms.end())
        {
            return room_ptr();
        }
        return it2->second;
    }
    //通过房间ID销毁房间
    bool destroyRoomByRid(uint64_t rid)
    {
        //1.通过房间ID获取房间信息
        room_ptr rp = getRoomByRid(rid);
        if(rp == nullptr)
        {
            LOG(INFO, "房间%lu 不存在，销毁该房间失败！");
            return false;
        }
        //2.通过房间ID获取房间中所有用户的ID
        uint64_t uid1 = rp->getWhitePlayerID();
        uint64_t uid2 = rp->getBlackPlayerID();
        //3.移除房间管理中的用户信息

        _user_room.erase(uid1);
        _user_room.erase(uid2);
        //4.移除房间管理信息
        _rooms.erase(rid);
    }
    //删除房间中指定用户
    bool removeRoomUser(uint64_t uid)
    {
        room_ptr rp = getRoomByUid(uid);
        if(rp == nullptr)
        {
            LOG(INFO, "房间%lu 不存在，删除用户失败！");
            return false;
        }
        rp->handleExit(uid);
        if(rp->getPlayerCount() == 0)
        {
            destroyRoomByRid(rp->getRoomID());
        }
        return true;
    }
};