//#include <iostream>
//#include <vector>
//using namespace std;
//
//class Student
//{
//    friend ostream& operator<<(ostream& cout, Student& st);
//public:
//    int _SerialNumber;
//    string _name;
//    double _score1;
//    double _score2;
//    double _score3;
//    double _averageScore;
//};
//ostream& operator<<(ostream& cout, Student& st)
//{
//    cout << st._SerialNumber << " " << st._name << " " << st._score1 << " " << st._score2 <<
//        " " << st._score3 << " " << st._averageScore << " ";
//
//    return cout;
//}
//
//void Swap(Student* s1, Student* s2)
//{
//    double tmp;
//    int tmpi;
//
//    string nametmp;
//    nametmp = s1->_name;
//    s1->_name = s2->_name;
//    s2->_name = nametmp;
//
//    tmpi = s1->_SerialNumber;
//    s1->_SerialNumber = s2->_SerialNumber;
//    s2->_SerialNumber = tmpi;
//
//    tmp = s1->_score1;
//    s1->_score1 = s2->_score1;
//    s2->_score1 = tmp;
//
//    tmp = s1->_score2;
//    s1->_score2 = s2->_score2;
//    s2->_score2 = tmp;
//
//    tmp = s1->_score3;
//    s1->_score3 = s2->_score3;
//    s2->_score3 = tmp;
//
//    tmp = s1->_averageScore;
//    s1->_averageScore = s2->_averageScore;
//    s2->_averageScore = tmp;
//}
//
//int main()
//{
//    int n;
//    cin >> n;
//
//    vector<Student> v1;
//    int SerialNumber;
//    string name;
//    int score1;
//    int score2;
//    int score3;
//    for (int i = 0; i < n; i++)
//    {
//        Student st;
//        cin >> st._SerialNumber >> st._name >> st._score1 >> st._score2 >> st._score3;
//        st._averageScore = (st._score1 + st._score2 + st._score3) / 3;
//        v1.push_back(st);
//    }
//    for (int i = 0; i < n - 1; i++)
//    {
//        for (int j = 0; j < n - 1 - i; j++)
//        {
//            if (v1[j]._averageScore < v1[j + 1]._averageScore)
//            {
//                Swap(&v1[j], &v1[j + 1]);
//            }
//        }
//    }
//    int j = 1;
//    for (int i = 0; i < n; i++)
//    {
//        // printf("%d ", v1[i]._SerialNumber);
//        // cout << v1[i]._name << " ";
//        // printf("%.2lf %.2lf %.2lf %.2lf %d\n", 
//        // v1[i]._score1, v1[i]._score2, v1[i]._score3, v1[i]._averageScore, j++);
//        cout << v1[i] << j++ << endl;
//    }
//
//    return 0;
//}

//1 孙俪莉 76.00 78.00 89.00 81.00 1
//3 刘德华 56.00 84.00 90.00 76.67 2
//2 章子怡 72.00 56.00 67.00 65.00 3



#include <stdio.h>

int main()
{
	int m, n, i, k, b, f;
	scanf("%d %d", &n, &m);

	//b = m - 1;//n-1使得序号从0开始 
	//for (k = 1; k <= n; k++)
	//{
	//	f = b % k;
	//	b = f + m;
	//}
	//printf("%d ", f + 1);//f+1使得序号从1开始 

	for (i = 0; i < n; i++)
	{
		b = m - 1;
		for (k=n-i; k<=n; k++)
		{
			f = b % k;
			b = f + m;
		}
		printf("%d ", f + 1);
	}
	return 0;
}