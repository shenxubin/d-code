#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

class InetAddr
{
private:
    struct sockaddr_in _addr;
    std::string _ip;
    int _port;

public:
    InetAddr(const struct sockaddr_in &addr)
        : _addr(addr)
    {
        SetInetAddr();
    }
    InetAddr(const std::string &ip, uint16_t port) : _ip(ip), _port(port)
    {
        _addr.sin_family = AF_INET;
        _addr.sin_port = htons(_port);
        _addr.sin_addr.s_addr = inet_addr(_ip.c_str());
    }
    InetAddr()
    {}
    ~InetAddr()
    {
    }
    void SetInetAddr()
    {
        _ip = inet_ntoa(_addr.sin_addr);
        _port = ntohs(_addr.sin_port);
    }
    std::string GetIP()
    {
        return _ip;
    }
    int GetPort()
    {
        return _port;
    }
    struct sockaddr_in getInetAddr()
    {
        return _addr;
    }
    bool operator==(const InetAddr &addr)
    {
        if (_ip == addr._ip && _port == addr._port)
        {
            return true;
        }
        return false;
    }
};