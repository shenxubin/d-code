#pragma once

#include <iostream>
#include <string>
#include <jsoncpp/json/json.h>
#include <memory>
#include <unistd.h>
namespace protocol_ns
{
    const std::string SEP = "\r\n";
    std::string Encode(const std::string &json_str)
    {
        int json_str_len = json_str.size();
        std::string proto_str = std::to_string(json_str_len);
        proto_str += SEP;
        proto_str += json_str;
        proto_str += SEP;
        return proto_str;
    }
    std::string Decode(std::string &inbuffer)
    {
        auto pos = inbuffer.find(SEP);
        if (pos == std::string::npos)
            return std::string();
        std::string len_str = inbuffer.substr(0, pos);
        if (len_str.empty())
            return std::string();
        int packlen = std::stoi(len_str);

        int total = packlen + len_str.size() + 2 * SEP.size();
        if (inbuffer.size() < total)
            return std::string();

        std::string package = inbuffer.substr(pos + SEP.size(), packlen);
        inbuffer.erase(0, total);
        return package;
    }
    class Request
    {
    public:
        int _x;
        int _y;
        char _oper;

    public:
        Request(int x, int y, char oper) : _x(x), _y(y), _oper(oper)
        {
        }
        Request()
        {}
        bool Serialize(std::string *out)
        {
            Json::Value root;
            root["x"] = _x;
            root["y"] = _y;
            root["oper"] = _oper;

            Json::FastWriter fastwrite;
            *out = fastwrite.write(root);
            return true;
        }
        bool Deserialize(const std::string &in)
        {
            Json::Value root;
            Json::Reader read;
            bool res = read.parse(in, root);
            if (!res)
                return false;

            _x = root["x"].asInt();
            _y = root["y"].asInt();
            _oper = root["oper"].asInt();

            return true;
        }
        ~Request()
        {
        }
    };
    class Response
    {
    public:
        int _res;
        int _code;

    public:
        Response(int result, int code)
            : _res(result), _code(code)
        {
        }
        Response()
        {}
        ~Response()
        {
        }
        bool Serialize(std::string *out)
        {
            Json::Value root;
            root["res"] = _res;
            root["code"] = _code;

            Json::FastWriter fastwrite;
            *out = fastwrite.write(root);
            return true;
        }
        bool Deserialize(std::string &in)
        {
            Json::Value root;
            Json::Reader read;
            bool res = read.parse(in, root);
            if (!res)
                return false;

            _res = root["res"].asInt();
            _code = root["code"].asInt();

            return true;
        }
    };
    class Factory
    {
    public:
        Factory()
        {
            srand(time(nullptr) ^ getpid());
            opers = "+/*/%^&|";
        }
        std::shared_ptr<Request> BuildRequest()
        {
            int x = rand() % 10 + 1;
            usleep(x * 10);
            int y = rand() % 5; // [01,2,3,4]
            usleep(y * x * 5);
            char oper = opers[rand() % opers.size()];
            std::shared_ptr<Request> req = std::make_shared<Request>(x, y, oper);
            return req;
        }
        std::shared_ptr<Response> BuildResponse()
        {
            return std::make_shared<Response>();
        }
        ~Factory()
        {
        }

    private:
        std::string opers;
    };
}