#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <cstring>
#include <unistd.h>
#include <functional>
#include <thread>
#include <memory>
#include "Log.hpp"
#include "InetAddr.hpp"
#include "Socket.hpp"

using namespace socket_ns;
class TcpServer;
using io_service_t = std::function<void (socket_sptr sockfd, InetAddr client)>;
class Threaddata
{
public:
    socket_sptr _sockfd;
    InetAddr _clientaddr;
    TcpServer *_t;

public:
    Threaddata(socket_sptr sockfd, InetAddr addr, TcpServer *t)
        : _sockfd(sockfd), _clientaddr(addr), _t(t)
    {
    }
    ~Threaddata()
    {
    }
};
class TcpServer
{
private:
    InetAddr _localaddr;
    std::unique_ptr<Socket> _listensock;
    bool _isrunning;

    io_service_t _service;

public:
    TcpServer(int port, io_service_t service) 
        : _localaddr("0", port),
          _listensock(std::make_unique<TcpSocket>()),
          _service(service),
          _isrunning(false)
    {
        _listensock->BuildListenSocket(_localaddr);
    }
    ~TcpServer()
    {
        
    }
    static void *HandlerSock(void *args)
    {
       pthread_detach(pthread_self());
        Threaddata *td = static_cast<Threaddata*>(args);
        td->_t->_service(td->_sockfd, td->_clientaddr);
        ::close(td->_sockfd->SockFd()); // 文件描述符泄漏
        delete td;
        return nullptr;
    }
    void Loop()
    {
        _isrunning = true;
        while (_isrunning)
        {
            InetAddr peeraddr;
            socket_sptr normalsock = _listensock->Accepter(&peeraddr);
            if(normalsock == nullptr) continue;

            pthread_t t;
            Threaddata *td = new Threaddata(normalsock, peeraddr, this);
            pthread_create(&t, nullptr, HandlerSock, td); //将线程分离
        }
        _isrunning = false;
    }
};