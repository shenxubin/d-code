#include "UdpServer.hpp"
#include <memory>

void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " port\n" << std::endl; 
}
int main(int argc, char* argv[])
{
   
    if(argc != 2)
    {
        Usage(argv[0]);
        return USE_ERROR;
    }
    EnableScreen();
    std::unique_ptr<UdpServer> usv = std::make_unique<UdpServer>(std::stoi(argv[1]));
    usv->InitUdpServer();
    usv->Start();
    return 0;
}