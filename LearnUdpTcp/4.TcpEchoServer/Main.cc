#include "TcpServer.hpp"
#include <memory>
void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " port\n" << std::endl;
}
int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        return -1;
    }
    EnableScreen();
    std::unique_ptr<TcpServer> tsvr = std::make_unique<TcpServer>(std::stoi(argv[1]));
    tsvr->InitTcpServer();
    tsvr->Loop();

    return 0;
}