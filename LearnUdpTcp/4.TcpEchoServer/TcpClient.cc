#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include "Log.hpp"

enum
{
    SOCKET_ERRNO = 1,
    USE_ERROR,
    CONNET_ERROR,
};
void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " server_ip server_port\n"
              << std::endl;
}
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        return USE_ERROR;
    }

    int sockfd = ::socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
    {
        LOG(FATAL, "socket create fail, %s, %d\n", strerror(errno), errno);
        exit(SOCKET_ERRNO);
    }
    LOG(INFO, "socket create successfully, sockfd : %d\n", sockfd);

    int server_port = std::stoi(argv[2]);
    std::string server_ip = argv[1];
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(server_port);
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    // 建立连接
    int n = ::connect(sockfd, (struct sockaddr *)&server, sizeof(server));
    if (n < 0)
    {
        LOG(FATAL, "socket connect fail, %s, %d\n", strerror(errno), errno);
        exit(CONNET_ERROR);
    }
    LOG(INFO, "socket connet successfully\n");

    while (true)
    {
        std::cout << "Client echo# ";
        std::string outstring;
        getline(std::cin, outstring);
        ssize_t n = ::send(sockfd, outstring.c_str(), outstring.size(), 0);
        std::string instring;
        char inbuffer[1024];
        if (n > 0)
        {
            int m = ::recv(sockfd, inbuffer, sizeof(inbuffer)-1, 0);
            if (m > 0)
            {
                inbuffer[m] = 0;
                std::cout  << inbuffer << std::endl;
            }
            else 
                break;
        }
        else
            break;
    }
    ::close(sockfd);
    return 0;
}