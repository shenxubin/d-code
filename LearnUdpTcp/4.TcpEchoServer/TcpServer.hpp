#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <cstring>
#include <unistd.h>
#include <functional>
#include <thread>
#include "Log.hpp"
#include "InetAddr.hpp"
#include "ThreadPool.hpp"

const static int gba = 16;
const static int defaultsock = -1;
using namespace ThreadModule;

using task_t = std::function<void()>;

class TcpServer;
class Threaddata
{
public:
    int _sockfd;
    InetAddr _addr;
    TcpServer* _t;
public:
    Threaddata(int sockfd, InetAddr addr, TcpServer* t)
    :_sockfd(sockfd),_addr(addr),_t(t)
    {}
    ~Threaddata()
    {}
};
class TcpServer
{
private:
    int _listensock;
    uint16_t _port;
    bool _isrunning;
public:
    TcpServer(uint16_t port)
        : _port(port),_isrunning(false),_listensock(defaultsock)
    {
    }
    ~TcpServer()
    {
        if(_listensock > defaultsock)
            ::close(_listensock);
    }
    void InitTcpServer()
    {
        _listensock = ::socket(AF_INET, SOCK_STREAM, 0);
        if (_listensock < 0)
        {
            LOG(FATAL, "socket create fail\n");
            exit(-1);
        }
        LOG(INFO, "socket create successfully, sockfd : %d\n", _listensock);

        struct sockaddr_in server;
        memset(&server, 0, sizeof(server));
        server.sin_family = AF_INET;
        server.sin_port = htons(_port);
        server.sin_addr.s_addr = INADDR_ANY;
        int n = ::bind(_listensock, (const struct sockaddr *)&server, sizeof(server));
        if (n < 0)
        {
            LOG(FATAL, "socket bind fail, %s, %d\n", strerror(errno), errno);
            exit(-1);
        }
        LOG(INFO, "socket bind successfully, sockfd : %d\n", _listensock);

        //建立连接
        n = ::listen(_listensock, gba);
        if (n < 0)
        {
            LOG(FATAL, "socket listen fail, %s, %d\n", strerror(errno), errno);
            exit(-1);
        }
        LOG(INFO, "socket listen successfully, sockfd : %d\n", _listensock);

    }
    void Server(int sockfd, InetAddr client)
    {
        std::string clientaddr = "[" + client.GetIP() + std::to_string(client.GetPort()) + "]";
        char buffer[1024];
        while(true)
        {
            ssize_t n = ::read(sockfd, buffer, sizeof(buffer)-1);
            if(n > 0)
            {
                buffer[n] = 0;
                std::cout << clientaddr << "# " << buffer << std::endl;
                std::string echo_string = "server echo# " ;
                echo_string += (buffer);
                ::write(sockfd, echo_string.c_str(), echo_string.size());
            }
            else if(n == 0)
            {
                LOG(INFO, "%s quit\n", clientaddr.c_str());
                break;
            }
            else
            {
                LOG(ERROR, "read error\n");
                break;
            }
        }
        ::close(sockfd);
    }
    static void* HandlerSock(void* args)
    {
        pthread_detach(pthread_self());
        Threaddata* t = static_cast<Threaddata*>(args);
        t->_t->Server(t->_sockfd, t->_addr);
        
        delete t;
        return nullptr;
    }
    void Loop()
    {
        _isrunning = true;
        while(_isrunning)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int sockfd = ::accept(_listensock, (struct sockaddr*)&peer, &len);

            //Server(sockfd, InetAddr(peer));

            //v1：采用多进程
            // pid_t id = fork();
            // if(id == 0)
            // {
            //     ::close(_listensock);
            //     //让孙子进程去执行，子进程直接返回，父进程等待成功
            //     if(fork() > 0)
            //         exit(0);
            //     Server(sockfd, InetAddr(peer));
            //     exit(0);
            // }
            // ::close(sockfd);
            // waitpid(id, nullptr, 0);

            //多线程
            // pthread_t t;
            // Threaddata* td = new Threaddata(sockfd, InetAddr(peer), this);
            // pthread_create(&t, nullptr, HandlerSock, td);

            //线程池
            task_t task = std::bind(&TcpServer::Server,this, sockfd, InetAddr(peer));
            ThreadPool<task_t>::GetInstance()->Enqueue(task);
        }
        _isrunning = false;
    }
};