#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "LockGuard.hpp"
#include "Log.hpp"
#include <cerrno>
#include <cstring>
#include <strings.h>
#include <functional>
#include "InetAddr.hpp"
#include "ThreadPool.hpp"


const static int defaultfd = -1;
enum 
{
    SOCKET_ERRNO = 1,
    USE_ERROR,
};

using handler_mess_t = std::function<void(int sockfd,const std::string message,InetAddr who)>;
class UdpServer
{
private:
    int _sockfd;
    uint16_t _port;
    bool _isrunning;
    handler_mess_t _handler_mess;
public:
    UdpServer(uint16_t port, handler_mess_t handler_mess)
    :_sockfd(defaultfd),_port(port),_isrunning(false),_handler_mess(handler_mess)
    {
        
    }
    ~UdpServer()
    {
    }
    void InitUdpServer()
    {
        //1.创建套接字
        _sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if(_sockfd < 0)
        {
            LOG(FATAL, "socket create fail, %s, %d\n",strerror(errno), errno);
            exit(SOCKET_ERRNO);
        }
        LOG(INFO, "socket create successfully, sockfd : %d\n", _sockfd);

        //2.bind，填充struct sockaddr_in结构
        struct sockaddr_in local;
        bzero(&local, sizeof(local));
        local.sin_family = AF_INET;
        local.sin_port = htons(_port);
        local.sin_addr.s_addr = INADDR_ANY;

        int n = bind(_sockfd, (sockaddr*)&local, sizeof(local));
        if(n < 0)
        {
            LOG(FATAL, "socket bind fail, %s, %d\n",strerror(errno), errno);
            exit(SOCKET_ERRNO);
        }
        LOG(INFO, "socket bind successfully\n");

    }
    void Start()
    {
        _isrunning = true;
        while(true)
        {
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            char message[1024];
            //3.收到消息
            ssize_t n = recvfrom(_sockfd,message, sizeof(message)-1, 0, (struct sockaddr*)&peer, &len);
            if(n > 0)
            {
                message[n] = 0;
                InetAddr inet(peer);
                LOG(INFO, "receive a message : %s, from %s:%d\n", message, inet.GetIP().c_str(), inet.GetPort());
                //进行路由转发
                _handler_mess(_sockfd, message, inet);
                
            }
            

        }
        _isrunning = false;
    }
};