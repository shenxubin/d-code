#pragma once
#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <vector>
#include <functional>
#include "InetAddr.hpp"

using task_t  = std::function<void()>;
class MessageRoute
{
private:
    std::vector<InetAddr> _online_user;
private:
    bool Exist(const InetAddr& addr)
    {
       for(auto& a : _online_user)
       {
            if(a == addr)
                return true;
       }
       return false;
    }
public:
    MessageRoute()
    {}
    ~MessageRoute()
    {}
    void AddUser(const InetAddr& addr)
    {
        if(!Exist(addr))
            _online_user.push_back(addr);
        
    }
    void DeleteUser(const InetAddr& addr)
    {
        for(auto a = _online_user.begin(); a != _online_user.end(); a++)
        {
            if(*a == addr)
            {
                _online_user.erase(a);
                break;
            }
        }
    }
    void RouteHelper(int sockfd, std::string message)
    {
        //进行消息转发
        for(auto& user : _online_user)
        {
            struct sockaddr_in addr = user.getInetAddr();
            socklen_t len = sizeof(addr);
            sendto(sockfd, message.c_str(), message.size(), 0, (struct sockaddr*)&addr,len);
        }
    }
    void Route(int sockfd, const std::string message,InetAddr who)
    {
        //首次发送消息时，将用户插入用户列表中
        AddUser(who);
        if(message == "Q" || message == "quit")
            DeleteUser(who);
        //构建对象，入队列，让线程进行转发
        task_t t = std::bind(&MessageRoute::RouteHelper, this, sockfd, message);
        ThreadPool<task_t>::GetInstance()->Enqueue(t);
    }
};