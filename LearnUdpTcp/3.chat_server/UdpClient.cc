#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstring>
#include "LockGuard.hpp"
#include "Log.hpp"
#include "Thread.hpp"
using namespace ThreadModule;

enum
{
    SOCKET_ERRNO = 1,
    USE_ERROR,
};

void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " server_ip server_port\n"
              << std::endl;
}
int InitClient(std::string &server_ip, int &server_port, struct sockaddr_in *server)
{
    // 1.创建套接字
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        LOG(FATAL, "socket create fail, %s, %d\n", strerror(errno), errno);
        exit(SOCKET_ERRNO);
    }
    memset(server, 0, sizeof(server));
    server->sin_family = AF_INET;
    server->sin_port = htons(server_port);
    server->sin_addr.s_addr = inet_addr(server_ip.c_str());

    return sockfd;
}

void recvmessage(int sockfd, std::string name)
{
    while (true)
    {
        struct sockaddr_in peer;
        socklen_t lens = sizeof(peer);
        char buffer[1024];
        int n = recvfrom(sockfd, buffer, sizeof(buffer) - 1, 0, (struct sockaddr *)&peer, &lens);
        if (n > 0)
        {
            buffer[n] = 0;
            std::cout <<name << " | " << buffer << std::endl;
        }
    }
}
void sendmessage(int sockfd, struct sockaddr_in &server, std::string name)
{
    std::string message;
    socklen_t len = sizeof(server);
    while (true)
    {
        std::cout << name << " enter | ";
        getline(std::cin, message);
        sendto(sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&server, len);
    }
}
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        return USE_ERROR;
    }

    // 2.客户端不需要显式地绑定端口号，直接收消息
    int server_port = std::stoi(argv[2]);
    std::string server_ip = argv[1];
    struct sockaddr_in server;
    int sockfd = InitClient(server_ip, server_port, &server);
    if (sockfd == -1)
        return -1;
    LOG(INFO, "socket create successfully, sockfd : %d\n", sockfd);

    func_t r = std::bind(&recvmessage, sockfd, std::placeholders::_1);
    func_t s = std::bind(&sendmessage, sockfd, server, std::placeholders::_1);

    Thread Recver(r, "recver");
    Thread Sender(s, "sender");
    Recver.Start();
    Sender.Start();

    Recver.Join();
    Sender.Join();

    return 0;
}