#include "UdpServer.hpp"
#include "MessRoute.hpp"
#include <memory>
void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " port\n" << std::endl; 
}
int main(int argc, char* argv[])
{
   
    if(argc != 2)
    {
        Usage(argv[0]);
        return -1;
    }
    EnableScreen();
    //消息转发模块
    MessageRoute mess;
    std::unique_ptr<UdpServer> usv = std::make_unique<UdpServer>(std::stoi(argv[1]), 
    std::bind(&MessageRoute::Route, &mess, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
    usv->InitUdpServer();
    usv->Start();
    return 0;
}