#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

class InetAddr
{
private:
    struct sockaddr_in _addr;
    std::string _ip;
    int _port;
public:
    InetAddr(const struct sockaddr_in& addr)
    :_addr(addr)
    {
        SetInetAddr();
    }
    ~InetAddr()
    {}
    void SetInetAddr()
    {
        _ip = inet_ntoa(_addr.sin_addr);
        _port = ntohs(_addr.sin_port);
    }
    std::string GetIP()
    {
        return _ip;
    }
    int GetPort()
    {
        return _port;
    }
};