#include <iostream>
#include <string>
#include <unordered_map>
#include <fstream>
#include "LockGuard.hpp"
#include "Log.hpp"

namespace dict_ns
{
    const std::string defaultFile = "Dict.txt";
    const std::string sep = ":";
    class Dict
    {
    private:
        std::string _conf_filename;
        std::unordered_map<std::string, std::string> _dict;
    public:
        Dict(const std::string filename = defaultFile)
        :_conf_filename(filename)
        {
            Load();
        }
        ~Dict()
        {}
        bool Load()
        {
            std::ifstream in(_conf_filename);
            if(!in.is_open())
            {
                LOG(FATAL, "file open fail");
                return false;
            }
            std::string line;
            std::string word,han;
            while(std::getline(in, line))
            {
                if(line.empty()) continue;
                auto pos = line.find(sep);
                if(pos == std::string::npos) continue;
                word = line.substr(0, pos);
                if(word.empty()) continue;
                han = line.substr(pos+sep.size());
                if(han.empty()) continue;

                _dict.insert(std::make_pair(word, han));
                LOG(INFO, "load %s:%s successfully\n", word.c_str(), han.c_str());
            }
            in.close();
            return true;
        }

        const std::string Translate(const std::string word)
        {
            auto iter = _dict.find(word);
            if(iter == _dict.end())
                return "未找到";
            return iter->second;
        }
    };
}
