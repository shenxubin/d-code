#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <cstring>
#include "LockGuard.hpp"
#include "Log.hpp"

enum
{
    SOCKET_ERRNO = 1,
    USE_ERROR,
};

void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " server_ip server_port\n"
              << std::endl;
}
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        return USE_ERROR;
    }

    // 1.创建套接字
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        LOG(FATAL, "socket create fail, %s, %d\n", strerror(errno), errno);
        exit(SOCKET_ERRNO);
    }
    LOG(INFO, "socket create successfully, sockfd : %d\n", sockfd);

    //2.客户端不需要显式地绑定端口号，直接收消息
    int server_port = std::stoi(argv[2]);
    std::string server_ip = argv[1];

    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(server_port);
    server.sin_addr.s_addr = inet_addr(server_ip.c_str());
    socklen_t len = sizeof(server);
    std::string buffer;
    while(true)
    {
        std::cout << "please enter# ";
        getline(std::cin, buffer);
        sendto(sockfd, buffer.c_str(), buffer.size(), 0, (struct sockaddr*)&server, len);

        struct sockaddr_in peer;
        socklen_t lens = sizeof(peer);
        char buffer[1024];
        int n = recvfrom(sockfd, buffer, sizeof(buffer)-1, 0, (struct sockaddr*)&peer, &lens);
        if(n > 0)
        {
            buffer[n] = 0;
            std::cout << "server echo# " << buffer << std::endl;
        }
    }

    return 0;
}