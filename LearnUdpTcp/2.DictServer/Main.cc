#include "UdpServer.hpp"
#include "Dict.hpp"
#include <memory>
using namespace dict_ns;
void Usage(std::string proc)
{
    std::cout << "Usage: \n\t" << proc << " port\n" << std::endl; 
}
int main(int argc, char* argv[])
{
   
    if(argc != 2)
    {
        Usage(argv[0]);
        return -1;
    }
    EnableScreen();
    Dict dict;
    std::unique_ptr<UdpServer> usv = std::make_unique<UdpServer>(std::stoi(argv[1]), std::bind(&Dict::Translate, &dict, std::placeholders::_1));
    usv->InitUdpServer();
    usv->Start();
    return 0;
}