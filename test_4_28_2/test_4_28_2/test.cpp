#include <iostream>
using namespace std;

template <class _T>
class Point3D
{
    template <class _Ty>
    friend ostream& operator<<(ostream& cout, const Point3D<_Ty>& p);
public:
    _T x, y, z;
    Point3D(_T x = 0, _T y = 0, _T z = 0)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    Point3D<_T>& operator=(const Point3D<_T>& p)
    {
        this->x = p.x;
        this->y = p.y;
        this->z = p.z;
        return *this;
    }
};

template <class _T, int length>
class Skeleton
{
    Point3D<_T> joints[length];
public:
    Skeleton(Point3D<_T>* j);

    Point3D<_T>& operator[](string sign)
    {
        if (sign == "HEAD")
        {
            return this->joints[0];
        }
        else
        {
            return this->joints[1];
        }
    }
};

template <class _T, int length>
Skeleton<_T, length>::Skeleton(Point3D<_T>* j)
{
    for (int i = 0; i < length; i++)
    {
        this->joints[i] = j[i];
    }
}

template <class _T>
ostream& operator<<(ostream& cout, const Point3D<_T>& p)
{
    cout << p.x << " " << p.y << " " << p.z << endl;
    return cout;
}
int main()
{
    Point3D<float> p1(0.0f, 0.0f, 0.0f);
    Point3D<float> p2(1.1f, 1.1f, 1.1f);
    Point3D<float> pSet[2];
    pSet[0] = p1;
    pSet[1] = p2;
    Skeleton<float, 2> sk(pSet);
    cout << sk["HEAD"] << sk["NECK"];
    return 0;
}