#include <iostream>
using namespace std;

class Person
{
public:
    Person(int a)
    {
        this->m_A = a;
    }
 /*   bool operator!=(const Person& p2)
    {
        if (this->m_A != p2.m_A)
        {
            return false;
        }
        else
        {
            return true;
        }
    }*/

    int operator+(const Person& p)
    {
        return this->m_A + p.m_A;
    }
private:
    int m_A;
};


int main()
{
    Person p1(1);
    Person p2(0);
    cout << (p1 + p2) << endl;
    return 0;
}